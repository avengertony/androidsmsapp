package com.infinitysms.usa.data.models

import android.os.Parcel
import android.os.Parcelable

data class DataEntityChild(
    val child: ArrayList<DataEntityChild>?,
    val created_at: String?,
    val created_by: Int,
    val dataentity: ArrayList<DataEntity>?,
    val deleted_at: Any,
    val id: Int,
    val is_active: Any,
    val name: String?,
    val parent_id: Int,
    val updated_at: String?,
    val updated_by: Int
) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.createTypedArrayList(CREATOR),
        parcel.readString(),
        parcel.readInt(),
        parcel.createTypedArrayList(DataEntity),
        TODO("deleted_at"),
        parcel.readInt(),
        TODO("is_active"),
        parcel.readString(),
        parcel.readInt(),
        parcel.readString(),
        parcel.readInt()
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeTypedList(child)
        parcel.writeString(created_at)
        parcel.writeInt(created_by)
        parcel.writeTypedList(dataentity)
        parcel.writeInt(id)
        parcel.writeString(name)
        parcel.writeInt(parent_id)
        parcel.writeString(updated_at)
        parcel.writeInt(updated_by)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<DataEntityChild> {
        override fun createFromParcel(parcel: Parcel): DataEntityChild {
            return DataEntityChild(parcel)
        }

        override fun newArray(size: Int): Array<DataEntityChild?> {
            return arrayOfNulls(size)
        }
    }
}