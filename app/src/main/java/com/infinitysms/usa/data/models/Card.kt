package com.infinitysms.usa.data.models

data class Card(
    val earning: Double,
    val total: Int
)