package com.infinitysms.usa.data.models

import android.os.Parcel
import android.os.Parcelable

data class Pivot(
    val document_id: Int,
    val public_notification_id: Int,
    val publish_date: String?
) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readInt(),
        parcel.readInt(),
        parcel.readString()
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(document_id)
        parcel.writeInt(public_notification_id)
        parcel.writeString(publish_date)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Pivot> {
        override fun createFromParcel(parcel: Parcel): Pivot {
            return Pivot(parcel)
        }

        override fun newArray(size: Int): Array<Pivot?> {
            return arrayOfNulls(size)
        }
    }
}