package com.infinitysms.usa.data.models

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

data class Designation(
    @SerializedName("created_at")
    val createdAt: String?,
    @SerializedName("created_by")
    val createdBy: Int,
    @SerializedName("deleted_at")
    val deletedAt: Any,
    @SerializedName("desg_name")
    val designationName: String?,
    val id: Int,
    @SerializedName("is_active")
    val isActive: Boolean,
    @SerializedName("parent_id")
    val parentId: Any,
    val pivot: Pivot?,
    @SerializedName("ulb_id")
    val ulbId: Int,
    @SerializedName("updated_at")
    val updatedAt: String?,
    @SerializedName("updated_by")
    val updatedBy: Int
) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readString(),
        parcel.readInt(),
        TODO("deletedAt"),
        parcel.readString(),
        parcel.readInt(),
        parcel.readByte() != 0.toByte(),
        TODO("parentId"),
        parcel.readParcelable(Pivot::class.java.classLoader),
        parcel.readInt(),
        parcel.readString(),
        parcel.readInt()
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(createdAt)
        parcel.writeInt(createdBy)
        parcel.writeString(designationName)
        parcel.writeInt(id)
        parcel.writeByte(if (isActive) 1 else 0)
        parcel.writeParcelable(pivot, flags)
        parcel.writeInt(ulbId)
        parcel.writeString(updatedAt)
        parcel.writeInt(updatedBy)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Designation> {
        override fun createFromParcel(parcel: Parcel): Designation {
            return Designation(parcel)
        }

        override fun newArray(size: Int): Array<Designation?> {
            return arrayOfNulls(size)
        }
    }
}