package com.infinitysms.usa.data.models

import android.os.Parcel
import android.os.Parcelable

data class DataEntity(
    val created_at: String?,
    val created_by: Int,
    val data_entity_category_id: Int,
    val deleted_at: Any,
    val id: Int,
    val is_active: Any,
    val parent_id: Any,
    val title: String?,
    val ulb_id: Any,
    val updated_at: String?,
    val updated_by: Int
) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readString(),
        parcel.readInt(),
        parcel.readInt(),
        TODO("deleted_at"),
        parcel.readInt(),
        TODO("is_active"),
        TODO("parent_id"),
        parcel.readString(),
        TODO("ulb_id"),
        parcel.readString(),
        parcel.readInt()
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(created_at)
        parcel.writeInt(created_by)
        parcel.writeInt(data_entity_category_id)
        parcel.writeInt(id)
        parcel.writeString(title)
        parcel.writeString(updated_at)
        parcel.writeInt(updated_by)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<DataEntity> {
        override fun createFromParcel(parcel: Parcel): DataEntity {
            return DataEntity(parcel)
        }

        override fun newArray(size: Int): Array<DataEntity?> {
            return arrayOfNulls(size)
        }
    }
}