package com.infinitysms.usa.data.models

import android.os.Parcel
import android.os.Parcelable

data class DayDuration(
    val sum: Int?
) : Parcelable {

    constructor(parcel: Parcel) : this(
        parcel.readInt()
    ) {

    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {

    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<DayDuration> {
        override fun createFromParcel(parcel: Parcel): DayDuration {
            return DayDuration(parcel)
        }

        override fun newArray(size: Int): Array<DayDuration?> {
            return arrayOfNulls(size)
        }
    }
}