package com.infinitysms.usa.data.models

import android.os.Parcel
import android.os.Parcelable

data class RegisterField(
    val custom_field_id: Int,
    val data_type: String?,
    val display_on_summary: String?,
    val hidden: Boolean,
    val html_element: String?,
    val label: String?,
    val model_id: Any,
    val model: String?,
    val name: String?,
    val options: List<Any>,
    val order: Int,
    val question: Any,
    val question_id: Any,
    val required: Boolean,
    val unit_id: Any
) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readInt(),
        parcel.readString(),
        parcel.readString(),
        parcel.readByte() != 0.toByte(),
        parcel.readString(),
        parcel.readString(),
        TODO("model_id"),
        parcel.readString(),
        parcel.readString(),
        TODO("options"),
        parcel.readInt(),
        TODO("question"),
        TODO("question_id"),
        parcel.readByte() != 0.toByte(),
        TODO("unit_id")
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(custom_field_id)
        parcel.writeString(data_type)
        parcel.writeString(display_on_summary)
        parcel.writeByte(if (hidden) 1 else 0)
        parcel.writeString(html_element)
        parcel.writeString(label)
        parcel.writeString(model)
        parcel.writeString(name)
        parcel.writeInt(order)
        parcel.writeByte(if (required) 1 else 0)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<RegisterField> {
        override fun createFromParcel(parcel: Parcel): RegisterField {
            return RegisterField(parcel)
        }

        override fun newArray(size: Int): Array<RegisterField?> {
            return arrayOfNulls(size)
        }
    }
}