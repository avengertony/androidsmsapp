package com.infinitysms.usa.data.models

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

/**
 * Created by Android Tony on 17,February,2020
 */
class EmployeeDetail (
    @SerializedName("aadhar_no")
    val aadharNo: String?,
    val address: String?,
    val code: String?,
    @SerializedName("day_duration")
    val dayDuration: List<DayDuration>?,
    val department: List<Department>?,
    val designation: List<Designation>?,
    val email: String?,
    @SerializedName("employee_attendances")
    val employeeAttendances: List<Any>,
    @SerializedName("first_name")
    val firstName: String?,
    val id: Int,
    @SerializedName("last_name")
    val lastName: String?,
    @SerializedName("middle_name")
    val middleName: Any,
    @SerializedName("mobile_no_1")
    val mobileNo1: String?,
    @SerializedName("mobile_no_2")
    val mobileNo2: String?,
    val ulb: Any,
    @SerializedName("working_shift")
    val workingShift: Any
): Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.createTypedArrayList(DayDuration),
        parcel.createTypedArrayList(Department),
        parcel.createTypedArrayList(Designation),
        parcel.readString(),
        TODO("employeeAttendances"),
        parcel.readString(),
        parcel.readInt(),
        parcel.readString(),
        TODO("middleName"),
        parcel.readString(),
        parcel.readString(),
        TODO("ulb"),
        TODO("workingShift")
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(aadharNo)
        parcel.writeString(address)
        parcel.writeString(code)
        parcel.writeTypedList(dayDuration)
        parcel.writeTypedList(department)
        parcel.writeTypedList(designation)
        parcel.writeString(email)
        parcel.writeString(firstName)
        parcel.writeInt(id)
        parcel.writeString(lastName)
        parcel.writeString(mobileNo1)
        parcel.writeString(mobileNo2)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<EmployeeDetail> {
        override fun createFromParcel(parcel: Parcel): EmployeeDetail {
            return EmployeeDetail(parcel)
        }

        override fun newArray(size: Int): Array<EmployeeDetail?> {
            return arrayOfNulls(size)
        }
    }
}