package com.infinitysms.usa.data.models

data class DriverArrived(
    val distance: Int,
    val fare_type: String,
    val price: String
)