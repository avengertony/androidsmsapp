package com.infinitysms.usa.data.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class DistanceData(
    val distance: String,
    val price: String
) : Parcelable