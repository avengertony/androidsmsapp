package com.infinitysms.usa.data.models

import android.os.Parcel
import android.os.Parcelable

data class Register(
    val category_id: Int,
    val created_at: String?,
    val created_by: Int,
    val deleted_at: Any,
    val id: Int,
    val is_active: Boolean,
    val name: String?,
    val register_fields: RegisterFields?,
    val register_type_id: Int,
    val updated_at: String?,
    val updated_by: Int
) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readInt(),
        parcel.readString(),
        parcel.readInt(),
        TODO("deleted_at"),
        parcel.readInt(),
        parcel.readByte() != 0.toByte(),
        parcel.readString(),
        parcel.readParcelable(RegisterFields::class.java.classLoader),
        parcel.readInt(),
        parcel.readString(),
        parcel.readInt()
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(category_id)
        parcel.writeString(created_at)
        parcel.writeInt(created_by)
        parcel.writeInt(id)
        parcel.writeByte(if (is_active) 1 else 0)
        parcel.writeString(name)
        parcel.writeParcelable(register_fields, flags)
        parcel.writeInt(register_type_id)
        parcel.writeString(updated_at)
        parcel.writeInt(updated_by)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Register> {
        override fun createFromParcel(parcel: Parcel): Register {
            return Register(parcel)
        }

        override fun newArray(size: Int): Array<Register?> {
            return arrayOfNulls(size)
        }
    }
}