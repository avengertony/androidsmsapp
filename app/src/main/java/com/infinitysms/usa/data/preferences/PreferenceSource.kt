package com.infinitysms.usa.data.preferences

import com.infinitysms.usa.data.models.MessageData
import com.infinitysms.usa.data.models.User

/**
 * Created by Android Tony on 23,March,2020
 */
interface PreferenceSource {
    fun clearUserData()
    fun saveUserData(user: User)
    fun getUserData(): User?
    fun getDriverOnline(): Boolean
    fun setDriverOnline(boolean: Boolean)
    fun startDriverOnline(value: Int)
    fun isDriverStatusOnline():Int
    fun saveDeviceToken(token: String)
    fun saveImageBaseUrl(imageBaseUrl: String)
    fun getImagesBaseUrl():String?
    fun getDeviceToken(): String?
    fun saveAuthToken(authToken: String)
    fun saveCompanyID(companyid: String)
    fun savePhoneNum(vehicleid: String)
    fun getAuthToken(): String?
    fun getCompanyId(): String?
    fun getPhoneNumbers(): String?
    fun saveIsIn(isIn: Boolean)
    fun getIsIn(): Boolean
    fun getMessageList(): HashSet<MessageData>?
    fun setMessageList(list:HashSet<MessageData>?)
    }