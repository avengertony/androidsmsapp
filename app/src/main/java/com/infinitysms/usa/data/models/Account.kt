package com.infinitysms.usa.data.models

data class Account(
    val earning: Double,
    val total: Int
)