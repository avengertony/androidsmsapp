package com.infinitysms.usa.data

import com.infinitysms.usa.data.preferences.PreferenceSource
import com.infinitysms.usa.data.remote.api_service.IApiService

/**
 * Created byAndroid Tony on 23,March,2020
 */
interface DataManager : IApiService, PreferenceSource