package com.infinitysms.usa.data.models

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

data class Department(
    @SerializedName("colour_code")
    val colourCode: Any,
    @SerializedName("created_at")
    val createdAt: String?,
    @SerializedName("created_by")
    val createdBy: Int,
    @SerializedName("deleted_at")
    val deletedAt: Any,
    @SerializedName("dept_name")
    val deptName: String?,
    val icon: Any,
    val id: Int,
    @SerializedName("is_active")
    val isActive: Boolean,
    val pivot: Pivot?,
    @SerializedName("ulb_id")
    val ulbId: Int,
    @SerializedName("updated_at")
    val updatedAt: String?,
    @SerializedName("updated_by")
    val updatedBy: Int
) : Parcelable {
    constructor(parcel: Parcel) : this(
        TODO("colourCode"),
        parcel.readString(),
        parcel.readInt(),
        TODO("deletedAt"),
        parcel.readString(),
        TODO("icon"),
        parcel.readInt(),
        parcel.readByte() != 0.toByte(),
        parcel.readParcelable(Pivot::class.java.classLoader),
        parcel.readInt(),
        parcel.readString(),
        parcel.readInt()
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(createdAt)
        parcel.writeInt(createdBy)
        parcel.writeString(deptName)
        parcel.writeInt(id)
        parcel.writeByte(if (isActive) 1 else 0)
        parcel.writeParcelable(pivot, flags)
        parcel.writeInt(ulbId)
        parcel.writeString(updatedAt)
        parcel.writeInt(updatedBy)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Department> {
        override fun createFromParcel(parcel: Parcel): Department {
            return Department(parcel)
        }

        override fun newArray(size: Int): Array<Department?> {
            return arrayOfNulls(size)
        }
    }
}