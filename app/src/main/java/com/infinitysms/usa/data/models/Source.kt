package com.infinitysms.usa.data.models

import android.os.Parcel
import android.os.Parcelable

data class Source(
    val booking_time: Int,
    val destination_address: String?,
    val destination_lat: Double,
    val destination_lng: Double,
    val driver_income: Double,
    val pay_type: String?,
    val source_address: String?,
    val source_lat: Double,
    val source_lng: Double,
    val status: String?,
    val utc_offset: Int,
    val is_bid: Int
):Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readInt(),
        parcel.readString(),
        parcel.readDouble(),
        parcel.readDouble(),
        parcel.readDouble(),
        parcel.readString(),
        parcel.readString(),
        parcel.readDouble(),
        parcel.readDouble(),
        parcel.readString(),
        parcel.readInt(),
        parcel.readInt()
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(booking_time)
        parcel.writeString(destination_address)
        parcel.writeDouble(destination_lat)
        parcel.writeDouble(destination_lng)
        parcel.writeDouble(driver_income)
        parcel.writeString(pay_type)
        parcel.writeString(source_address)
        parcel.writeDouble(source_lat)
        parcel.writeDouble(source_lng)
        parcel.writeString(status)
        parcel.writeInt(utc_offset)
        parcel.writeInt(is_bid)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Source> {
        override fun createFromParcel(parcel: Parcel): Source {
            return Source(parcel)
        }

        override fun newArray(size: Int): Array<Source?> {
            return arrayOfNulls(size)
        }
    }
}