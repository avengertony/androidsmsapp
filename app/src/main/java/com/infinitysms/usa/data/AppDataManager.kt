package com.infinitysms.usa.data

import android.content.Context
import com.infinitysms.usa.data.preferences.AppPreferenceManager
import com.infinitysms.usa.data.preferences.PreferenceSource
import com.infinitysms.usa.data.remote.RemoteDataManager
import com.infinitysms.usa.data.remote.api_service.IApiService
import javax.inject.Inject

/**
 * Created byAndroid Tony on 23,March,2020
 * Use this class to access/store all data from/to server or shared preference
 */
class AppDataManager @Inject
constructor(
    private var context: Context,
    private val remoteDataManager: RemoteDataManager,
    preferenceManager: AppPreferenceManager
) : DataManager, IApiService by remoteDataManager, PreferenceSource by preferenceManager {

}