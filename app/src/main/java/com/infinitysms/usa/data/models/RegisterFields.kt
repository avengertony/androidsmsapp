package com.infinitysms.usa.data.models

import android.os.Parcel
import android.os.Parcelable

data class RegisterFields(
    val public_permissions: PublicPermissions?,
    val sections: ArrayList<Section>?,
    val summary_fields: ArrayList<String>?
) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readParcelable(PublicPermissions::class.java.classLoader),
        parcel.createTypedArrayList(Section),
        parcel.createStringArrayList()
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeParcelable(public_permissions, flags)
        parcel.writeTypedList(sections)
        parcel.writeStringList(summary_fields)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<RegisterFields> {
        override fun createFromParcel(parcel: Parcel): RegisterFields {
            return RegisterFields(parcel)
        }

        override fun newArray(size: Int): Array<RegisterFields?> {
            return arrayOfNulls(size)
        }
    }
}