package com.infinitysms.usa.data.preferences

import android.content.Context
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.infinitysms.usa.data.models.MessageData
import com.infinitysms.usa.data.models.User
import com.infinitysms.usa.di.qualifiers.PreferenceInfo
import javax.inject.Inject

/**
 * Created byAndroid Tony on 23,March,2020
 * Use to store object and primitive data in shared preference using delegates,
 * by converting object into string
 */
class AppPreferenceManager
@Inject
constructor(private val context: Context, @PreferenceInfo prefFileName: String) : Preferences(),
    PreferenceSource {

    init {
        Preferences.init(context, prefFileName)
    }

    var loggedUser by stringPref("loggedUser")
    var dtoken by stringPref("token")
    var imageBaseUrl by stringPref("imageBaseUrl")
    var aToken by stringPref("authToken")
    var companyid by stringPref("companyid","")
    var phoneNumber by stringPref("phoneNumber","")
    var isIn by booleanPref("isIn")
    var isDriverOnline2 by booleanPref("isDriverOnline",false)
    var isDriverStatusOnline2 by intPref("isDriverStatusOnline",0)
    var messageList by stringPref("messageList",null)

    override fun clearUserData() {
       var phone_number= getUserData()?.phone_number
        var companyid=getUserData()?.company_id
        clearPreferences()
        saveCompanyID(companyid!!)
        savePhoneNum(phone_number!!)
    }

    override fun saveUserData(user: User) {
        loggedUser = Gson().toJson(user)
    }

    override fun getUserData(): User? {
        return Gson().fromJson(loggedUser, User::class.java)
    }

    override fun setMessageList(list: HashSet<MessageData>?) {
        messageList = if(list!=null)
            Gson().toJson(list)
        else
            null
    }
    override fun getMessageList(): HashSet<MessageData>? {
        return if(messageList!=null)
            Gson().fromJson(
                messageList,
                object : TypeToken<HashSet<MessageData>>() {}.type
            )
        else null
    }




    override fun getDriverOnline(): Boolean {
        return isDriverOnline2
    }

    override fun setDriverOnline(boolean: Boolean) {
        isDriverOnline2=boolean
    }

    override fun startDriverOnline(value: Int) {
        isDriverStatusOnline2=value
    }

    override fun isDriverStatusOnline(): Int {
        return isDriverStatusOnline2
    }




    override fun saveDeviceToken(token: String) {
        dtoken = token
    }


    override fun getCompanyId(): String? {
      return  companyid!!
    }

    override fun getPhoneNumbers(): String? {
        return phoneNumber!!
    }

    override fun saveImageBaseUrl(imageBaseUrl: String) {
        this.imageBaseUrl=imageBaseUrl
    }

    override fun getImagesBaseUrl(): String? {
        return imageBaseUrl
    }


    override fun getDeviceToken(): String? {
        return dtoken
    }

    override fun saveAuthToken(authToken: String) {
        aToken = authToken
    }


    override fun saveCompanyID(companyid: String) {
        this.companyid=companyid
    }

    override fun savePhoneNum(phoneNumber: String) {
        this.phoneNumber=phoneNumber
    }

    override fun getAuthToken(): String? {
        return aToken
    }

    override fun saveIsIn(isIn: Boolean) {
        this.isIn = isIn
    }

    override fun getIsIn(): Boolean {
        return isIn
    }

}