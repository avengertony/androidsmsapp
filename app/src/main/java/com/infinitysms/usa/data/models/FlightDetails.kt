package com.infinitysms.usa.data.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class FlightDetails(
    val airportName: String,
    val flightExpected: String,
    val flightScheduled: String,
    val flightStatus: String,
    val flightTerminal: String,
    val flightType: String,
    val luggageFlight: String,
    val numberFlight: String
): Parcelable