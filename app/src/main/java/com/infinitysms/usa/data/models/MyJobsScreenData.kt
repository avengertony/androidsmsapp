package pegasus.driver.data.model

import android.os.Parcelable
import com.infinitysms.usa.data.models.MyJobData
import kotlinx.android.parcel.Parcelize

/**
 * Created by @author Android Tony on 15/5/19.
 */

@Parcelize
data class MyJobsScreenData(
    var total_fare: Double?,
    var total_record: Int?,
    var     data: ArrayList<MyJobData>
) : Parcelable