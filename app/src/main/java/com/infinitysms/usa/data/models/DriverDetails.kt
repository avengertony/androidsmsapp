package com.infinitysms.usa.data.models

data class DriverDetails(
    val email: String,
    val firstname: String,
    val lastname: String,
    val mobileNumber: String,
    val user_image: String,
    val driver_id: String

)