package com.infinitysms.usa.data.remote

import android.content.Context
import com.google.gson.JsonObject
import com.infinitysms.usa.BuildConfig
import com.infinitysms.usa.data.preferences.AppPreferenceManager
import com.infinitysms.usa.data.remote.api_service.IApiService
import com.infinitysms.usa.utils.LogUtils
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Inject
import io.reactivex.Observable
import retrofit2.Response

/**
 * Created by Android Tony on 23,March,2020
 * Use to instantiate retrofit
 */
class RemoteDataManager @Inject constructor(private var context: Context, private val appPreferenceManager: AppPreferenceManager) :
    IApiService {

    private lateinit var iApiService: IApiService
    private lateinit var iApiService2: IApiService
    private lateinit var retrofit: Retrofit
    private lateinit var retrofit2: Retrofit

    init {
        initializeAPIService()
    }

    private fun initializeAPIService() {
        val logging = HttpLoggingInterceptor()
        logging.level = HttpLoggingInterceptor.Level.BODY

        val httpClient = UnTrustedCertificate.getUnsafeOkHttpClient()
        httpClient.addInterceptor(logging)
        httpClient.addInterceptor(UnauthorisedInterceptor(context))
        httpClient.connectTimeout(30, TimeUnit.SECONDS)
        httpClient.readTimeout(30, TimeUnit.SECONDS)
        httpClient.writeTimeout(30, TimeUnit.SECONDS)

        //addEAuthHeader(httpClient, "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1aWQiOjEsImlhdCI6MTU2MDYwNzkzM30.FwpwR1UchB20dVIjLd-jqg7-Q0HQsKLL33Avc5rGPHE")
       // addEAuthHeader(httpClient, "Bearer " + appPreferenceManager.getAuthToken())

        retrofit = Retrofit.Builder()
            .baseUrl(BuildConfig.BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .client(httpClient.build())
            .build()
        iApiService = retrofit.create(IApiService::class.java)
      retrofit2 = Retrofit.Builder()
            .baseUrl(BuildConfig.FREE_BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .client(httpClient.build())
            .build()
        iApiService2 = retrofit2.create(IApiService::class.java)
    }

    private fun addEAuthHeader(httpClient: OkHttpClient.Builder, authToken: String?) {
        httpClient.addInterceptor(object : Interceptor {
            override fun intercept(chain: Interceptor.Chain?): okhttp3.Response? {
                val original = chain?.request()

                var requestBuilder: Request.Builder? = null
                if (authToken == null) {
                    /*requestBuilder = original?.newBuilder()?.header("Content-Type", "application/json")
                        ?.header("NST", context.getString(R.string.jwt_token))
                        ?.header("DeviceId", Helper.getSerialNumber())*/
                } else  {
                    requestBuilder = original?.newBuilder()?.header("Content-Type", "application/json")
                        ?.header("Authorization", authToken)
                    LogUtils.e("RemoteDataManager", "Token: " + authToken)
                }

                val request = requestBuilder?.build()
                return chain?.proceed(request)
            }
        })
    }

    override fun getRegisterCategories(): Observable<Response<JsonObject>> {
        return iApiService.getRegisterCategories()
    }

    override fun getMessages(jsonObject: JsonObject): Observable<Response<JsonObject>> {
        return iApiService2.getMessages(jsonObject)
    }


    override fun sendMessage(jsonObject: JsonObject): Observable<Response<JsonObject>> {
        return iApiService2.sendMessage(jsonObject)
    }

    override fun getNotifications(): Observable<Response<JsonObject>> {
        return iApiService.getNotifications()
    }

    override fun login(jsonObject: JsonObject): Observable<Response<JsonObject>> {
        return iApiService.login(jsonObject)
    }

    override fun freeDriver(jsonObject: JsonObject): Observable<Response<JsonObject>> {
        return iApiService2.freeDriver(jsonObject)
    }

    override fun updateDriverStatus(
        jsonObject: JsonObject
    ): Observable<Response<JsonObject>> {
        return iApiService.updateDriverStatus(jsonObject)
    }

    override fun updateBookingStatus(jsonObject: JsonObject): Observable<Response<JsonObject>> {
       return iApiService2.updateBookingStatus(jsonObject)
    }

    override fun callApplyBid(jsonObject: JsonObject): Observable<Response<JsonObject>> {
        return iApiService2.callApplyBid(jsonObject)

    }

    override fun finishBookingStatus(jsonObject: JsonObject): Observable<Response<JsonObject>> {
       return iApiService2.finishBookingStatus(jsonObject)
    }
override fun completeBooking(jsonObject: JsonObject): Observable<Response<JsonObject>> {
       return iApiService2.completeBooking(jsonObject)
    }

    override fun getMyProfile(jsonObject: JsonObject): Observable<Response<JsonObject>> {
        return iApiService2.getMyProfile(jsonObject)
    }

    override fun getBookingData(jsonObject: JsonObject): Observable<Response<JsonObject>> {
       return iApiService2.getBookingData(jsonObject)
    }

   override fun getZoneList(jsonObject: JsonObject): Observable<Response<JsonObject>> {
       return iApiService.getZoneList(jsonObject)
    }

   override fun getMyJobsList(jsonObject: JsonObject): Observable<Response<JsonObject>> {
       return iApiService2.getMyJobsList(jsonObject)
    }


   override fun getPaymentHistory(jsonObject: JsonObject): Observable<Response<JsonObject>> {
       return iApiService2.getPaymentHistory(jsonObject)
    }



    override fun getEmployeeList(): Observable<Response<JsonObject>> {
        return iApiService.getEmployeeList()
    }

    override fun getAttendanceStatusList(): Observable<Response<JsonObject>> {
        return iApiService.getAttendanceStatusList()
    }

    override fun updateDriverBreakStatus(jsonObject: JsonObject): Observable<Response<JsonObject>> {
        return iApiService.updateDriverBreakStatus(jsonObject)
    }
}