package com.infinitysms.usa.data.models

import android.os.Parcel
import android.os.Parcelable

data class PublicPermissions(
    val delete: Boolean,
    val read: Boolean,
    val update: Boolean,
    val write: Boolean
) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readByte() != 0.toByte(),
        parcel.readByte() != 0.toByte(),
        parcel.readByte() != 0.toByte(),
        parcel.readByte() != 0.toByte()
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeByte(if (delete) 1 else 0)
        parcel.writeByte(if (read) 1 else 0)
        parcel.writeByte(if (update) 1 else 0)
        parcel.writeByte(if (write) 1 else 0)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<PublicPermissions> {
        override fun createFromParcel(parcel: Parcel): PublicPermissions {
            return PublicPermissions(parcel)
        }

        override fun newArray(size: Int): Array<PublicPermissions?> {
            return arrayOfNulls(size)
        }
    }
}