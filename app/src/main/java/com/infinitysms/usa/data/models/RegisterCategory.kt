package com.infinitysms.usa.data.models

import android.os.Parcel
import android.os.Parcelable

data class RegisterCategory(
    val childs: ArrayList<Child>?,
    val created_at: String?,
    val created_by: Int,
    val deleted_at: Any,
    val id: Int,
    val is_active: Boolean,
    val name: String?,
    val parent_id: Any,
    val registers: ArrayList<Register>?,
    val updated_at: String?,
    val updated_by: Int
) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.createTypedArrayList(Child),
        parcel.readString(),
        parcel.readInt(),
        TODO("deleted_at"),
        parcel.readInt(),
        parcel.readByte() != 0.toByte(),
        parcel.readString(),
        TODO("parent_id"),
        parcel.createTypedArrayList(Register),
        parcel.readString(),
        parcel.readInt()
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeTypedList(childs)
        parcel.writeString(created_at)
        parcel.writeInt(created_by)
        parcel.writeInt(id)
        parcel.writeByte(if (is_active) 1 else 0)
        parcel.writeString(name)
        parcel.writeTypedList(registers)
        parcel.writeString(updated_at)
        parcel.writeInt(updated_by)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<RegisterCategory> {
        override fun createFromParcel(parcel: Parcel): RegisterCategory {
            return RegisterCategory(parcel)
        }

        override fun newArray(size: Int): Array<RegisterCategory?> {
            return arrayOfNulls(size)
        }
    }
}