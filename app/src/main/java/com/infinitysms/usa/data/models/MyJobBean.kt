package com.infinitysms.usa.data.models

data class MyJobBean(
    val `data`: List<MyJobData>,
    val message: String,
    val status: Boolean,
    val total_record: Int
)