package com.infinitysms.usa.data.models

import android.os.Parcel
import android.os.Parcelable

/**
 * Created byAndroid Tony on 23,March,2020
 */
class UIState(
    var loading: Boolean = false, var success: Boolean = false,
    var message: String? = null, var messageWithAlert: String? = null,
    var error: String? = null
) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readByte() != 0.toByte(),
        parcel.readByte() != 0.toByte(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString()
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeByte(if (loading) 1 else 0)
        parcel.writeByte(if (success) 1 else 0)
        parcel.writeString(message)
        parcel.writeString(messageWithAlert)
        parcel.writeString(error)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<UIState> {
        override fun createFromParcel(parcel: Parcel): UIState {
            return UIState(parcel)
        }

        override fun newArray(size: Int): Array<UIState?> {
            return arrayOfNulls(size)
        }
    }

}
