package com.infinitysms.usa.data.models

data class VehicleDetails(
    val colour: String,
    val vehicle_random_id: String,
    val vehicle_number: String
)