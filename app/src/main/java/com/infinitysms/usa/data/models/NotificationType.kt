package com.infinitysms.usa.data.models

import android.os.Parcel
import android.os.Parcelable

data class NotificationType(
    val created_at: String?,
    val created_by: Int,
    val deleted_at: Any,
    val id: Int,
    val is_active: Boolean,
    val name: String?,
    val parent_id: Any,
    val updated_at: String?,
    val updated_by: Int
) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readString(),
        parcel.readInt(),
        TODO("deleted_at"),
        parcel.readInt(),
        parcel.readByte() != 0.toByte(),
        parcel.readString(),
        TODO("parent_id"),
        parcel.readString(),
        parcel.readInt()
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(created_at)
        parcel.writeInt(created_by)
        parcel.writeInt(id)
        parcel.writeByte(if (is_active) 1 else 0)
        parcel.writeString(name)
        parcel.writeString(updated_at)
        parcel.writeInt(updated_by)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<NotificationType> {
        override fun createFromParcel(parcel: Parcel): NotificationType {
            return NotificationType(parcel)
        }

        override fun newArray(size: Int): Array<NotificationType?> {
            return arrayOfNulls(size)
        }
    }
}