package com.infinitysms.usa.data.models

data class ProfileBean(
    val driver_details: DriverDetails,
    val driver_documents: List<DriverDocument>,
    val driver_rating: Double,
    val message: String,
    val status: Boolean,
    val vehicle_details: VehicleDetails

)