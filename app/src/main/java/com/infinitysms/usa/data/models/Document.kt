package com.infinitysms.usa.data.models

import android.os.Parcel
import android.os.Parcelable

data class Document(
    val created_at: String?,
    val created_by: Int,
    val deleted_at: Any,
    val document_alias: String?,
    val document_file: Any,
    val document_file_type: String?,
    val document_location: String?,
    val document_name: String?,
    val document_type_id: Int,
    val entity_id: Any,
    val entity_type_id: Int,
    val id: Int,
    val is_active: Any,
    val pivot: Pivot?,
    val ulb_id: Int,
    val updated_at: String?,
    val updated_by: Int
) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readString(),
        parcel.readInt(),
        TODO("deleted_at"),
        parcel.readString(),
        TODO("document_file"),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readInt(),
        TODO("entity_id"),
        parcel.readInt(),
        parcel.readInt(),
        TODO("is_active"),
        parcel.readParcelable(Pivot::class.java.classLoader),
        parcel.readInt(),
        parcel.readString(),
        parcel.readInt()
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(created_at)
        parcel.writeInt(created_by)
        parcel.writeString(document_alias)
        parcel.writeString(document_file_type)
        parcel.writeString(document_location)
        parcel.writeString(document_name)
        parcel.writeInt(document_type_id)
        parcel.writeInt(entity_type_id)
        parcel.writeInt(id)
        parcel.writeParcelable(pivot, flags)
        parcel.writeInt(ulb_id)
        parcel.writeString(updated_at)
        parcel.writeInt(updated_by)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Document> {
        override fun createFromParcel(parcel: Parcel): Document {
            return Document(parcel)
        }

        override fun newArray(size: Int): Array<Document?> {
            return arrayOfNulls(size)
        }
    }
}