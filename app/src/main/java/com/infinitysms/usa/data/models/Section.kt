package com.infinitysms.usa.data.models

import android.os.Parcel
import android.os.Parcelable

data class Section(
    val id: String?,
    val order: Int,
    val register_fields: ArrayList<RegisterField>?,
    val section: List<Any>,
    val section_name: String?
) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readString(),
        parcel.readInt(),
        parcel.createTypedArrayList(RegisterField),
        TODO("section"),
        parcel.readString()
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(id)
        parcel.writeInt(order)
        parcel.writeTypedList(register_fields)
        parcel.writeString(section_name)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Section> {
        override fun createFromParcel(parcel: Parcel): Section {
            return Section(parcel)
        }

        override fun newArray(size: Int): Array<Section?> {
            return arrayOfNulls(size)
        }
    }
}