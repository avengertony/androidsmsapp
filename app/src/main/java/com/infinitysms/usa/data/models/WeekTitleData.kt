package com.infinitysms.usa.data.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize



@Parcelize
data class WeekTitleData(var title: String?, var millisFirstDay: Long, var millisLastDay: Long) :
    Parcelable

