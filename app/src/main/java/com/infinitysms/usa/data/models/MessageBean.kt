package com.infinitysms.usa.data.models

data class MessageBean(
    val company_id: Int,
    val id: Int,
    val msg: String,
    val userid: Int,
    var selected:Boolean=false
)