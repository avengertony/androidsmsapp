package pegasus.driver.data.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Dropup(
    val dest_address: String,
    val dest_lat: String,
    val dest_long: String,
    val dest_note: String
) : Parcelable