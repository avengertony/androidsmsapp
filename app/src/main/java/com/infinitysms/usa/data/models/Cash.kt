package com.infinitysms.usa.data.models

data class Cash(
    val earning: Double,
    val total: Int
)