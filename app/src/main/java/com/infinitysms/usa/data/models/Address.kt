package com.infinitysms.usa.data.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

/**
 * Created by Android Tony on 17/5/20.
 */
@Parcelize
data class Address(
    val type: Int,
    val address: String,
    val note: String
) : Parcelable