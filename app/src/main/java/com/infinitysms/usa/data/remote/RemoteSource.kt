package com.infinitysms.usa.data.remote

import com.google.gson.JsonObject
import io.reactivex.Observable
import retrofit2.Response

/**
 * Created byAndroid Tony on 23,March,2020
 */
interface RemoteSource {

    fun getRegisterCategories(): Observable<Response<JsonObject>>

    fun login(jsonObject: JsonObject): Observable<Response<JsonObject>>
}