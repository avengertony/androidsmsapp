package com.infinitysms.usa.data.interfaces

import org.json.JSONObject

interface WebSocketListener {
    fun onConnectSuccessfully()
    fun onDissconnectSuccessfully()
    fun onServerError(message: String)



    fun onInboxMessage(
        notification_type: String, json: JSONObject
    )


    fun onCloseConnection(closeConn: Boolean)

    fun onCloseFailed(e: Throwable)

    fun onMessageShow(message: String)
}