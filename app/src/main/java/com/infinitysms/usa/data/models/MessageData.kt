package com.infinitysms.usa.data.models

data class MessageData(
    val company_id: Int,
    val message: String?,
    val phone_number: String?,
    val status: String?,
    val code: Int=0
)