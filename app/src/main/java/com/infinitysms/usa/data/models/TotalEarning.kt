package com.infinitysms.usa.data.models

import android.os.Parcel
import android.os.Parcelable

data class TotalEarning(
    val card: Card,
    val cash: Cash,
    val account: Account
):Parcelable {
    constructor(parcel: Parcel) : this(
        TODO("card"),
        TODO("cash"),
        TODO("total")
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {

    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<TotalEarning> {
        override fun createFromParcel(parcel: Parcel): TotalEarning {
            return TotalEarning(parcel)
        }

        override fun newArray(size: Int): Array<TotalEarning?> {
            return arrayOfNulls(size)
        }
    }
}