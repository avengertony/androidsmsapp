package com.infinitysms.usa.data.models

data class DriverDocument(
    val expirydate: String,
    val meta_data: String,
    val uploadDoc: String
)