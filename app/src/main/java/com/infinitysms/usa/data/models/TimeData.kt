package com.infinitysms.usa.data.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class TimeData(
    val price: String,
    val time: String
) : Parcelable