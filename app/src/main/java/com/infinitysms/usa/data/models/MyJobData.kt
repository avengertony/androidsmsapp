package com.infinitysms.usa.data.models

import android.os.Parcel
import android.os.Parcelable

data class MyJobData(
    val _id: String?,
    val _index: String?,
    val _score: Any,
    val _source: Source,
    val _type: String?,
    val sort: List<Int>
):Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readString(),
        parcel.readString(),
        TODO("_score"),
        TODO("_Message_source"),
        parcel.readString(),
        TODO("sort")
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(_id)
        parcel.writeString(_index)
        parcel.writeString(_type)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<MyJobData> {
        override fun createFromParcel(parcel: Parcel): MyJobData {
            return MyJobData(parcel)
        }

        override fun newArray(size: Int): Array<MyJobData?> {
            return arrayOfNulls(size)
        }
    }
}