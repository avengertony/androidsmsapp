package com.infinitysms.usa.data.models

import android.os.Parcel
import android.os.Parcelable

data class NotificationConfiguration(
    val action_plan_document: Boolean,
    val created_at: String?,
    val created_by: Int,
    val date: Boolean,
    val deleted_at: Any,
    val id: Int,
    val is_active: Boolean,
    val name: String?,
    val newspaper_document: Boolean,
    val notification_type: NotificationType?,
    val notification_type_id: Int,
    val social_media_link: Boolean,
    val updated_at: String?,
    val updated_by: Int
) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readByte() != 0.toByte(),
        parcel.readString(),
        parcel.readInt(),
        parcel.readByte() != 0.toByte(),
        TODO("deleted_at"),
        parcel.readInt(),
        parcel.readByte() != 0.toByte(),
        parcel.readString(),
        parcel.readByte() != 0.toByte(),
        parcel.readParcelable(NotificationType::class.java.classLoader),
        parcel.readInt(),
        parcel.readByte() != 0.toByte(),
        parcel.readString(),
        parcel.readInt()
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeByte(if (action_plan_document) 1 else 0)
        parcel.writeString(created_at)
        parcel.writeInt(created_by)
        parcel.writeByte(if (date) 1 else 0)
        parcel.writeInt(id)
        parcel.writeByte(if (is_active) 1 else 0)
        parcel.writeString(name)
        parcel.writeByte(if (newspaper_document) 1 else 0)
        parcel.writeParcelable(notification_type, flags)
        parcel.writeInt(notification_type_id)
        parcel.writeByte(if (social_media_link) 1 else 0)
        parcel.writeString(updated_at)
        parcel.writeInt(updated_by)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<NotificationConfiguration> {
        override fun createFromParcel(parcel: Parcel): NotificationConfiguration {
            return NotificationConfiguration(parcel)
        }

        override fun newArray(size: Int): Array<NotificationConfiguration?> {
            return arrayOfNulls(size)
        }
    }
}