package com.infinitysms.usa.data.remote.api_service

import com.google.gson.JsonObject
import io.reactivex.Observable
import retrofit2.Response
import retrofit2.http.*

/**
 * Created byAndroid Tony on 23,March,2020
 */
interface IApiService {

    @GET("register_categories")
    fun getRegisterCategories(): Observable<Response<JsonObject>>

    @POST("driver-messages")
    fun getMessages(@Body jsonObject: JsonObject): Observable<Response<JsonObject>>

   @POST("send-message-to-company")
    fun sendMessage(@Body jsonObject: JsonObject): Observable<Response<JsonObject>>

    @GET("notifications")
    fun getNotifications(): Observable<Response<JsonObject>>

    @POST("login")
    fun login(@Body jsonObject: JsonObject): Observable<Response<JsonObject>>


    @POST("free-vehicle-test")
    fun freeDriver(@Body jsonObject: JsonObject): Observable<Response<JsonObject>>


    @POST("driver-status")
    fun updateDriverStatus(@Body jsonObject: JsonObject): Observable<Response<JsonObject>>


    @POST("update-booking-status")
    fun updateBookingStatus(@Body jsonObject: JsonObject): Observable<Response<JsonObject>>


    @POST("applyonbid")
    fun callApplyBid(@Body jsonObject: JsonObject): Observable<Response<JsonObject>>

    @POST("getbookingdata")
    fun getBookingData(@Body jsonObject: JsonObject): Observable<Response<JsonObject>>

    @POST("break-api-driver")
    fun updateDriverBreakStatus(@Body jsonObject: JsonObject): Observable<Response<JsonObject>>


    @POST("finishbooking")
    fun finishBookingStatus(@Body jsonObject: JsonObject): Observable<Response<JsonObject>>

 @POST("completebooking")
    fun completeBooking(@Body jsonObject: JsonObject): Observable<Response<JsonObject>>


    @POST("driver-profile")
    fun getMyProfile(@Body jsonObject: JsonObject): Observable<Response<JsonObject>>


    @POST("driver-profile")
    fun getZoneList(@Body jsonObject: JsonObject): Observable<Response<JsonObject>>


    @POST("list-bookings")
    fun getMyJobsList(@Body jsonObject: JsonObject): Observable<Response<JsonObject>>

    @POST("payment-history-driver")
    fun getPaymentHistory(@Body jsonObject: JsonObject): Observable<Response<JsonObject>>

    @GET("zones")
    fun getEmployeeList(): Observable<Response<JsonObject>>

    @GET("attendance_status")
    fun getAttendanceStatusList(): Observable<Response<JsonObject>>
}