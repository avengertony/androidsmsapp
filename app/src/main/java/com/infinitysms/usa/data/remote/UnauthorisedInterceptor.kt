package com.infinitysms.usa.data.remote

import android.app.AlertDialog
import android.content.Context
import com.infinitysms.usa.R
import com.infinitysms.usa.ui.auth.AuthActivity
import okhttp3.Interceptor
import okhttp3.Response

/**
 * Created by Android Tony on 26,February,2020
 */
class UnauthorisedInterceptor(var context: Context) : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        val response = chain.proceed(chain.request())
        if (response.code() == 403) {
            AlertDialog.Builder(context).setCancelable(false)
                .setTitle(context.resources.getString(R.string.authorization_error))
                .setMessage(context.resources.getString(R.string.your_session_is_expired))
                .setPositiveButton(context.resources.getString(R.string.ok)) { dialog, id ->
                    run {
                        dialog.dismiss()
                        AuthActivity.startInstanceWithBackStackCleared(context)
                    }
                }.create().show()
        }
        return response
    }
}