package com.infinitysms.usa.di.module

import com.infinitysms.usa.ui.auth.sign_in.SignInFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

/**
 * Created byAndroid Tony on 23,March,2020
 */
@Module
abstract class AuthFragmentBindingModule {

    @ContributesAndroidInjector
    abstract fun signInFragment(): SignInFragment


}