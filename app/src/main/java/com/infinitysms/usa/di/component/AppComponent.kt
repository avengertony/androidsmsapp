package com.infinitysms.usa.di.component

import android.app.Application
import com.infinitysms.usa.MyApplication
import com.infinitysms.usa.di.module.ActivityBindingModule
import com.infinitysms.usa.di.module.MyApplicationModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

/**
 * Created byAndroid Tony on 23,March,2020
 */
@Singleton
@Component(
    modules = [(MyApplicationModule::class),
        (ActivityBindingModule::class),
        (AndroidSupportInjectionModule::class)]
)
interface AppComponent : AndroidInjector<MyApplication> {

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(app: Application): Builder

        fun build(): AppComponent
    }

    override fun inject(application: MyApplication)
}