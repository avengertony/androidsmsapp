package com.infinitysms.usa.di.module

import android.app.Application
import android.content.Context
import com.infinitysms.usa.data.preferences.AppPreferenceManager
import com.infinitysms.usa.data.preferences.PreferenceSource
import com.infinitysms.usa.di.qualifiers.PreferenceInfo
import com.infinitysms.usa.utils.Constants.PREF_NAME
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/**
 * Created byAndroid Tony on 23,March,2020
 */
@Module(includes = [ActivityViewModelModule::class])
class MyApplicationModule {

    @Provides
    @Singleton
    internal fun bindContext(application: Application): Context = application

    @Provides
    @Singleton
    internal fun getPreferenceSource(appPreferenceManager: AppPreferenceManager):
            PreferenceSource = appPreferenceManager

    @Provides
    @PreferenceInfo
    internal fun providePreferenceName(): String {
        return PREF_NAME
    }
}