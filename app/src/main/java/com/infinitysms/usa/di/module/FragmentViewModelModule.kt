package com.infinitysms.usa.di.module

import androidx.lifecycle.ViewModel
import com.infinitysms.usa.di.key.ViewModelKey
import com.infinitysms.usa.ui.auth.sign_in.SignInFragmentVM
import com.infinitysms.usa.ui.dashboard.alerts.AlertsFragmentVM
import com.infinitysms.usa.ui.dashboard.my_profile.MyProfileFragmentVM
import com.infinitysms.usa.ui.dashboard.home.HomeFragmentVM
import com.infinitysms.usa.ui.dashboard.myjobs.MyJobsFragmentVM
import com.infinitysms.usa.ui.dashboard.nav_drawer.NavDrawerHomeFragmentVM
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

/**
 * Created byAndroid Tony on 23,March,2020
 */
@Module
abstract class FragmentViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(SignInFragmentVM::class)
    abstract fun bindSignInFragmentVM(signInFragmentVM: SignInFragmentVM): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(HomeFragmentVM::class)
    abstract fun bindHomeFragmentVM(homeFragmentVM: HomeFragmentVM): ViewModel


    @Binds
    @IntoMap
    @ViewModelKey(AlertsFragmentVM::class)
    abstract fun bindAlertsFragmentVM(alertsFragmentVM: AlertsFragmentVM): ViewModel


    @Binds
    @IntoMap
    @ViewModelKey(NavDrawerHomeFragmentVM::class)
    abstract fun bindNavDrawerHomeFragmentVM(navDrawerHomeFragmentVM: NavDrawerHomeFragmentVM): ViewModel



    @Binds
    @IntoMap
    @ViewModelKey(MyJobsFragmentVM::class)
    abstract fun bindRegisterFormFragmentVM(myJobsFragmentVM: MyJobsFragmentVM): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(MyProfileFragmentVM::class)
    abstract fun bindRegistrationEmployeeDetailFragmentVM(myProfileFragmentVM: MyProfileFragmentVM): ViewModel


}