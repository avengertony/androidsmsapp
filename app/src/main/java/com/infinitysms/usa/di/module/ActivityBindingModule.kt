package com.infinitysms.usa.di.module

import com.infinitysms.usa.ui.auth.AuthActivity
import com.infinitysms.usa.ui.auth.AuthActivityModule
import com.infinitysms.usa.ui.dashboard.DashboardActivity
import com.infinitysms.usa.ui.dashboard.DashboardActivityModule
import com.infinitysms.usa.ui.splash.SplashActivity
import com.infinitysms.usa.ui.splash.SplashActivityModule
import dagger.Module
import dagger.android.ContributesAndroidInjector

/**
 * Created byAndroid Tony on 23,March,2020
 */
@Module
abstract class ActivityBindingModule {

    @ContributesAndroidInjector(modules = [SplashActivityModule::class, SplashFragmentBindingModule::class])
    @SplashActivityModule.ActivityScoped
    abstract fun splashActivity(): SplashActivity

    @ContributesAndroidInjector(modules = [AuthActivityModule::class, AuthFragmentBindingModule::class])
    @AuthActivityModule.ActivityScoped
    abstract fun authActivity(): AuthActivity

    @ContributesAndroidInjector(modules = [DashboardActivityModule::class, DashboardFragmentBindingModule::class])
    @DashboardActivityModule.ActivityScoped
    abstract fun dashboardActivityModule(): DashboardActivity


}