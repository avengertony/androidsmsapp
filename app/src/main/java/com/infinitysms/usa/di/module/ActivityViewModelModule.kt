package com.infinitysms.usa.di.module

import androidx.lifecycle.ViewModel
import com.infinitysms.usa.di.key.ViewModelKey
import com.infinitysms.usa.ui.auth.AuthActivityVM
import com.infinitysms.usa.ui.dashboard.DashboardActivityVM
import com.infinitysms.usa.ui.splash.SplashActivityViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

/**
 * Created byAndroid Tony on 23,March,2020
 */
@Module(includes = [FragmentViewModelModule::class])
abstract class ActivityViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(SplashActivityViewModel::class)
    abstract fun bindSplashViewModel(splashActivityViewModel: SplashActivityViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(AuthActivityVM::class)
    abstract fun bindAuthViewModel(authViewModel: AuthActivityVM): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(DashboardActivityVM::class)
    abstract fun bindDashboardActivityVM(dashboardActivityVM: DashboardActivityVM): ViewModel


}