package com.infinitysms.usa.di.module

import com.infinitysms.usa.ui.dashboard.alerts.AlertsFragment
import com.infinitysms.usa.ui.dashboard.home.HomeFragment
import com.infinitysms.usa.ui.dashboard.my_profile.MyProfileFragment
import com.infinitysms.usa.ui.dashboard.myjobs.MyJobsFragment
import com.infinitysms.usa.ui.dashboard.nav_drawer.NavDrawerHomeFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

/**
 * Created byAndroid Tony on 23,March,2020
 */
@Module
abstract class DashboardFragmentBindingModule {

    @ContributesAndroidInjector
    abstract fun homeFragment(): HomeFragment


    @ContributesAndroidInjector
    abstract fun alertsFragment(): AlertsFragment


    @ContributesAndroidInjector
    abstract fun navDrawerHomeFragment(): NavDrawerHomeFragment


    @ContributesAndroidInjector
    abstract fun registerFormFragment(): MyJobsFragment


    @ContributesAndroidInjector
    abstract fun registrationEmployeeDetailFragment(): MyProfileFragment


}