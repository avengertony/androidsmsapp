package com.infinitysms.usa.ui.base

import android.content.Context
import android.os.Build
import android.os.Bundle
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModel
import androidx.navigation.NavController
import com.infinitysms.usa.R
import com.infinitysms.usa.utils.Helper
import com.infinitysms.usa.utils.LocaleHelper
import com.infinitysms.usa.utils.NetworkUtils
import dagger.android.AndroidInjection
import java.util.*

/**
 * Created byAndroid Tony on 23,March,2020
 */
abstract class BaseAuthActivity<out V : ViewModel> : AppCompatActivity(), BaseAuthFragment.Callback {

    private lateinit var mViewModel: V
    var allowTransaction = true
    lateinit var helper: Helper
    var fragment: BaseAuthFragment<*>? = null
    private var languageCode = Locale.getDefault().language

    override fun onCreate(savedInstanceState: Bundle?) {
        performDependencyInjection()

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            if (LocaleHelper.isRTL(Locale.getDefault())) {
                window.decorView.layoutDirection = View.LAYOUT_DIRECTION_RTL
            } else {
                window.decorView.layoutDirection = View.LAYOUT_DIRECTION_LTR
            }
        }

        helper = Helper(this)
        super.onCreate(savedInstanceState)
    }

    override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(base?.let { LocaleHelper.onAttach(it) })
    }

    override fun onFragmentAttached(fragment: BaseAuthFragment<*>) {
        this.fragment = fragment
    }

    override fun onFragmentDetached(tag: String) {
        this.fragment = null
    }

    override fun onTransitionStarted() {
        allowTransaction = false
    }

    override fun onTransitionEnded() {
        allowTransaction = true
    }

    override fun onTransitionRepeated() {
        allowTransaction = false
    }

    override fun onResume() {
        super.onResume()
        allowTransaction = true
       /* if (languageCode === Locale.getDefault().language) {
            return
        } else {
         //   recreate()
        }*/
    }

    override fun onPause() {
        super.onPause()
        languageCode = Locale.getDefault().language
    }

    override fun onStop() {
        super.onStop()
        allowTransaction = false
    }

    private fun performDependencyInjection() {
        AndroidInjection.inject(this)
        mViewModel = getViewModel()
    }

    /**Use to check internet connectivity*/
    fun isNetworkConnected(): Boolean {
        val flag = NetworkUtils.isNetworkConnected(applicationContext)
        if (!flag) {
            helper.showWarningToast(resources.getString(R.string.please_check_internet_connection))
        }
        return flag
    }

    /**Use to hide keyboard anywhere in app*/
    fun hideKeyboard() {
        val view: View? = this.currentFocus
        val inputMethodManager: InputMethodManager = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        inputMethodManager.hideSoftInputFromWindow(view?.windowToken, 0)

        //getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN)
    }

    fun showShortToast(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }

    /**Use to show error message in toast*/
    fun showErrorToast(message: String) {
        helper.showErrorToast(message)
    }

    /**Use to show info message in toast*/
    fun showInfoToast(message: String) {
        helper.showInfoToast(message)
    }

    /**Use to show warning message in toast*/
    fun showWarningToast(message: String) {
        helper.showWarningToast(message)
    }

    /**Use to show success message in toast*/
    fun showSuccessToast(message: String) {
        helper.showSuccessToast(message)
    }

    fun showCustomAlertDialog(title: String, message: String) {
        //helper.showCustomAlertDialog(title, message)
    }

    /**Use to show message in default alert dialog*/
    fun showDefaultAlertDialog(title: String, message: String) {
        helper.showAlertDialog(title, message)
    }

    /**Use to show loading when don't allow to move from screen
     * while api calling*/
    fun showLoadingDialog() {
        if(helper!=null)
        helper.showLoadingDialog()
    }

    /**Use to hide loading when don't allow to move from screen
     * while api calling*/
    fun dismissLoadingDialog() {
        helper.dismissLoadingDialog()
    }

    fun showNotificationDialog() {
        /* navigationController.launchNotificationDialogFragment(this)*/
    }

    /**Use to change application language*/
    open fun updateLocale(lang: String) {
        LocaleHelper.setLocale(this, lang)
        languageCode = lang
     //   recreate()
    }

    /**Use to open or close navigation drawer*/
    open fun toggleNavDrawer() {

    }

    /**Use to don't allow to open nav draser*/
    open fun lockedNavDrawer() {

    }

    /**Use to allow to open nav draser*/
    open fun unLockedNavDrawer() {

    }

    /**Use to hide bottom navigation in full screen where back button is enable*/
    open fun hideBottomNavigation() {

    }

    /**Use to show bottom navigation after coming back to dashboard*/
    open fun showBottomNavigation() {

    }

    /**Use to get dashboard nav controller*/
    open fun getDashboardNavController(): NavController? {
        return null
    }

    open fun showPopupMenu(v: View) {

    }
    override fun onBackPressed() {
        if(fragment?.onBackPressed() == null || !fragment?.onBackPressed()!!) {
            super.onBackPressed()
        }
    }

    /**
     * Override for set view model
     * @return view model instance
     */
    abstract fun getViewModel(): V
}