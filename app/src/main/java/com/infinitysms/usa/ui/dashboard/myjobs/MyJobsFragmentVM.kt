package com.infinitysms.usa.ui.dashboard.myjobs

import android.content.Context
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.gson.Gson
import com.google.gson.JsonObject
import com.infinitysms.usa.R
import com.infinitysms.usa.data.AppDataManager
import com.infinitysms.usa.data.models.MyJobBean
import com.infinitysms.usa.data.models.UIState
import com.infinitysms.usa.data.models.WeekTitleData
import com.infinitysms.usa.utils.Constants
import com.infinitysms.usa.utils.LogUtils
import default
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableObserver
import io.reactivex.schedulers.Schedulers
import org.json.JSONObject
import retrofit2.Response
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject

/**
 * Created by Android Tony on 17,January,2020
 */
class MyJobsFragmentVM @Inject constructor(
    private var context: Context,
    var appDataManager: AppDataManager
) :
    ViewModel() {

    private val TAG = MyJobsFragmentVM::class.java.simpleName
    private val millisInAWeek: Long = 604800000

    var uiState = MutableLiveData<UIState>().default(UIState(loading = false))
var myJobBean  = MutableLiveData<MyJobBean>()

    private var compositeDisposable = CompositeDisposable()

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.dispose()
    }

    fun getMyJobsList(startTime: Long, endTime: Long,day_id:Int) {
        try {
            uiState.value = UIState(loading = true)
            val jsonObject = JsonObject()
            jsonObject.addProperty("from_timestamp", startTime/1000L)  //into unix time
            jsonObject.addProperty("to_timestamp", endTime/1000L-60L)     //into unix time, 1 minuteback
            jsonObject.addProperty("company_id", appDataManager.getUserData()?.company_id!!)
            jsonObject.addProperty("vehicle_id", appDataManager.getUserData()?.phone_number  !!)

            jsonObject.addProperty("day_id", day_id)

            val loginDisposable = appDataManager.getMyJobsList(jsonObject)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(object : DisposableObserver<Response<JsonObject>>() {
                    override fun onComplete() {
                        uiState.value = UIState(loading = false)
                    }

                    override fun onNext(response: Response<JsonObject>) {
                        try {
                            if (response.code() == 200 && response.body() != null) {
                                if (response.code() == 200 && response.body()!!.has(Constants.API.KEY.SUCCESS)
                                    && response.body()!!.get(Constants.API.KEY.SUCCESS).asBoolean
                                    && response.body()!!.has(Constants.API.KEY.DATA)

                                ) {
                                  //  val data = response.body()!!.get(Constants.API.KEY.DATA)
                                    if(response.body() != null) {
                                       //  val lDataList: ArrayList<MyJobBean> = Gson().fromJson(data.toString(), object : TypeToken<ArrayList<MyJobBean>>() {}.type)
                                       var data= Gson().fromJson(response.body(), MyJobBean::class.java)

                                         myJobBean.value = data
                                        uiState.value = UIState(loading = false)
                                    } else {

                                        if (response.body()!!.has("message")) {
                                            var message = response.body()!!.get("message").asString
                                            uiState.value = UIState(loading = false, error = message)
                                        }
                                    }
                                }
                            } else if (response.code() == 400) {
                                val json = JSONObject(response.errorBody()?.string())
                                var message = json.getString("errorDetails")
                                uiState.value = UIState(loading = false, error = message)
                            }

                            else if (response.body() != null) {
                                var message = context.resources.getString(R.string.something_went_wrong)
                                if (response.body()!!.has(Constants.API.KEY.MESSAGE)) {
                                    message = response.body()!!.get(Constants.API.KEY.MESSAGE).asString
                                } else if (response.body()!!.has(Constants.API.KEY.ERROR)) {
                                    message = response.body()!!.get(Constants.API.KEY.ERROR).asString
                                }

                                uiState.value = UIState(error = message)
                            } else {
                                uiState.value = UIState(error = context.getString(R.string.something_went_wrong))
                            }
                        } catch (e: Exception) {
                            LogUtils.e(TAG, e.message.toString())
                            uiState.value = UIState(loading = false, error = e.message)
                        }
                    }

                    override fun onError(e: Throwable) {
                        uiState.value = UIState(loading = false, error = e.message)
                    }
                })
            compositeDisposable.add(loginDisposable)
        } catch (e: Exception) {
            LogUtils.e(TAG, e.message.toString())
            uiState.value = UIState(loading = false, error = e.message)
        }
    }
    fun getCurrentWeekTitle(): WeekTitleData {
        val format = SimpleDateFormat("dd MMM")
        val calendar = Calendar.getInstance()
        calendar.set(Calendar.HOUR_OF_DAY, 0)
        calendar.set(Calendar.MINUTE, 0)
        calendar.set(Calendar.SECOND, 1)

        calendar.firstDayOfWeek
        calendar.firstDayOfWeek = Calendar.MONDAY
        calendar.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY)


        val mData = WeekTitleData("", 0, 0)
        mData.millisFirstDay = (calendar.timeInMillis)


        val days = arrayOfNulls<String>(2)

        days[0] = format.format(calendar.time)

        calendar.timeInMillis = calendar.timeInMillis + millisInAWeek - 10000

        days[1] = format.format(calendar.time)

        mData.millisLastDay = (calendar.timeInMillis)

        mData.title = (days[0] + " - " + days[1])

        return mData
    }

    fun getPreviousWeekTitle(firstDayOfLastWeek: Long): WeekTitleData {
        val format = SimpleDateFormat("dd MMM")
        val calendar = Calendar.getInstance()

        calendar.timeInMillis = (firstDayOfLastWeek).minus(millisInAWeek)


        calendar.set(Calendar.HOUR_OF_DAY, 0)
        calendar.set(Calendar.MINUTE, 0)
        calendar.set(Calendar.SECOND, 1)

        val mData = WeekTitleData("", 0, 0)
        mData.millisFirstDay = (calendar.timeInMillis)
        mData.title = (format.format(calendar.time))

        calendar.timeInMillis = calendar.timeInMillis + millisInAWeek - 10000

        mData.title = (mData.title + " - " + format.format(calendar.time))
        mData.millisLastDay = (calendar.timeInMillis)

        return mData

    }

}