package com.infinitysms.usa.ui.dashboard.nav_drawer


import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.infinitysms.usa.BuildConfig
import com.infinitysms.usa.R
import com.infinitysms.usa.data.AppDataManager
import com.infinitysms.usa.di.module.AppViewModelFactory
import com.infinitysms.usa.ui.base.BaseFragment
import com.infinitysms.usa.ui.dashboard.DashboardActivity
import com.infinitysms.usa.utils.LocaleHelper
import com.infinitysms.usa.utils.toolbar.FragmentToolbar
import kotlinx.android.synthetic.main.fragment_nav_drawer_home.*
import loadUrl
import javax.inject.Inject


class NavDrawerHomeFragment : BaseFragment<NavDrawerHomeFragmentVM>(), View.OnClickListener {

    private val TAG = NavDrawerHomeFragment::class.java.simpleName

    companion object {
        @JvmStatic
        fun newInstance() =
            NavDrawerHomeFragment().apply {
                arguments = Bundle().apply {

                }
            }
    }

    @Inject
    lateinit var appViewModelFactory: AppViewModelFactory

    @Inject
    lateinit var navDrawerHomeFragmentVM: NavDrawerHomeFragmentVM

    @Inject
    lateinit var appDataManager: AppDataManager

    override fun builder(): FragmentToolbar? {
        return null
    }

    override fun getViewModel(): NavDrawerHomeFragmentVM {
        navDrawerHomeFragmentVM =
            ViewModelProvider(this@NavDrawerHomeFragment, appViewModelFactory)
                .get(NavDrawerHomeFragmentVM::class.java)
        return navDrawerHomeFragmentVM
    }

    override fun onClick(v: View?) {

        when (v?.id) {
            R.id.mcvHome -> {
                toggleNavDrawer()
            }

            R.id.mcvMyProfiling -> {
                toggleNavDrawer()
                Handler().postDelayed({
                    (activity as DashboardActivity).gotoMyProfile()

                }, 160)
            }



            R.id.mcvPublicNotification -> {
//                Navigation.findNavController(this.view!!)
//                    .navigate(R.id.action_navDrawerHomeFragment_to_publicNotificationSubMenuFragment)
                toggleNavDrawer()
                Handler().postDelayed({
                    (activity as DashboardActivity).showLogoutConfirmDialog()

                }, 150)
            }


        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {

        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        if (root == null) {
            root =
                inflater.inflate(R.layout.fragment_nav_drawer_home, container, false) as ViewGroup
        }

        return root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        if (!isFragmentLoaded) {
            initInstances()
        }

        super.onViewCreated(view, savedInstanceState)
    }

    private fun initInstances() {

        observeViewModel()
        //civProfilePIc.loadUrl(activity!!, BuildConfig.IMG_BASE_URL+appDataManager.getUserData()?.user_image!!)

        mcvHome.setOnClickListener(this)
        mcvMyProfiling.setOnClickListener(this)
        mcvPublicNotification.setOnClickListener(this)
        mcvRegisters.setOnClickListener(this)
        mcvDataEntity.setOnClickListener(this)
        mcvAttendance.setOnClickListener(this)


        if (appDataManager.getUserData()?.phone_number != null) {
            tvName.text = appDataManager.getUserData()?.phone_number
        }
    }

    private fun observeViewModel() {
        navDrawerHomeFragmentVM.uiState.observe(viewLifecycleOwner, Observer {
            if (it.loading) {
                showLoadingDialog()
            } else {
                dismissLoadingDialog()
            }

            if (it.success) {
            }

            if (it.message != null) {
                showSuccessToast(it.message!!)
            }

            if (it.error != null) {
                showErrorToast(it.error!!)
            }

            if (it.messageWithAlert != null) {

            }
        })
    }
}
