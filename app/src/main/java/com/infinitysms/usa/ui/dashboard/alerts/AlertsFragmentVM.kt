package com.infinitysms.usa.ui.dashboard.alerts

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.infinitysms.usa.data.AppDataManager
import com.infinitysms.usa.data.models.UIState
import default
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

/**
 * Created by Android Tony on 23,March,2020
 */
class AlertsFragmentVM @Inject constructor(var appDataManager: AppDataManager) :
    ViewModel() {


    private val TAG = AlertsFragmentVM::class.java.simpleName

    var uiState = MutableLiveData<UIState>().default(UIState(loading = false))

    private var compositeDisposable = CompositeDisposable()

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.dispose()
    }

}