package com.infinitysms.usa.ui.dashboard.my_profile

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.Gson
import com.google.gson.JsonObject
import com.infinitysms.usa.R
import com.infinitysms.usa.utils.Helper
import java.util.*

class DataAdapter(
    var context: Context,
    dataList: ArrayList<HashMap<String, String>>
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private var VT_EXTRA = 1



    var list =
        ArrayList<HashMap<String, String>>()

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): RecyclerView.ViewHolder {
        val viewHolder: RecyclerView.ViewHolder
        val inflater = LayoutInflater.from(parent.context)

        when (viewType) {
            1 -> {
                val v1 = inflater.inflate(R.layout.row_recycler, parent, false)
                viewHolder = DataHolder(v1)
            }
            2 -> {
                val v1 = inflater.inflate(R.layout.row_recycler_vehicle, parent, false)
                viewHolder = DataHolder2(v1)
            }
            3 -> {
                val v1 = inflater.inflate(R.layout.row_recycler_document, parent, false)
                viewHolder = DataHolder3(v1)
            }
            else -> {
                val v1 = inflater.inflate(R.layout.row_recycler_document, parent, false)
                viewHolder = DataHolder3(v1)
            }
        }

        return viewHolder
    }

    override fun onBindViewHolder(
        viewHolder: RecyclerView.ViewHolder,
        position: Int
    ) {
        when (viewHolder.itemViewType) {
            1 -> {
                val holder = viewHolder as DataHolder
                val dataMap = list[position]
                holder.tvDesc1.text = dataMap["Desc1"]
                holder.tvDesc2.text = dataMap["Desc2"]
                holder.tvDesc3.text = dataMap["Desc3"]
                holder.tvDesc4.text = dataMap["Desc4"]
                holder.tvDesc5.text = dataMap["Desc5"]
            }
            2 -> {
                val holder = viewHolder as DataHolder2
                val dataMap = list[position]
                holder.tvDesc1.text = dataMap["Desc1"]
                holder.tvDesc2.text = dataMap["Desc2"]
                holder.tvDesc3.text = dataMap["Desc3"]
                holder.tvDesc4.text = dataMap["Desc4"]
                holder.tvDesc5.text = dataMap["Desc5"]
            }
            3 -> {
                val holder = viewHolder as DataHolder3
                val dataMap = list[position]
                for(i in 0 until dataMap.size-1) {
                    var data=dataMap["Desc"+(i+1)]
                    val jsonData = Gson().fromJson(data, JsonObject::class.java)
                    var data2=jsonData.get("meta_data").asString
                    val jsonData2 = Gson().fromJson(data2, JsonObject::class.java)

                    when (jsonData2.get("docTitle").asString) {
                        "Driver's License" -> {
                            if(jsonData.get("uploadDoc").asString.isNullOrEmpty())
                            {
                                holder.ivStatusIcon2.setImageResource(R.mipmap.red_cross)
                            }
                             if(!jsonData.get("expirydate").asString.isNullOrEmpty()) {
                                 holder.tvDesc2.text =  Helper.getFormattedDateFromDate(
                                     jsonData.get("expirydate").asString,
                                     "YYYY-MM-dd",
                                     "dd/MM/YYYY",
                                     false
                                 )
                                 if(jsonData2.has("expiryDateStatus")
                                     &&jsonData2.get("expiryDateStatus").asInt==1)
                                 holder.ivStatusIcon2.setImageResource(R.mipmap.warning_yellow)
                                 else
                                     holder.ivStatusIcon2.setImageResource(R.mipmap.green_right)

                             }
                            else
                                 holder.tvDesc2.text = "No expiry date"



                        }
                        "Proof of Address" -> {
//                            holder.tvDesc6.text =  if(!jsonData.get("expirydate").asString.isEmpty() !=null)
//                                jsonData.get("expirydate").asString
//                            else "Document outstanding"

                            if(jsonData.get("uploadDoc").asString.isNullOrEmpty())
                            {
                                holder.ivStatusIcon6.setImageResource(R.mipmap.red_cross)
                            }
                            if(!jsonData.get("expirydate").asString.isNullOrEmpty()) {
                                holder.tvDesc6.text =  Helper.getFormattedDateFromDate(
                                    jsonData.get("expirydate").asString,
                                "YYYY-MM-dd",
                                    "dd/MM/YYYY",
                                    false
                                    )
                                if(jsonData2.has("expiryDateStatus")
                                    &&jsonData2.get("expiryDateStatus").asInt==1)
                                    holder.ivStatusIcon6.setImageResource(R.mipmap.warning_yellow)
                                else
                                    holder.ivStatusIcon6.setImageResource(R.mipmap.green_right)

                            }
                            else
                                holder.tvDesc6.text = "Document outstanding"
                        }
                    }
                }

            }
        }
    }

    override fun getItemViewType(position: Int): Int {
        VT_EXTRA=position+1
        return VT_EXTRA
    }
    override fun getItemCount(): Int {
        return list.size
    }
    class DataHolder(itemView: View) :
        RecyclerView.ViewHolder(itemView) {
        var tvDesc1: TextView
        var tvDesc2: TextView
        var tvDesc3: TextView
        var tvDesc4: TextView
        var tvDesc5: TextView

        init {
            tvDesc1 =
                itemView.findViewById<View>(R.id.tv_driverID) as TextView
            tvDesc2 =
                itemView.findViewById<View>(R.id.tv_fullName) as TextView
         tvDesc3 =
                itemView.findViewById<View>(R.id.tv_companyName) as TextView
         tvDesc4 =
                itemView.findViewById<View>(R.id.tv_phone) as TextView
         tvDesc5 =
                itemView.findViewById<View>(R.id.tv_email) as TextView
        }
    }
    class DataHolder2(itemView: View) :
        RecyclerView.ViewHolder(itemView) {
        var tvDesc1: TextView
        var tvDesc2: TextView
        var tvDesc3: TextView
        var tvDesc4: TextView
        var tvDesc5: TextView

        init {
            tvDesc1 =
                itemView.findViewById<View>(R.id.tv_vehicleID) as TextView
            tvDesc2 =
                itemView.findViewById<View>(R.id.tv_regnum) as TextView
            tvDesc3 =
                itemView.findViewById<View>(R.id.tv_model) as TextView
            tvDesc4 =
                itemView.findViewById<View>(R.id.tv_year) as TextView
            tvDesc5 =
                itemView.findViewById<View>(R.id.tv_color) as TextView
        }
    }
    class DataHolder3(itemView: View) :
        RecyclerView.ViewHolder(itemView) {
        var tvDesc1: TextView
        var tvDesc2: TextView
        var tvDesc3: TextView
        var tvDesc4: TextView
        var tvDesc5: TextView
        var tvDesc6: TextView
        var ivStatusIcon1: ImageView
        var ivStatusIcon2: ImageView
        var ivStatusIcon3: ImageView
        var ivStatusIcon4: ImageView
        var ivStatusIcon5: ImageView
        var ivStatusIcon6: ImageView

        init {
            tvDesc1 =
                itemView.findViewById<View>(R.id.tv_passport) as TextView
            tvDesc2 =
                itemView.findViewById<View>(R.id.tv_license) as TextView
            tvDesc3 =
                itemView.findViewById<View>(R.id.tv_badge) as TextView
            tvDesc4 =
                itemView.findViewById<View>(R.id.tv_crb) as TextView
            tvDesc5 =
                itemView.findViewById<View>(R.id.tv_insuarance) as TextView
         tvDesc6 =
                itemView.findViewById<View>(R.id.tv_address_proof) as TextView

            ivStatusIcon1 =
                itemView.findViewById<View>(R.id.ivStatusIcon1) as ImageView
            ivStatusIcon2 =
                itemView.findViewById<View>(R.id.ivStatusIcon2) as ImageView
            ivStatusIcon3 =
                itemView.findViewById<View>(R.id.ivStatusIcon3) as ImageView
            ivStatusIcon4 =
                itemView.findViewById<View>(R.id.ivStatusIcon4) as ImageView
            ivStatusIcon5 =
                itemView.findViewById<View>(R.id.ivStatusIcon5) as ImageView
            ivStatusIcon6 =
                itemView.findViewById<View>(R.id.ivStatusIcon6) as ImageView
        }
    }

    init {
        list = dataList
    }

}