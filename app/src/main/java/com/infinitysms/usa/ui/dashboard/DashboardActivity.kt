package com.infinitysms.usa.ui.dashboard

import android.app.Activity
import android.app.PendingIntent
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.location.Location
import android.media.AudioManager
import android.media.SoundPool
import android.os.*
import android.telephony.SmsManager
import android.telephony.SubscriptionInfo
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.view.WindowManager
import android.widget.ProgressBar
import android.widget.TextView
import androidx.annotation.NonNull
import androidx.appcompat.app.AlertDialog
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.NavOptions
import androidx.navigation.fragment.NavHostFragment
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.infinitysms.usa.R
import com.infinitysms.usa.data.AppDataManager
import com.infinitysms.usa.data.interfaces.WebSocketListener
import com.infinitysms.usa.data.models.MessageData
import com.infinitysms.usa.di.module.AppViewModelFactory
import com.infinitysms.usa.ui.auth.AuthActivity
import com.infinitysms.usa.ui.base.BaseActivity
import com.infinitysms.usa.ui.dashboard.home.HomeFragment
import com.infinitysms.usa.ui.websocket.WebSocketDataManager
import com.infinitysms.usa.utils.Constants
import com.infinitysms.usa.utils.Constants.REQUEST_CODE.Companion.LOCATION_PREMISSION_REQUEST_CODE
import com.infinitysms.usa.utils.Constants.REQUEST_CODE.Companion.PERMISSIONS_REQUEST_ENABLE_GPS
import com.infinitysms.usa.utils.LocationUtils
import com.neovisionaries.ws.client.WebSocketState
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasAndroidInjector
import kotlinx.android.synthetic.main.activity_dashboard.*
import kotlinx.android.synthetic.main.layout_progress_and_error.*
import makeGone
import makeVisible
import org.json.JSONObject
import javax.inject.Inject


class DashboardActivity : BaseActivity<DashboardActivityVM>(), HasAndroidInjector,
    BottomNavigationView.OnNavigationItemSelectedListener, WebSocketListener {

    private var messageList: ArrayList<MessageData?>? = null
    private var enableGPSRejected: Boolean = false
    private lateinit var wl: PowerManager.WakeLock

    var activeFrag: StatusCheck? = null
    var inboxDialog: androidx.appcompat.app.AlertDialog? = null
    var isSoundPlay: Boolean = false
    var streamId: Int = 0
    var soundBeepId: Int = 0

    var sentStatusReceiver: BroadcastReceiver? = null
    var deliveredStatusReceiver: BroadcastReceiver? = null
    var soundPool: SoundPool? = null

    companion object {
        var activeSim: SubscriptionInfo? = null
        var mActivityRunning = false
        var isLocationSendStop = false
        var isLogout: Boolean = false
        var isOncreateCall: Boolean = false
        var dontStartService: Boolean = false
        var goingTostartService: Boolean = false

        var webSocketDataManager: WebSocketDataManager? = null
        var mContext: Context? = null

        var dashboardNavController: NavController? = null
        var newJobtvLocationt: TextView? = null
        var progressBarJobs: ProgressBar? = null
        var tvJobProgres: TextView? = null
    }

    private val TAG = DashboardActivity.javaClass.name


    @Inject
    lateinit var dashboardActivityVM: DashboardActivityVM

    @Inject
    lateinit var appViewModelFactory: AppViewModelFactory

    @Inject
    lateinit var appDataManager: AppDataManager

    @Inject
    lateinit var androidInjector: DispatchingAndroidInjector<Any>
    private var drawerNavController: NavController? = null

    var status: String? = null

    override fun androidInjector(): AndroidInjector<Any> {
        return androidInjector
    }

    override fun getViewModel(): DashboardActivityVM {
        dashboardActivityVM = ViewModelProvider(this@DashboardActivity, appViewModelFactory)
            .get(DashboardActivityVM::class.java)
        return dashboardActivityVM
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.actionHome -> {
                val navBuilder = NavOptions.Builder()
                val navOptions = navBuilder
                    .setPopUpTo(getDashboardNavController()?.currentDestination?.id!!, true).build()
                dashboardNavController?.navigate(R.id.homeFragment, null, navOptions)
            }
            R.id.actionLiveTracking -> {
                val navBuilder = NavOptions.Builder()
                val navOptions = navBuilder
                    .setPopUpTo(getDashboardNavController()?.currentDestination?.id!!, true).build()
                dashboardNavController?.navigate(R.id.playPauseFragment, null, navOptions)
            }
            R.id.actionAlerts -> {
                val navBuilder = NavOptions.Builder()
                val navOptions = navBuilder
                    .setPopUpTo(getDashboardNavController()?.currentDestination?.id!!, true).build()
                dashboardNavController?.navigate(R.id.alertsFragment, null, navOptions)
            }
            R.id.actionCompliance -> {
                val navBuilder = NavOptions.Builder()
                val navOptions = navBuilder
                    .setPopUpTo(getDashboardNavController()?.currentDestination?.id!!, true).build()
                // dashboardNavController?.navigate(R.id.complianceFragment, null, navOptions)
            }
        }
        return true
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dashboard)
        hideKeyboard()
        isOncreateCall = true
        mContext = this
        window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
        val pm: PowerManager =
            getSystemService(Context.POWER_SERVICE) as PowerManager
        wl = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "MyApp:InfinitySMS")
        wl.acquire()

        WebSocketDataManager.phoneNumber = appDataManager.getUserData()?.phone_number!!
        val dashboardNavHost: Fragment? =
            supportFragmentManager.findFragmentById(R.id.dashboardNavHostFragment)
        dashboardNavController = dashboardNavHost?.let { NavHostFragment.findNavController(it) }


        newJobtvLocationt = findViewById<TextView>(R.id.tvLocation)
        progressBarJobs = findViewById<ProgressBar>(R.id.progressBarJob)
        tvJobProgres = findViewById<TextView>(R.id.tvJobProgress)
        bnv.setOnNavigationItemSelectedListener(this)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            soundPool = SoundPool.Builder().setMaxStreams(10).build()
        } else {
            soundPool = SoundPool(10, AudioManager.STREAM_MUSIC, 1)
        }
        soundBeepId = soundPool?.load(this, R.raw.click_beep, 10)!!

        observeViewModel()

    }

    private fun observeViewModel() {
        dashboardActivityVM.uiState.observe(this, Observer {
            if (it.loading) {
                loadingLayout.makeVisible()
            } else {
                loadingLayout.makeGone()
            }

            if (it.success) {
                showSuccessToast(it.message!!)
            }

            if (it.message != null) {
                showInfoToast(it.message!!)
            }

            if (it.error != null) {
                showErrorToast(it.error!!)
            }

            if (it.messageWithAlert != null) {
                playBeepSound()

                showDefaultAlertDialog(getString(R.string.message), it.messageWithAlert!!)
            }
        })

    }


    private fun findVisibleFragemnt() {
        if (isNetworkConnected()) {
            //  if (checkMapServices()) {
            if (activeFrag != null) {
                activeFrag?.changeToggle(true)
            }
            // }
        }
    }

    override fun onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    override fun toggleNavDrawer() {
        if (drawerLayout != null) {
            if (!drawerLayout.isDrawerOpen(GravityCompat.START)) {
                drawerLayout.openDrawer(GravityCompat.START)
            } else {
                drawerLayout.closeDrawer(GravityCompat.START)
            }
        }
    }

    override fun lockedNavDrawer() {
        drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED)
        super.lockedNavDrawer()
    }

    override fun unLockedNavDrawer() {
        drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED)
        super.unLockedNavDrawer()
    }

    override fun hideBottomNavigation() {
        bnv.makeGone()
        super.hideBottomNavigation()
    }

    override fun showBottomNavigation() {
        bnv.makeVisible()
        super.showBottomNavigation()
    }

    override fun getDashboardNavController(): NavController? {
        super.getDashboardNavController()
        return dashboardNavController
    }

    fun gotoMyProfile() {
        val navBuilder = NavOptions.Builder()
        val navOptions = navBuilder
            .setEnterAnim(R.anim.trans_left_in)
            .setExitAnim(R.anim.trans_left_out)
            .setPopEnterAnim(R.anim.trans_right_in)
            .setPopExitAnim(R.anim.trans_right_out)
            .build()
        dashboardNavController?.navigate(R.id.myProfileFragment, null, navOptions)
    }


    fun showLogoutConfirmDialog() {
        AlertDialog.Builder(this).setIcon(R.mipmap.ic_launcher)
            .setMessage(resources.getString(R.string.confirm_logout_message))
            .setPositiveButton(
                resources.getString(R.string.yes)
            ) { dialog, which ->
                if (appDataManager.getDriverOnline()) {
                    isLogout = true
                    HomeFragment.loadingLayout?.visibility = View.VISIBLE
                    stopService_WebSocket()
                } else {
                    appDataManager.clearUserData()
                    startInstanceWithBackStackCleared()
                }
            }
            .setNegativeButton(
                resources.getString(R.string.no)
            ) { dialog, which -> dialog.dismiss() }.create().show()
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        @NonNull permissions: Array<String>,
        @NonNull grantResults: IntArray
    ) {
        Log.v("TONY", "onRequestPermissionsResult")
        if (requestCode == LOCATION_PREMISSION_REQUEST_CODE) {
            if (LocationUtils.getInstance(this).checkLocationPermission()) {
                LocationUtils.getInstance(this)
                    .getLocation(false, object : LocationUtils.LocationUtilsListener {
                        override fun onSuccess(location: Location) {
                            findVisibleFragemnt()
                        }

                        override fun onFailure(message: String) {

                        }
                    })

            }
        }

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        Log.v("TONY", "onActivityResult")

        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == PERMISSIONS_REQUEST_ENABLE_GPS) {
            if (resultCode == 0) {
                enableGPSRejected = true
                return
            } else
                enableGPSRejected = false
            if (LocationUtils.getInstance(this).checkLocationPermission()) {
                LocationUtils.getInstance(this)
                    .getLocation(false, object : LocationUtils.LocationUtilsListener {
                        override fun onSuccess(location: Location) {
                            LocationUtils.getInstance(this@DashboardActivity)
                                .onActivityResult(requestCode, resultCode, data)
                            findVisibleFragemnt()
                        }

                        override fun onFailure(message: String) {

                        }
                    })
            }
        }
    }

    fun stopService_WebSocket() {
        if (isNetworkConnected()) {

            isLocationSendStop = true
            appDataManager.setDriverOnline(false)

            LocationUtils.getInstance(this).stopLocationService()
            if (webSocketDataManager != null) {

                appDataManager.startDriverOnline(0)


                webSocketDataManager?.closeConnection()
            }
            runOnUiThread {
                HomeFragment.loadingLayout?.visibility = View.GONE
            }
        }
    }

    fun startWebSocket() {

        if (!isNetworkConnected()) {
            return
        }

        isLocationSendStop = false
        appDataManager.setDriverOnline(true)
        appDataManager.startDriverOnline(1)


        var phoneNumber = appDataManager.getUserData()?.phone_number
        var companyId = appDataManager.getUserData()?.company_id

        //============== web socket connection ==============================================


        if (webSocketDataManager != null) {
            if (WebSocketDataManager.getConnectedWebSocket() != null) {
                when (WebSocketDataManager.getConnectedWebSocket()!!.state) {
                    WebSocketState.CLOSED -> {
                        Log.v("AndroidTony", "Successfully CLOSED")
                        WebSocketDataManager.getConnectedWebSocket()?.disconnect("Manual")
                        WebSocketDataManager.getConnectedWebSocket()?.recreate()!!
                            .connectAsynchronously()
                    }
                    WebSocketState.CLOSING -> {
                        Log.v("AndroidTony", "Successfully CLOSING")

                        // Toast.makeText(DashboardActivity.this, "Socket Already Closing.", Toast.LENGTH_LONG).show()
                    }
                    WebSocketState.OPEN -> {
                        Log.v("AndroidTony", "Successfully connected")
                        if (mActivityRunning) {
                            runOnUiThread {
                                HomeFragment.loadingLayout?.visibility = View.GONE
                            }
                        }
                        //    Toast.makeText(DashboardActivity.this, "Socket Already Connected.", Toast.LENGTH_LONG).show()
                    }
                    WebSocketState.CONNECTING -> {
                        //Toast.makeText(DashboardActivity.this, "Socket Already Connecting.", Toast.LENGTH_LONG).show()
                        Log.v("AndroidTony", "Successfully CONNECTING")

                    }
                    else -> {

                    }
                }

            } else {
                webSocketDataManager =
                    WebSocketDataManager.getInstance(phoneNumber!!, companyId!!, this)
            }
        } else {
            webSocketDataManager =
                WebSocketDataManager.getInstance(phoneNumber!!, companyId!!, this)
            //    startWebSocket()
        }
    }

    override fun onConnectSuccessfully() {
        try {

            Log.v("AndroidTony", "Successfully connected2")
            if (mActivityRunning) {
                runOnUiThread {
                    HomeFragment.loadingLayout?.visibility = View.GONE
                }
            }
            //if (checkMapServices()) {
            if (LocationUtils.getInstance(this).checkLocationPermission()) {
                goingTostartService = true
                dontStartService = false
                setsmsBroadCast()
                LocationUtils.getInstance(this).getLastKnownLocation()

            } else {
                getLocationPermission()
            }
            //}

        } catch (e: Exception) {

            e.printStackTrace()
        }
    }

    private fun setsmsBroadCast() {
        sentStatusReceiver = object : BroadcastReceiver() {
            override fun onReceive(arg0: Context?, arg1: Intent?) {
                var s = "Unknown Error"
                var code = 0
                when (resultCode) {
                    Activity.RESULT_OK -> {
                        code = 1
                        s = "Message Sent Successfully !!"
                    }
                    SmsManager.RESULT_ERROR_GENERIC_FAILURE -> s =
                        "Generic Failure Error"
                    SmsManager.RESULT_ERROR_NO_SERVICE -> s =
                        "Error : No Service Available"
                    SmsManager.RESULT_ERROR_NULL_PDU -> s = "Error : Null PDU"
                    SmsManager.RESULT_ERROR_RADIO_OFF -> s =
                        "Error : Radio is off"
                    else -> {
                    }
                }
                Log.v("VINAY", "SMessage" + s + " :" + arg1?.getStringExtra("Number"))
                var list = appDataManager.getMessageList()
                if (list != null) {
                    list.add(
                        MessageData(
                            0,
                            arg1?.getStringExtra("Message"),
                            arg1?.getStringExtra("Number"),
                            s,
                            code
                        )
                    )
                   
                    appDataManager.setMessageList(list)

                    if (mActivityRunning && activeFrag != null)
                        (activeFrag as HomeFragment).setMessageAdapter()
                } else {
                    list = HashSet()
                    list.add(
                        MessageData(
                            0,
                            arg1?.getStringExtra("Message"),
                            arg1?.getStringExtra("Number"),
                            s,
                            code
                        )
                    )
                    appDataManager.setMessageList(list)
                    if (mActivityRunning && activeFrag != null)
                        (activeFrag as HomeFragment).setMessageAdapter()

                }
                // showInfoToast("SMessage"+s)
                //sendStatusTextView.setText(s)
            }
        }
        deliveredStatusReceiver = object : BroadcastReceiver() {
            override fun onReceive(arg0: Context?, arg1: Intent?) {
                var s = "Message Not Delivered"
                when (resultCode) {
                    Activity.RESULT_OK -> s = "Message Delivered Successfully"
                    Activity.RESULT_CANCELED -> {
                    }
                }
                Log.v("VINAY", "DMessage" + s + " :" + arg1?.getStringExtra("Number"))
                // showInfoToast("DMessage"+s)
//                deliveryStatusTextView.setText(s)
//                phoneEditText.setText("")
//                messageEditText.setText("")
            }
        }
        registerReceiver(sentStatusReceiver, IntentFilter("SMS_SENT"))
        registerReceiver(deliveredStatusReceiver, IntentFilter("SMS_DELIVERED"))
    }


    override fun onDissconnectSuccessfully() {
        appDataManager.startDriverOnline(0)

        Log.v("AndroidTony", "Successfully disconnected2")

        if (isLogout) {
            WebSocketDataManager.webSocket = null
            WebSocketDataManager.clearInstance()
            webSocketDataManager = null
            System.gc()
            isLogout = false
            appDataManager.clearUserData()
            startInstanceWithBackStackCleared()
        }
    }

    override fun onServerError(message: String) {
        if (mActivityRunning) {
            runOnUiThread {
                HomeFragment.loadingLayout?.visibility = View.GONE

                showShortToast(message)
            }
        }
    }


    fun playBeepSound() {
        if (soundPool != null) {
            if (soundBeepId == 0) {
                soundBeepId = soundPool?.load(this, R.raw.click_beep, 10)!!

            }
            soundPool?.play(soundBeepId, 1f, 1f, 10, 0, 1f)!!
        }
    }


    override fun onInboxMessage(
        notification_type: String, json: JSONObject
    ) {
        if (mActivityRunning) {
            runOnUiThread {
                HomeFragment.loadingLayout?.visibility = View.GONE
            }
        }
        if ((notification_type == "Internal server error") ||
            (notification_type == "ignore") || (notification_type == "session_timeout")
        ) {
            if (notification_type == "session_timeout") {
                WebSocketDataManager.webSocket = null
                if (mActivityRunning) {
                    runOnUiThread {
                        showLogoutAlertDialog(
                            "Session Timeout",
                            json.getString("message").toString()
                        )

                    }
                }
            } else {
                if (mActivityRunning) {
                    helper.showErrorToast(notification_type)
                }
            }

            return
        }
        val data = json.get(Constants.API.KEY.DATA)
        if (data != null) {
            messageList = Gson().fromJson(
                data.toString(),
                object : TypeToken<ArrayList<MessageData>>() {}.type
            )

        }
        prepareSms()

    }

    override fun onCloseConnection(closeConn: Boolean) {
        TODO("Not yet implemented")
        //    webSocketDataManager=null
    }

    override fun onCloseFailed(e: Throwable) {
        TODO("Not yet implemented")
    }


    override fun onMessageShow(message: String) {
        Log.v("AndroidTony", message)

        if (mActivityRunning) {
            HomeFragment.loadingLayout?.visibility = View.GONE
            showErrorToast(message)
        }
    }

    override fun onResume() {
        mActivityRunning = true
        super.onResume()
        if (!goingTostartService && !enableGPSRejected) {
            Handler().postDelayed({

                if (LocationUtils.getInstance(this).checkLocationPermission()) {
                    dontStartService = true
                    goingTostartService = false
                    setsmsBroadCast()
                    LocationUtils.getInstance(this).getLastKnownLocation()
                    if (isOncreateCall) {
                        isOncreateCall = false
                        findVisibleFragemnt()
                    }
                } else {
                    goingTostartService = false
                    dontStartService = true
                }

            }, 1200)
        }
        enableGPSRejected = false


        val drawerNavHost: Fragment? =
            supportFragmentManager.findFragmentById(R.id.drawerNavHostFragment)
        drawerNavController = drawerNavHost?.let { NavHostFragment.findNavController(it) }
        if (appDataManager != null) {
            var list = appDataManager.getMessageList()
            if (list != null) {
                if (mActivityRunning && activeFrag != null)
                    (activeFrag as HomeFragment).setMessageAdapter()
            }
        }
    }


    fun getLocationPermission() {
        LocationUtils.getInstance(this).requestLocation()
    }

    fun setListener(frag: HomeFragment) {
        activeFrag = frag
        //findVisibleFragemnt()
    }

    interface StatusCheck {
        fun changeToggle(boolean: Boolean)
        fun setHomeData()
    }


    override fun onDestroy() {
        wl.release()
        if (isLogout) {
            // webSocketDataManager = null
            //mFusedLocationClient=null
            //  isLogout = false
            // jsonDATA=null
//System.gc()
        }
        super.onDestroy()
    }

    fun startInstanceWithBackStackCleared() {
        val intent = Intent(this, AuthActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
        startActivity(intent)
        overridePendingTransition(R.anim.activity_enter, R.anim.activity_leave)
        finishAffinity()
        mActivityRunning = false
        isLocationSendStop = false
        isLogout = false
        isOncreateCall = false
        dontStartService = false
        goingTostartService = false
    }

    override fun onPause() {
        mActivityRunning = false
        pauseJobSound()
        super.onPause()
    }

    fun showLogoutAlertDialog(title: String, message: String) {
        runOnUiThread {
            if (mActivityRunning) {
                AlertDialog.Builder(this).setTitle(title).setMessage(message)
                    .setCancelable(false).setPositiveButton(
                        "OK"
                    ) { dialog, id ->
                        dialog.dismiss()
                        HomeFragment.loadingLayout?.visibility = View.VISIBLE
                        LocationUtils.getInstance(this).stopLocationService()
                        if (webSocketDataManager != null)
                            webSocketDataManager?.closeConnection()
                        appDataManager.clearUserData()
                        WebSocketDataManager.clearInstance()
                        webSocketDataManager = null
                        System.gc()
                        startInstanceWithBackStackCleared()

                    }.create().show()
            }
        }
    }


    fun pauseJobSound() {
        if (soundPool != null) {
            soundPool?.pause(streamId)
        }
    }

    fun stopJobSound() {
        if (soundPool != null) {
            soundPool?.stop(streamId)
            isSoundPlay = false
        }
    }


    private fun prepareSms() {
        if (messageList != null && messageList?.isNotEmpty()!!) {
            for (i in messageList?.indices!!) {
                if (messageList?.get(i)?.message?.isNotEmpty()!!) {
                    var dataForSend: String? = messageList?.get(i)?.message
                    val callNineNumber: String = messageList?.get(i)?.phone_number!!
                    val asyncTask = SMSTask(callNineNumber, dataForSend!!)
                    asyncTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR)
                    //   }
                    //     }
                }
            }
        }
    }

    class SMSTask(var friendOneNumber: String, var dataForSend: String) :
        AsyncTask<Void?, Void?, Void?>() {
        override fun doInBackground(vararg params: Void?): Void? {
            sendSMS(friendOneNumber, dataForSend)
            return null
        }


        fun sendSMS(phoneNumber: String, message: String) {
            println("Sms for send==>$message  $phoneNumber")
            val sentPendingIntents =
                java.util.ArrayList<PendingIntent>()
            val deliveredPendingIntents =
                java.util.ArrayList<PendingIntent>()
            try {
                /* val sms = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1) {
                     SmsManager.getSmsManagerForSubscriptionId(activeSim?.subscriptionId!!)
                 } else {
                     SmsManager.getDefault()
                 }*/
                var i = Intent("SMS_SENT")
                i.putExtra("Number", phoneNumber)
                i.putExtra("Message", message)
                var j = Intent("SMS_DELIVERED")
                j.putExtra("Number", phoneNumber)
                j.putExtra("Message", message)
                val sentIntent =
                    PendingIntent.getBroadcast(
                        mContext?.applicationContext,
                        Math.random().toInt(),
                        i,
                        PendingIntent.FLAG_CANCEL_CURRENT
                    )
                val deliveredIntent =
                    PendingIntent.getBroadcast(
                        mContext?.applicationContext,
                        Math.random().toInt(),
                        j,
                        PendingIntent.FLAG_CANCEL_CURRENT
                    )
                sentPendingIntents.add(sentIntent)
                deliveredPendingIntents.add(deliveredIntent)

                val sms = SmsManager.getDefault()
                // val mSMSMessage = sms.divideMessage(message)

                sms.sendTextMessage(
                    phoneNumber, null, message,
                    sentIntent, deliveredIntent
                )
            } catch (e: java.lang.Exception) {
                e.printStackTrace()
                val h = Handler(Looper.getMainLooper())
                h.post {
                    println("SMS sending failed.. ")

                }
            }
        }

        override fun onPostExecute(result: Void?) {}


    }


}