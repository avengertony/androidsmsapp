package com.infinitysms.usa.ui.dashboard

import android.content.Context
import com.infinitysms.usa.data.AppDataManager
import dagger.Module
import dagger.Provides

/**
 * Created byAndroid Tony on 23,March,2020
 */
@Module
class DashboardActivityModule {

    @ActivityScoped
    @Provides
    fun provideDashboardActi(context: Context, appDataManager: AppDataManager):
            DashboardActivityVM = DashboardActivityVM(context, appDataManager)

    annotation class ActivityScoped
}