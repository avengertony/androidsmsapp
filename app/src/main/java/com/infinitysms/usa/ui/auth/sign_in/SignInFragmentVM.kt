package com.infinitysms.usa.ui.auth.sign_in

import android.content.Context
import android.text.TextUtils
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.gson.Gson
import com.google.gson.JsonObject
import com.infinitysms.usa.R
import com.infinitysms.usa.data.AppDataManager
import com.infinitysms.usa.data.models.UIState
import com.infinitysms.usa.data.models.User
import com.infinitysms.usa.utils.Constants
import com.infinitysms.usa.utils.LogUtils
import com.infinitysms.usa.utils.ValidationUtil
import default
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableObserver
import io.reactivex.schedulers.Schedulers
import retrofit2.Response
import javax.inject.Inject

/**
 * Created byAndroid Tony on 23,March,2020
 */
class SignInFragmentVM @Inject constructor(private var context: Context,
                                           var appDataManager: AppDataManager) : ViewModel() {


    private val TAG = SignInFragmentVM::class.java.simpleName

    var user = MutableLiveData<User>()
    var uiState = MutableLiveData<UIState>().default(UIState(loading = false))

    private var compositeDisposable = CompositeDisposable()

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.dispose()
    }

    fun doLogin(phonenum: String, pinNo: String) {
        try {
            uiState.value = UIState(loading = true,success = false,error = null,message = null)
            if(!checkLoginValidation(phonenum,pinNo)) {
                return
            }

            val jsonObject = JsonObject()
            jsonObject.addProperty("phone_number", phonenum)
            jsonObject.addProperty("random_number", pinNo)
            jsonObject.addProperty("device_type", "A")

            val loginDisposable = appDataManager.login(jsonObject)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(object : DisposableObserver<Response<JsonObject>>() {
                    override fun onComplete() {
                        uiState.value = UIState(loading = false)
                    }

                    override fun onNext(response: Response<JsonObject>) {
                        try {
                            if (response.code() == 200 && response.body()!!.has(Constants.API.KEY.SUCCESS)
                                && response.body()!!.get(Constants.API.KEY.SUCCESS).asBoolean
                                && response.body()!!.has(Constants.API.KEY.DATA)
                            ) {
                                val data = response.body()!!.get(Constants.API.KEY.DATA).asJsonObject
                                if (data != null) {

        user.value = Gson().fromJson(data.toString(), User::class.java)

                                    uiState.value = UIState(loading = false,success = true)
                                } else {
                                    var message = context.resources.getString(R.string.something_went_wrong)
                                    if (response.body()!!.has(Constants.API.KEY.MESSAGE)) {
                                        message = response.body()!!.get(Constants.API.KEY.MESSAGE).asString
                                    } else if (response.body()!!.has(Constants.API.KEY.ERROR)) {
                                        message = response.body()!!.get(Constants.API.KEY.ERROR).asString
                                    }

                                    uiState.value = UIState(error = message)
                                }
                            } else if (response.body() != null) {
                                var message = context.resources.getString(R.string.something_went_wrong)
                                if (response.body()!!.has(Constants.API.KEY.MESSAGE)) {
                                    message = response.body()!!.get(Constants.API.KEY.MESSAGE).asString
                                } else if (response.body()!!.has(Constants.API.KEY.ERROR)) {
                                    message = response.body()!!.get(Constants.API.KEY.ERROR).asString
                                }

                                uiState.value = UIState(error = message)
                            } else {
                                uiState.value = UIState(error = context.getString(R.string.something_went_wrong))
                            }
                        } catch (e: Exception) {
                            LogUtils.e(TAG, e.message.toString())
                            uiState.value = UIState(loading = false, error = e.message)
                        }
                    }

                    override fun onError(e: Throwable) {
                        uiState.value = UIState(loading = false, error = e.message)
                    }
                })
            compositeDisposable.add(loginDisposable)
        } catch (e: Exception) {
            LogUtils.e(TAG, e.message.toString())
            uiState.value = UIState(loading = false, error = e.message)
        }
    }

    private fun checkLoginValidation( phonenum: String, pinNo: String): Boolean {
        var isValid = true

        if(TextUtils.isEmpty(phonenum)) {
            uiState.value = UIState(loading = false, error = "Enter phone number")
            isValid = false
            return isValid
        }
        if(TextUtils.isEmpty(pinNo)) {
            uiState.value = UIState(loading = false, error ="Enter pin number")
            isValid = false
            return isValid
        }

        return isValid
    }
}