package com.infinitysms.usa.ui.dashboard.alerts


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider

import com.infinitysms.usa.R
import com.infinitysms.usa.data.AppDataManager
import com.infinitysms.usa.di.module.AppViewModelFactory
import com.infinitysms.usa.ui.base.BaseFragment
import com.infinitysms.usa.utils.toolbar.FragmentToolbar
import com.infinitysms.usa.utils.toolbar.IHamburgerCallback
import com.infinitysms.usa.utils.toolbar.IMenuCallback
import javax.inject.Inject


class AlertsFragment : BaseFragment<AlertsFragmentVM>() {

    private val TAG = AlertsFragment::class.java.simpleName

    @Inject
    lateinit var appViewModelFactory: AppViewModelFactory

    private lateinit var alertsFragmentVM: AlertsFragmentVM

    @Inject
    lateinit var appDataManager: AppDataManager

    companion object {
        @JvmStatic
        fun newInstance() =
            AlertsFragment().apply {
                arguments = Bundle().apply {

                }
            }
    }

    override fun builder(): FragmentToolbar? {
        return FragmentToolbar.Builder()
            .withId(R.id.toolbar).withTitle(resources.getString(R.string.alerts))
            .withBackground()
            .withHamburger()
            .withHamburgerCallback(object : IHamburgerCallback {
                override fun onToolbarHamburgerClicked(view: View) {
                    toggleNavDrawer()
                }
            })
            .withMenu()
            .withMenuCallback(object : IMenuCallback {
                override fun onMenuClicked(view: View) {
                    showPopupMenu(view)
                }
            })
           // .withNotification()
            .withEvent()
            .build()
    }

    override fun getViewModel(): AlertsFragmentVM {
        alertsFragmentVM = ViewModelProvider(this@AlertsFragment, appViewModelFactory)
            .get(AlertsFragmentVM::class.java)
        return alertsFragmentVM
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {

        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        if (root == null) {
            root = inflater.inflate(R.layout.fragment_alerts, container, false) as ViewGroup
        }

        return root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        if (!isFragmentLoaded) {
            initInstances()
        }

        super.onViewCreated(view, savedInstanceState)
    }

    override fun onResume() {
        super.onResume()
        showBottomNavigation()
        unLockedNavDrawer()
    }

    private fun initInstances() {
        observeViewModel()
    }

    private fun observeViewModel() {
        alertsFragmentVM.uiState.observe(viewLifecycleOwner, Observer {
            if (it.loading) {

            }

            if (it.success) {

            }

            if (it.message != null) {

            }

            if (it.error != null) {
                showErrorToast(it.error!!)
            }

            if (it.messageWithAlert != null) {

            }
        })
    }

}
