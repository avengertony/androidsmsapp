package com.infinitysms.usa.ui.services

import android.Manifest
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.app.Service
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.os.IBinder
import android.util.Log
import androidx.annotation.Nullable
import androidx.core.app.ActivityCompat
import androidx.core.app.NotificationCompat
import com.infinitysms.usa.R
import com.infinitysms.usa.ui.dashboard.DashboardActivity
import java.util.*


class MyTimerService : Service() {


    @Nullable
    override fun onBind(intent: Intent): IBinder? {
        return null
    }


    override fun onCreate() {
        super.onCreate()
        val notificationIntent = Intent(this, DashboardActivity::class.java)
        notificationIntent.action = (Intent.ACTION_MAIN)
        notificationIntent.flags = (Intent.FLAG_ACTIVITY_SINGLE_TOP)
        notificationIntent.addCategory(Intent.CATEGORY_LAUNCHER)
        val pendingIntent = PendingIntent.getActivity(this, 0, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT)
        if (Build.VERSION.SDK_INT >= 26) {
            val CHANNEL_ID = "infinitysms_channel_01"
            val channel = NotificationChannel(
                CHANNEL_ID,
                "InfinitySMS Channel",
                NotificationManager.IMPORTANCE_DEFAULT
            )
            (getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager).createNotificationChannel(
                channel
            )
            val notification =
                NotificationCompat.Builder(this, CHANNEL_ID)
                    .setContentTitle("InfinitySMS")
                    .setContentText("Infinity SMS service running...")
                    .setSmallIcon(R.mipmap.ic_launcher_round)
                    .setContentIntent(pendingIntent)
                    .build()
            startForeground(1, notification)
        }
    }

    override fun onStartCommand(
        intent: Intent,
        flags: Int,
        startId: Int
    ): Int {
        Log.d(TAG, "onStartCommand: called.")

        startTimer(intent)


        //  getLocation()
        return START_NOT_STICKY
    }

    private fun startTimer(intent: Intent) {
       var STATUS= intent.getStringExtra("STATUS")
        // new Google API SDK v11 uses getFusedLocationProviderClient(this)
        val timer = Timer()
        if (ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.FOREGROUND_SERVICE
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            Log.d(
                TAG,
                "MyTimerService: stopping the FOREGROUND_SERVICE service."
            )
            timer.cancel()
            stopSelf()
            return
        }
        Log.d(
            TAG,
            "MyTimerService: getting FOREGROUND_SERVICE information."
        )

        timer.scheduleAtFixedRate(object : TimerTask() {
            override fun run() {
                val intent1local = Intent()
                intent1local.action = "Counter"
                COUNTER++
//                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//                    if (timeRemaining[0] == 1) NotificationUpdate(timeRemaining[0])
//                }
                if (COUNTER >= FINAL_COUNTER) {
                    // timer.cancel()
                    COUNTER = 0

                    intent1local.putExtra("TimeRemaining", COUNTER)
                    sendBroadcast(intent1local)
                }
                else if(STATUS!=null&&STATUS=="START")
                {
                    STATUS=null
                    intent1local.putExtra("TimeRemaining", COUNTER)
                    sendBroadcast(intent1local)
                }
            }
        }, 0, UPDATE_INTERVAL)

    }

    // ---------------------------------- LocationRequest ------------------------------------

    companion object {
        private const val TAG = "MyTimerService"
        private const val UPDATE_INTERVAL = 5000  /* 5 secs */.toLong()
        private const val FINAL_COUNTER: Long = 6 /* 30 sec */
        private var COUNTER: Long = 0

    }

}