package com.infinitysms.usa.ui.base

import android.content.Context
import android.graphics.Rect
import android.os.Bundle
import android.view.View
import android.view.ViewGroup
import android.view.ViewTreeObserver
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModel
import androidx.navigation.NavController
import com.infinitysms.usa.utils.Helper
import com.infinitysms.usa.utils.toolbar.FragmentToolbar
import com.infinitysms.usa.utils.toolbar.ToolbarManager
import dagger.android.support.AndroidSupportInjection

/**
 * Created by Android Tony on 23,March,2020
 */
abstract class BaseAuthFragment<out V : ViewModel> : Fragment() {

    private var mActivity: BaseAuthActivity<*>? = null
    private var mViewModel: V? = null
    protected var root: ViewGroup? = null
    var isFragmentLoaded = false
    lateinit var helper: Helper
    lateinit var toolbar: ToolbarManager


    val keyboardLayoutListener = ViewTreeObserver.OnGlobalLayoutListener {
        // navigation bar height
        var navigationBarHeight = 0
        var resourceId = resources.getIdentifier("navigation_bar_height", "dimen", "android")
        if (resourceId > 0) {
            navigationBarHeight = resources.getDimensionPixelSize(resourceId)
        }

        // status bar height
        var statusBarHeight = 0
        resourceId = resources.getIdentifier("status_bar_height", "dimen", "android")
        if (resourceId > 0) {
            statusBarHeight = resources.getDimensionPixelSize(resourceId)
        }

        // display window size for the app layout
        val rect = Rect()
        activity?.getWindow()?.getDecorView()?.getWindowVisibleDisplayFrame(rect)

        // screen height - (user app height + status + nav) ..... if non-zero, then there is a soft keyboard
        val keyboardHeight =
            root?.getRootView()?.getHeight()!! - (statusBarHeight + navigationBarHeight + rect.height())

        if (keyboardHeight <= 0) {
            onKeyboardClose()
        } else {
            //onShowKeyboard(keyboardHeight)
            onKeyboardOpen()
        }
    }

    open fun onKeyboardOpen() {

    }

    open fun onKeyboardClose() {

    }

    override fun onCreate(savedInstanceState: Bundle?) {

        mViewModel = getViewModel()
        setHasOptionsMenu(false)
        super.onCreate(savedInstanceState)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        //var mActivity: SDashboardActivity = activityS as SDashboardActivity

        if (this.builder() != null) {
            toolbar = ToolbarManager(this.builder()!!, view)
            toolbar.prepareToolbar(mActivity!!)
        }
        isFragmentLoaded = true
    }

    protected abstract fun builder(): FragmentToolbar?

    override fun onAttach(context: Context) {
        performDependencyInjection()

        if (context is BaseAuthActivity<*>) {
            val activity = context as BaseAuthActivity<*>?
            this.mActivity = activity
            activity!!.onFragmentAttached(this)
            helper = mActivity!!.helper
        }
        super.onAttach(context)
    }

    override fun onResume() {
        super.onResume()

        hideKeyboard()
    }

    override fun onStop() {
        super.onStop()

        hideKeyboard()
    }

    override fun onDetach() {
        mActivity = null
        super.onDetach()
    }

    override fun onDestroyView() {
        if (root?.getParent() != null) {
            (root?.getParent() as ViewGroup).removeView(root)
        }
        super.onDestroyView()
    }

    override fun onDestroy() {
        super.onDestroy()

        isFragmentLoaded = false
    }

    override fun onCreateAnimation(transit: Int, enter: Boolean, nextAnim: Int): Animation? {
        var animation: Animation? = super.onCreateAnimation(transit, enter, nextAnim)

        if (animation == null && nextAnim != 0) {
            animation = AnimationUtils.loadAnimation(activity, nextAnim)
        }

        if (animation != null) {
            view!!.setLayerType(View.LAYER_TYPE_HARDWARE, null)

            animation.setAnimationListener(object : Animation.AnimationListener {
                override fun onAnimationStart(animation: Animation) {
                    mActivity?.onTransitionStarted()
                }

                override fun onAnimationEnd(animation: Animation) {
                    if (view != null) {
                        view!!.setLayerType(View.LAYER_TYPE_NONE, null)
                    }
                    mActivity?.onTransitionEnded()
                }

                override fun onAnimationRepeat(animation: Animation) {
                    mActivity?.onTransitionRepeated()
                }
                // ...other AnimationListener methods go here...
            })
        }
        return animation
    }

    fun getBaseActivity(): BaseAuthActivity<*>? {
        return mActivity
    }

    /**Use to check internet connectivity*/
    fun isNetworkConnected(): Boolean {
        return mActivity != null && mActivity!!.isNetworkConnected()
    }

    /**Use to hide keyboard anywhere in app*/
    fun hideKeyboard() {
        if (mActivity != null) {
            mActivity!!.hideKeyboard()
        }
    }

    fun showShortToast(message: String) {
        if (mActivity != null) {
            mActivity!!.showShortToast(message)
        }
    }

    /**Use to show error message in toast*/
    fun showErrorToast(message: String) {
        if (mActivity != null) {
            mActivity!!.showErrorToast(message)
        }
    }

    /**Use to show info message in toast*/
    fun showInfoToast(message: String) {
        if (mActivity != null) {
            mActivity!!.showInfoToast(message)
        }
    }

    /**Use to show warning message in toast*/
    fun showWarningToast(message: String) {
        if (mActivity != null) {
            mActivity!!.showWarningToast(message)
        }
    }

    /**Use to show success message in toast*/
    fun showSuccessToast(message: String) {
        if (mActivity != null) {
            mActivity!!.showSuccessToast(message)
        }
    }

    fun showCustomAlertDialog(title: String, message: String) {
        if (mActivity != null) {
            mActivity!!.showCustomAlertDialog(title, message)
        }
    }

    /**Use to show message in default alert dialog*/
    fun showDefaultAlertDialog(title: String, message: String) {
        if (mActivity != null) {
            mActivity!!.showDefaultAlertDialog(title, message)
        }
    }

    /**Use to show loading when don't allow to move from screen
     * while api calling*/
    fun showLoadingDialog() {
        if (mActivity != null) {
            mActivity!!.showLoadingDialog()
        }
    }

    /**Use to hide loading when don't allow to move from screen
     * while api calling*/
    fun dismissLoadingDialog() {
        if (mActivity != null) {
            mActivity!!.dismissLoadingDialog()
        }
    }

    fun showNotificationDialog() {
        if (mActivity != null) {
            mActivity!!.showNotificationDialog()
        }
    }

    /**Use to change application language*/
    fun updateLocale(lang: String) {
        if (mActivity != null) {
            mActivity!!.updateLocale(lang)
        }
    }

    /**Use to open or close navigation drawer*/
    fun toggleNavDrawer() {
        if (mActivity != null) {
            mActivity!!.toggleNavDrawer()
        }
    }

    /**Use to don't allow to open nav draser*/
    open fun lockedNavDrawer() {
        if (mActivity != null) {
            mActivity!!.lockedNavDrawer()
        }
    }

    /**Use to allow to open nav draser*/
    open fun unLockedNavDrawer() {
        if (mActivity != null) {
            mActivity!!.unLockedNavDrawer()
        }
    }

    /**Use to hide bottom navigation in full screen where back button is enable*/
    fun hideBottomNavigation() {
        if (mActivity != null) {
            mActivity!!.hideBottomNavigation()
        }
    }

    /**Use to show bottom navigation after coming back to dashboard*/
    fun showBottomNavigation() {
        if (mActivity != null) {
            mActivity!!.showBottomNavigation()
        }
    }

    /**Use to get dashboard nav controller*/
    open fun getDashboardNavController(): NavController? {
        if (mActivity != null) {
            return mActivity!!.getDashboardNavController()
        }

        return null
    }

    /**Use to show popup menu on click of overflow menu icon*/
    open fun showPopupMenu(v: View) {
        if (mActivity != null) {
            return mActivity!!.showPopupMenu(v)
        }
    }

    open fun onBackPressed(): Boolean {
        return false
    }

    private fun performDependencyInjection() {
        AndroidSupportInjection.inject(this)
    }

    interface Callback {

        fun onFragmentAttached(fragment: BaseAuthFragment<*>)

        fun onFragmentDetached(tag: String)

        fun onTransitionStarted()

        fun onTransitionEnded()

        fun onTransitionRepeated()
    }

    /**
     * Override for set view model
     *
     * @return view model instance
     */
    abstract fun getViewModel(): V

}