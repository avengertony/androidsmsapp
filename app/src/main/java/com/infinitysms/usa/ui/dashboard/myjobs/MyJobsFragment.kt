package com.infinitysms.usa.ui.dashboard.myjobs


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import com.infinitysms.usa.R
import com.infinitysms.usa.data.AppDataManager
import com.infinitysms.usa.data.models.MyJobBean
import com.infinitysms.usa.data.models.WeekTitleData
import com.infinitysms.usa.di.module.AppViewModelFactory
import com.infinitysms.usa.ui.base.BaseFragment
import com.infinitysms.usa.utils.toolbar.FragmentToolbar
import com.infinitysms.usa.utils.toolbar.IBackButtonCallback
import kotlinx.android.synthetic.main.fragment_my_jobs.*
import pegasus.driver.data.model.MyJobsScreenData
import pegasus.driver.mvvm.dashboard.myjobs.ShowMyJobsAdapter
import java.util.*
import javax.inject.Inject


class MyJobsFragment : BaseFragment<MyJobsFragmentVM>() {

    private val TAG = MyJobsFragment::class.java.simpleName

    companion object {
        @JvmStatic
        fun newInstance() =
            MyJobsFragment().apply {
                arguments = Bundle().apply {

                }
            }
    }

    @Inject
    lateinit var appViewModelFactory: AppViewModelFactory
    private lateinit var myJobsFragmentVM: MyJobsFragmentVM

    @Inject
    lateinit var appDataManager: AppDataManager

    lateinit var myJobsData: MyJobsScreenData

    var titleData = ArrayList<WeekTitleData>()


    val millisInADay = 86400000
    override fun builder(): FragmentToolbar? {
        return FragmentToolbar.Builder()
            .withId(R.id.toolbar)
            .withTitle(getString(R.string.my_jobs))
            .withBackground()
            .withBackButton()
            //.withNotification()
            .withBackButtonCallback(object : IBackButtonCallback {
                override fun onToolbarBackPressed(view: View) {
                    Navigation.findNavController(this@MyJobsFragment.view!!).navigateUp()
                }
            })
            .build()
    }

    override fun getViewModel(): MyJobsFragmentVM {
        myJobsFragmentVM =
            ViewModelProvider(this@MyJobsFragment, appViewModelFactory)
                .get(MyJobsFragmentVM::class.java)
        return myJobsFragmentVM
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {

        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        if (root == null) {
            root =
                inflater.inflate(
                    R.layout.fragment_my_jobs,
                    container,
                    false
                ) as ViewGroup
        }

        return root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        if (!isFragmentLoaded) {
            initInstances()
        }

        super.onViewCreated(view, savedInstanceState)
    }

    override fun onResume() {
        hideBottomNavigation()
        lockedNavDrawer()
        super.onResume()
    }

    private fun initInstances() {
        observeViewModel()

        myJobsData = MyJobsScreenData(0.0, 0, arrayListOf())

        rvJobs.apply {
            layoutManager = LinearLayoutManager(requireContext())
            adapter = ShowMyJobsAdapter(requireContext(), myJobsData.data)
        }

        addCurrentWeek()
//        adapWeeksTitle = AdapterWeeksTitle(titleData)
//        rvTitle.layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
//        rvTitle.adapter = adapWeeksTitle

        if (titleData.size > 0) {
            val visibleItem = titleData[0]
            myJobsFragmentVM.getMyJobsList(visibleItem.millisFirstDay, visibleItem.millisLastDay, 7)
            unselectAlldaysOfWeek()
            tvAll.setTextColor(ContextCompat.getColor(requireContext(), R.color.green_btn))

        }
        ivPrevious.setOnClickListener {
            var updatedPosition =
                (rvTitle.layoutManager as LinearLayoutManager).findFirstCompletelyVisibleItemPosition() - 1
            ivNext.setImageResource(R.mipmap.right_arrow)
            if (-1 == updatedPosition) {
                addPreviousWeek()
                if (titleData.size > 0) {
                    val visibleItem = titleData[0]
                    myJobsFragmentVM.getMyJobsList(
                        visibleItem.millisFirstDay,
                        visibleItem.millisLastDay, 7
                    )
                }

            } else {
                rvTitle.smoothScrollToPosition(updatedPosition)
                val visibleItem = titleData[updatedPosition]
                myJobsFragmentVM.getMyJobsList(
                    visibleItem.millisFirstDay,
                    visibleItem.millisLastDay, 7
                )
            }
            unselectAlldaysOfWeek()
            tvAll.setTextColor(ContextCompat.getColor(requireContext(), R.color.green_btn))
        }


        ivNext.setOnClickListener {
            var updatedPosition =
                (rvTitle.layoutManager as LinearLayoutManager).findFirstCompletelyVisibleItemPosition() + 1
            if (titleData.size - 1 == updatedPosition)
                ivNext.setImageResource(R.mipmap.right_arrow_gray)

            if (titleData.size > updatedPosition) {

                rvTitle.smoothScrollToPosition(updatedPosition)
                val visibleItem = titleData[updatedPosition]
                myJobsFragmentVM.getMyJobsList(
                    visibleItem.millisFirstDay,
                    visibleItem.millisLastDay, 7
                )
            }
            unselectAlldaysOfWeek()
            tvAll.setTextColor(ContextCompat.getColor(requireContext(), R.color.green_btn))

        }

        addWeekDayClicks()
    }

    private fun observeViewModel() {
        myJobsFragmentVM.uiState.observe(viewLifecycleOwner, Observer {
            if (it.loading) {
                //  loadingLayout.makeVisible()
            } else
            //  loadingLayout.makeGone()
                if (it.success) {

                }

            if (it.message != null) {
                showInfoToast(it.message!!)
            }

            if (it.error != null) {
                showErrorToast(it.error!!)
            }

            if (it.messageWithAlert != null) {
                showDefaultAlertDialog(getString(R.string.message), it.messageWithAlert!!)
            }
        })
        myJobsFragmentVM.myJobBean.observe(viewLifecycleOwner, Observer {
            if (it != null)
                loadUserJobs(it)
        })
    }

    private fun loadUserJobs(data: MyJobBean) {

        myJobsData.data.clear()
        myJobsData.data.addAll(data.data)
        if (myJobsData.data.size == 0) {
            tvNoData.visibility = View.VISIBLE
        } else
            tvNoData.visibility = View.GONE

        var amount = 0.0
        for (tempData in myJobsData.data) {
            amount += tempData._source.driver_income
        }
        myJobsData.total_fare = amount

        tvRides.text =  data.total_record!!.toString()
        tvTotalSelling.text = "£ " + String.format("%.02f", myJobsData.total_fare)
        rvJobs.adapter!!.notifyDataSetChanged()
    }

    fun addWeekDayClicks() {

        tvAll.setOnClickListener {
            unselectAlldaysOfWeek()
            tvAll.setTextColor(ContextCompat.getColor(requireContext(), R.color.green_btn))

            if (titleData.size > 0) {
                var currentPosition =
                    (rvTitle.layoutManager as LinearLayoutManager).findFirstCompletelyVisibleItemPosition()
                val visibleItem = titleData[currentPosition]
                myJobsFragmentVM.getMyJobsList(
                    visibleItem.millisFirstDay,
                    visibleItem.millisLastDay, 7
                )
            }
        }

        tvMon.setOnClickListener {
            unselectAlldaysOfWeek()
            tvMon.setTextColor(ContextCompat.getColor(requireContext(), R.color.green_btn))

            if (titleData.size > 0) {
                var currentPosition =
                    (rvTitle.layoutManager as LinearLayoutManager).findFirstCompletelyVisibleItemPosition()
                val visibleItem = titleData[currentPosition]
                myJobsFragmentVM.getMyJobsList(
                    visibleItem.millisFirstDay,
                    visibleItem.millisFirstDay + millisInADay,
                    1
                )
            }
        }

        tvTue.setOnClickListener {
            unselectAlldaysOfWeek()
            tvTue.setTextColor(ContextCompat.getColor(requireContext(), R.color.green_btn))

            if (titleData.size > 0) {
                var currentPosition =
                    (rvTitle.layoutManager as LinearLayoutManager).findFirstCompletelyVisibleItemPosition()
                val visibleItem = titleData[currentPosition]
                myJobsFragmentVM.getMyJobsList(
                    visibleItem.millisFirstDay + (millisInADay * 1),
                    visibleItem.millisFirstDay + (millisInADay * 2), 2
                )
            }
        }

        tvWed.setOnClickListener {
            unselectAlldaysOfWeek()
            tvWed.setTextColor(ContextCompat.getColor(requireContext(), R.color.green_btn))

            if (titleData.size > 0) {
                var currentPosition =
                    (rvTitle.layoutManager as LinearLayoutManager).findFirstCompletelyVisibleItemPosition()
                val visibleItem = titleData[currentPosition]
                myJobsFragmentVM.getMyJobsList(
                    visibleItem.millisFirstDay + (millisInADay * 2),
                    visibleItem.millisFirstDay + (millisInADay * 3), 3
                )
            }
        }

        tvThu.setOnClickListener {
            unselectAlldaysOfWeek()
            tvThu.setTextColor(ContextCompat.getColor(requireContext(), R.color.green_btn))

            if (titleData.size > 0) {
                var currentPosition =
                    (rvTitle.layoutManager as LinearLayoutManager).findFirstCompletelyVisibleItemPosition()
                val visibleItem = titleData[currentPosition]
                myJobsFragmentVM.getMyJobsList(
                    visibleItem.millisFirstDay + (millisInADay * 3),
                    visibleItem.millisFirstDay + (millisInADay * 4), 4
                )
            }
        }


        tvFri.setOnClickListener {
            unselectAlldaysOfWeek()
            tvFri.setTextColor(ContextCompat.getColor(requireContext(), R.color.green_btn))

            if (titleData.size > 0) {
                var currentPosition =
                    (rvTitle.layoutManager as LinearLayoutManager).findFirstCompletelyVisibleItemPosition()
                val visibleItem = titleData[currentPosition]
                myJobsFragmentVM.getMyJobsList(
                    visibleItem.millisFirstDay + (millisInADay * 4),
                    visibleItem.millisFirstDay + (millisInADay * 5), 5
                )
            }
        }


        tvSat.setOnClickListener {
            unselectAlldaysOfWeek()
            tvSat.setTextColor(ContextCompat.getColor(requireContext(), R.color.green_btn))

            if (titleData.size > 0) {
                var currentPosition =
                    (rvTitle.layoutManager as LinearLayoutManager).findFirstCompletelyVisibleItemPosition()
                val visibleItem = titleData[currentPosition]
                myJobsFragmentVM.getMyJobsList(
                    visibleItem.millisFirstDay + (millisInADay * 5),
                    visibleItem.millisFirstDay + (millisInADay * 6), 6
                )
            }
        }

        tvSun.setOnClickListener {
            unselectAlldaysOfWeek()
            tvSun.setTextColor(ContextCompat.getColor(requireContext(), R.color.green_btn))

            if (titleData.size > 0) {
                var currentPosition =
                    (rvTitle.layoutManager as LinearLayoutManager).findFirstCompletelyVisibleItemPosition()
                val visibleItem = titleData[currentPosition]
                myJobsFragmentVM.getMyJobsList(
                    visibleItem.millisFirstDay + (millisInADay * 6),
                    visibleItem.millisFirstDay + (millisInADay * 7), 0
                )
            }
        }


    }

    private fun unselectAlldaysOfWeek() {
        tvAll.setTextColor(ContextCompat.getColor(requireContext(), R.color.colorBlackLight))
        tvMon.setTextColor(ContextCompat.getColor(requireContext(), R.color.colorBlackLight))
        tvTue.setTextColor(ContextCompat.getColor(requireContext(), R.color.colorBlackLight))
        tvWed.setTextColor(ContextCompat.getColor(requireContext(), R.color.colorBlackLight))
        tvThu.setTextColor(ContextCompat.getColor(requireContext(), R.color.colorBlackLight))
        tvFri.setTextColor(ContextCompat.getColor(requireContext(), R.color.colorBlackLight))
        tvSat.setTextColor(ContextCompat.getColor(requireContext(), R.color.colorBlackLight))
        tvSun.setTextColor(ContextCompat.getColor(requireContext(), R.color.colorBlackLight))
    }

    private fun addCurrentWeek() {

        titleData.add(myJobsFragmentVM.getCurrentWeekTitle())

    }

    private fun addPreviousWeek() {

        titleData.add(0, myJobsFragmentVM.getPreviousWeekTitle(titleData[0].millisFirstDay))
        //adapWeeksTitle.notifyItemInserted(0)
        rvTitle.smoothScrollToPosition(0)

    }
}
