package com.infinitysms.usa.ui.dashboard.my_profile

import android.content.Context
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.gson.Gson
import com.google.gson.JsonObject
import com.infinitysms.usa.R
import com.infinitysms.usa.data.AppDataManager
import com.infinitysms.usa.data.models.ProfileBean
import com.infinitysms.usa.data.models.UIState
import com.infinitysms.usa.utils.Constants
import com.infinitysms.usa.utils.LogUtils
import default
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableObserver
import io.reactivex.schedulers.Schedulers
import org.json.JSONObject
import retrofit2.Response
import javax.inject.Inject

/**
 * Created by Android Tony on 18,February,2020
 */
class MyProfileFragmentVM @Inject constructor(private var context: Context, var appDataManager: AppDataManager) :
    ViewModel() {


    private val TAG = "MyProfileFragmentVM"

    var uiState = MutableLiveData<UIState>().default(UIState(loading = false))
    var profileBean = MutableLiveData<ProfileBean>()

    private var compositeDisposable = CompositeDisposable()

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.dispose()
    }

    fun getUserDetail() {
        try {
            uiState.value = UIState(loading = true)
            val jsonObject = JsonObject()
            jsonObject.addProperty("company_id", appDataManager.getUserData()?.company_id!!)
            jsonObject.addProperty("vehicle_id", appDataManager.getUserData()?.phone_number  !!)

            val loginDisposable = appDataManager.getMyProfile(jsonObject)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(object : DisposableObserver<Response<JsonObject>>() {
                    override fun onComplete() {
                        uiState.value = UIState(loading = false)
                    }

                    override fun onNext(response: Response<JsonObject>) {
                        try {
                            if (response.code() == 200 && response.body() != null) {
                            if (response.code() == 200 && response.body()!!.has(Constants.API.KEY.SUCCESS)
                                && response.body()!!.get(Constants.API.KEY.SUCCESS).asBoolean

                            ) {
                                val data = response.body()
                                if (data != null) {
                                    val lEmployeeDetail = Gson().fromJson(data.toString(), ProfileBean::class.java)
                                    profileBean.value = lEmployeeDetail
                                    uiState.value = UIState(loading = false)
                                } else {

                                    if (response.body()!!.has("message")) {
                                        var message = response.body()!!.get("message").asString
                                        uiState.value = UIState(loading = false, error = message)
                                    }
                                }
                            }
                       } else if (response.code() == 400) {
                            val json = JSONObject(response.errorBody()?.string())
                            var message = json.getString("errorDetails")
                            uiState.value = UIState(loading = false, error = message)
                        }

                            else if (response.body() != null) {
                                var message = context.resources.getString(R.string.something_went_wrong)
                                if (response.body()!!.has(Constants.API.KEY.MESSAGE)) {
                                    message = response.body()!!.get(Constants.API.KEY.MESSAGE).asString
                                } else if (response.body()!!.has(Constants.API.KEY.ERROR)) {
                                    message = response.body()!!.get(Constants.API.KEY.ERROR).asString
                                }

                                uiState.value = UIState(error = message)
                            } else {
                                uiState.value = UIState(error = context.getString(R.string.something_went_wrong))
                            }
                        } catch (e: Exception) {
                            LogUtils.e(TAG, e.message.toString())
                            uiState.value = UIState(loading = false, error = e.message)
                        }
                    }

                    override fun onError(e: Throwable) {
                        uiState.value = UIState(loading = false, error = e.message)
                    }
                })
            compositeDisposable.add(loginDisposable)
        } catch (e: Exception) {
            LogUtils.e(TAG, e.message.toString())
            uiState.value = UIState(loading = false, error = e.message)
        }
    }
}