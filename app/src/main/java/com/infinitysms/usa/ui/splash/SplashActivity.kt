package com.infinitysms.usa.ui.splash

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.provider.Settings
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import com.infinitysms.usa.R
import com.infinitysms.usa.data.AppDataManager
import com.infinitysms.usa.di.module.AppViewModelFactory
import com.infinitysms.usa.ui.auth.AuthActivity
import com.infinitysms.usa.ui.base.BaseAuthActivity
import com.infinitysms.usa.ui.dashboard.DashboardActivity
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasAndroidInjector
import javax.inject.Inject

class SplashActivity : BaseAuthActivity<SplashActivityViewModel>(), HasAndroidInjector {

    companion object {
        fun startInstanceWithBackStackCleared(context: Context?) {
            val intent = Intent(context, SplashActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
            context?.startActivity(intent)
            (context as AppCompatActivity).overridePendingTransition(
                R.anim.activity_enter,
                R.anim.activity_leave
            )
        }
    }

    @Inject
    lateinit var splashActivityViewModel: SplashActivityViewModel

    @Inject
    lateinit var androidInjector: DispatchingAndroidInjector<Any>

    @Inject
    lateinit var appViewModelFactory: AppViewModelFactory

    @Inject
    lateinit var appDataManager: AppDataManager
    private var SPLASH_TIME_OUT = 2000

    override fun androidInjector(): AndroidInjector<Any> {
        return androidInjector
    }

    override fun getViewModel(): SplashActivityViewModel {
        splashActivityViewModel = ViewModelProvider(this@SplashActivity, appViewModelFactory)
            .get(SplashActivityViewModel::class.java)
        return splashActivityViewModel
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )
        setContentView(R.layout.activity_splash)


            launchNextActivity()

    }

    private fun launchNextActivity() {
        Handler().postDelayed({
            val user = appDataManager.getUserData()
           // val token = appDataManager.getAuthToken()

           // if (user != null && token != null) {
            if (user != null) {
                startInstanceWithBackStackCleared(this@SplashActivity)
            } else {
                AuthActivity.startInstanceWithBackStackCleared(this@SplashActivity)
            }
        }, SPLASH_TIME_OUT.toLong())
    }

    fun startInstanceWithBackStackCleared(context: Context?) {
        val intent = Intent(context, DashboardActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
        context?.startActivity(intent)
        (context as AppCompatActivity).overridePendingTransition(
            R.anim.activity_enter,
            R.anim.activity_leave
        )
    }

}
