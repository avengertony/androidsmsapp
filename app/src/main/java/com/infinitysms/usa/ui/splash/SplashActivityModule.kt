package com.infinitysms.usa.ui.splash

import com.infinitysms.usa.data.AppDataManager
import dagger.Module
import dagger.Provides

/**
 * Created byAndroid Tony on 23,March,2020
 */
@Module
class SplashActivityModule {

    @ActivityScoped
    @Provides
    fun provideSplashActivityViewModel(appDataManager: AppDataManager):
            SplashActivityViewModel = SplashActivityViewModel(appDataManager)

    annotation class ActivityScoped
}