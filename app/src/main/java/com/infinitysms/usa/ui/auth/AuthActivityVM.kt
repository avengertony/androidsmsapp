package com.infinitysms.usa.ui.auth

import androidx.lifecycle.ViewModel
import com.infinitysms.usa.data.AppDataManager
import javax.inject.Inject

/**
 * Created byAndroid Tony on 23,March,2020
 */
class AuthActivityVM
@Inject
constructor
    (var appDataManager: AppDataManager) : ViewModel()