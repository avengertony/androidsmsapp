package com.infinitysms.usa.ui.dashboard

import android.content.Context
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.gson.JsonObject
import com.infinitysms.usa.R
import com.infinitysms.usa.data.AppDataManager
import com.infinitysms.usa.data.models.UIState
import default
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableObserver
import io.reactivex.schedulers.Schedulers
import org.json.JSONObject
import retrofit2.Response
import javax.inject.Inject

/**
 * Created byAndroid Tony on 23,March,2020
 */
class DashboardActivityVM
@Inject
constructor
    (private var context: Context, var appDataManager: AppDataManager) : ViewModel() {

    private val TAG = DashboardActivityVM::class.java.simpleName

    var uiState = MutableLiveData<UIState>().default(UIState(loading = false))
    var bookingdData = MutableLiveData<JsonObject>()

    private var compositeDisposable = CompositeDisposable()

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.dispose()
    }

}