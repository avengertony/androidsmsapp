package com.infinitysms.usa.ui.dashboard.home


import android.Manifest
import android.annotation.SuppressLint
import android.content.Context
import android.content.pm.PackageManager
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.media.SoundPool
import android.os.*
import android.telephony.SubscriptionInfo
import android.telephony.SubscriptionManager
import android.telephony.TelephonyManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.app.ActivityCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.infinitysms.usa.R
import com.infinitysms.usa.data.AppDataManager
import com.infinitysms.usa.data.models.MessageData
import com.infinitysms.usa.di.module.AppViewModelFactory
import com.infinitysms.usa.ui.base.BaseFragment
import com.infinitysms.usa.ui.dashboard.DashboardActivity
import com.infinitysms.usa.utils.Constants
import com.infinitysms.usa.utils.toolbar.FragmentToolbar
import com.infinitysms.usa.utils.toolbar.IHamburgerCallback
import kotlinx.android.synthetic.main.dialog_messsage_alert.view.*
import kotlinx.android.synthetic.main.fragment_home.*
import pegasus.driver.mvvm.dashboard.myjobs.ShowMyMessageAdapter
import javax.inject.Inject


class HomeFragment : BaseFragment<HomeFragmentVM>(), View.OnClickListener,
    DashboardActivity.StatusCheck {


    private val MESSAGE_UPDATE_TEXT_CHILD_THREAD = 1

    private var updateUIHandler: Handler? = null
    private var ride_status: String? = null
    var linearLayoutManager: LinearLayoutManager? = null


    private val TAG = HomeFragment::class.java.simpleName

    @Inject
    lateinit var appViewModelFactory: AppViewModelFactory

    private lateinit var homeFragmentVM: HomeFragmentVM

    @Inject
    lateinit var appDataManager: AppDataManager

    companion object {
        @JvmStatic
        fun newInstance() =
            HomeFragment().apply {
                arguments = Bundle().apply {

                }
            }

        var loadingLayout: ConstraintLayout? = null
        var mContext: AppCompatActivity? = null

        var tvregNo: TextView? = null
        var tvdriverStatus: TextView? = null
        var labelendShift: TextView? = null
        var switchonlineIV: ImageView? = null
        var messagesRV: RecyclerView? = null


    }

    override fun builder(): FragmentToolbar? {
        return FragmentToolbar.Builder()
            .withId(R.id.toolbar).withTitle(resources.getString(R.string.home))
            .withBackground()
            .withHamburger()
            .withHamburgerCallback(object : IHamburgerCallback {
                override fun onToolbarHamburgerClicked(view: View) {
                    toggleNavDrawer()
                }
            })

            .build()
    }

    override fun getViewModel(): HomeFragmentVM {
        homeFragmentVM = ViewModelProvider(this@HomeFragment, appViewModelFactory)
            .get(HomeFragmentVM::class.java)
        return homeFragmentVM
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {

        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        if (root == null) {
            root = inflater.inflate(R.layout.fragment_home, container, false) as ViewGroup
        }

        return root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        if (!isFragmentLoaded) {
            initInstances()
        }

        super.onViewCreated(view, savedInstanceState)
    }

    override fun onResume() {
        super.onResume()
        hideBottomNavigation()
        unLockedNavDrawer()

    }

    private fun initInstances() {

        tvregNo = root?.findViewById(R.id.tvRegNo)
        tvdriverStatus = root?.findViewById(R.id.tvDriverStatus)
        messagesRV = root?.findViewById(R.id.messageRV)
        labelendShift = root?.findViewById(R.id.labelEndShift)
        switchonlineIV = root?.findViewById(R.id.switchOnline)
        loadingLayout = root?.findViewById(R.id.loadingLayouts)
        tvregNo?.text = appDataManager.getUserData()?.phone_number

        observeViewModel()
        var user = homeFragmentVM.appDataManager.getUserData()
        //ivDriverThumb.loadUrl(activity!!, BuildConfig.IMG_BASE_URL + user.user_image!!)
        tvDriverName.text = user?.device_name //+ " (" + user.company_id + ")"
        labelendShift?.setOnClickListener(this)
        switchonlineIV?.setOnClickListener(this)

        createUpdateUiHandler()
        (activity as DashboardActivity).setListener(this)
    }

    private fun observeViewModel() {
        homeFragmentVM.uiState.observe(viewLifecycleOwner, Observer {
            if (it.loading) {

            }

            if (it.success) {
            }

            if (it.message != null) {

            }

            if (it.messageWithAlert != null) {
                showDefaultAlertDialog(getString(R.string.message), it.messageWithAlert!!)
            }
        })
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.labelEndShift -> {
                clickToggle()
            }
            R.id.switchOnline -> {
                clickToggle()
            }

        }
    }

    private fun clickToggle() {

        if (!isNetworkConnected())
            return

            if (appDataManager.getDriverOnline()) {
                appDataManager.setDriverOnline(false)
                switchonlineIV?.setImageResource(R.drawable.ic_toggle)
                labelendShift?.text = getString(R.string.start_shift)


                tvregNo?.text = appDataManager.getUserData()?.phone_number

                tvdriverStatus?.setText(R.string.offline)

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    tvdriverStatus?.setTextColor(
                        mContext?.resources?.getColor(
                            R.color.red,
                            mContext?.theme
                        )!!
                    )
                } else
                    tvdriverStatus?.setTextColor(mContext?.resources?.getColor(R.color.red)!!)

                loadingLayout?.visibility = View.VISIBLE

                (mContext as DashboardActivity).stopService_WebSocket()
            } else {
                appDataManager.setDriverOnline(true)

                switchonlineIV?.setImageResource(R.drawable.ic_toggle_active)
                labelendShift?.text = getString(R.string.end_shift)
                tvdriverStatus?.setText(R.string.available)

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    tvdriverStatus?.setTextColor(
                        mContext?.resources?.getColor(
                            R.color.colorAccent,
                            mContext?.theme
                        )!!
                    )
                } else
                    tvdriverStatus?.setTextColor(mContext?.resources?.getColor(R.color.colorAccent)!!)

                if (mContext != null) {
                    loadingLayout?.visibility = View.VISIBLE
                    (mContext as DashboardActivity).startWebSocket()
                }

        }

    }

    fun switchToggle() {
        if (appDataManager.getDriverOnline()) {
            appDataManager.setDriverOnline(true)

            switchonlineIV?.setImageResource(R.drawable.ic_toggle_active)
            labelendShift?.text = getString(R.string.end_shift)
            tvdriverStatus?.setText(R.string.available)

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                tvdriverStatus?.setTextColor(
                    mContext?.resources?.getColor(
                        R.color.colorAccent,
                        mContext?.theme
                    )!!
                )
            } else
                tvdriverStatus?.setTextColor(mContext?.resources?.getColor(R.color.colorAccent)!!)

            loadingLayout?.visibility = View.VISIBLE

            (mContext as DashboardActivity).startWebSocket()

        } else {
            appDataManager.setDriverOnline(false)
            switchonlineIV?.setImageResource(R.drawable.ic_toggle)
            labelendShift?.text = getString(R.string.start_shift)

            tvregNo?.text = appDataManager.getUserData()?.phone_number

            tvdriverStatus?.setText(R.string.offline)
            loadingLayout?.visibility = View.GONE
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                tvdriverStatus?.setTextColor(
                    mContext?.resources?.getColor(
                        R.color.red,
                        mContext?.theme
                    )!!
                )
            } else
                tvdriverStatus?.setTextColor(mContext?.resources?.getColor(R.color.red)!!)

            (mContext as DashboardActivity).stopService_WebSocket()

        }
    }

    override fun changeToggle(boolean: Boolean) {
        if (boolean)
            switchToggle()
    }

    override fun setHomeData() {
        try {


            if (ride_status.equals("Available") || ride_status.equals("Accepted") ||
                ride_status?.contains("Decline Penalty")!!
            ) {
                loadingLayout?.visibility = View.GONE
                switchonlineIV?.setImageResource(R.drawable.ic_toggle_active)
                labelendShift?.text = mContext?.resources?.getString(R.string.end_shift)
                if (ride_status?.contains("Decline Penalty")!!) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        tvdriverStatus?.setTextColor(
                            mContext?.resources?.getColor(
                                R.color.red,
                                mContext?.theme
                            )!!
                        )
                    } else
                        tvdriverStatus?.setTextColor(mContext?.resources?.getColor(R.color.red)!!)
                } else {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        tvdriverStatus?.setTextColor(
                            mContext?.resources?.getColor(
                                R.color.colorAccent,
                                mContext?.theme
                            )!!
                        )
                    } else
                        tvdriverStatus?.setTextColor(mContext?.resources?.getColor(R.color.colorAccent)!!)
                }

            } else {
                loadingLayout?.visibility = View.GONE
                switchonlineIV?.setImageResource(R.drawable.ic_toggle)
                labelendShift?.text = mContext?.resources?.getString(R.string.start_shift)
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    tvdriverStatus?.setTextColor(
                        mContext?.resources?.getColor(
                            R.color.red,
                            mContext?.theme
                        )!!
                    )
                } else
                    tvdriverStatus?.setTextColor(mContext?.resources?.getColor(R.color.red)!!)


            }


        } catch (e: Exception) {
            e.printStackTrace()
            try {
                dismissLoadingDialog()
            } catch (ee: Exception) {
                ee.printStackTrace()
            }
        }
    }


    private fun createUpdateUiHandler() {
        if (updateUIHandler == null) {
            updateUIHandler = object : Handler() {
                @SuppressLint("HandlerLeak")
                override fun handleMessage(msg: Message) {
                    // Means the message is sent from child thread.
                    if (msg.what === MESSAGE_UPDATE_TEXT_CHILD_THREAD) {
                        // Update ui in main thread.
                        updateText()
                    }
                }
            }
        }
    }

    private fun updateText() {
        tvRegNo?.text = appDataManager.getUserData()?.phone_number
    }

    override fun onAttach(context: Context) {

        if (context is DashboardActivity) {
            mContext = context
        }
        super.onAttach(context)
    }

fun setMessageAdapter()
    {
        var messageList2= appDataManager.getMessageList()
        if(messageList2!=null) {
            var messageList=ArrayList<MessageData>()
            messageList.addAll(messageList2)
            messageList.reverse()
            linearLayoutManager = LinearLayoutManager(this.activity)
            messagesRV?.setLayoutManager(linearLayoutManager)
            val dataAdapter = ShowMyMessageAdapter(this.activity!!,messageList)
            messagesRV?.setAdapter(dataAdapter)
        }
    }
}
