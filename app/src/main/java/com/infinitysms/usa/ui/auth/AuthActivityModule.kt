package com.infinitysms.usa.ui.auth

import com.infinitysms.usa.data.AppDataManager
import dagger.Module
import dagger.Provides

/**
 * Created byAndroid Tony on 23,March,2020
 */
@Module
class AuthActivityModule {

    @ActivityScoped
    @Provides
    fun provideAuthActivityViewModel(appDataManager: AppDataManager):
            AuthActivityVM = AuthActivityVM(appDataManager)

    annotation class ActivityScoped
}