package com.infinitysms.usa.ui.websocket

import android.util.Log
import com.infinitysms.usa.data.interfaces.WebSocketListener
import com.infinitysms.usa.utils.WebSocketConstants
import com.neovisionaries.ws.client.*
import org.json.JSONObject
import timber.log.Timber
import java.util.*

class WebSocketDataManager : com.neovisionaries.ws.client.WebSocketListener {

   private var socketListener: WebSocketListener


    companion object {
        var webSocket: WebSocket? = null
        private var webSocketDataManager: WebSocketDataManager? = null
        var phoneNumber: String=""
        var companyId: String=""

        fun getInstance(
            vehicle_id: String,
            companyid: String,
            webSocketListener: WebSocketListener
        ): WebSocketDataManager {
            return if (webSocketDataManager == null) {
                webSocketDataManager =
                    WebSocketDataManager(
                        vehicle_id,
                        companyid,
                        webSocketListener
                    )
                webSocketDataManager!!.initSocketConnection(vehicle_id)
                webSocketDataManager!!
            } else {
                webSocketDataManager!!
            }
        }

       public fun clearInstance() {
            webSocketDataManager = null
            System.gc()
        }

        fun getConnectedWebSocket(): WebSocket? {
            return if (webSocket != null && webSocket!!.socket != null && webSocket!!.socket.isConnected) {
                webSocket!!
            } else {
                null
            }
        }

        fun getConnectedInstance(): WebSocketDataManager? {
            return if (webSocketDataManager != null) {
                webSocketDataManager!!
            } else {
                null
            }
        }

       public fun clearWebSocketInstance() {
            webSocket = null
            System.gc()
        }
    }

    private constructor(
        phoneNumbers: String,
        companyid: String,
        webSocketListener: WebSocketListener
    ) {
        phoneNumber = phoneNumbers
        companyId = companyid
        socketListener = webSocketListener
    }

    private fun initSocketConnection(phoneNumber: String) {
        var authToken=""
        if (webSocket == null) {
            try {
                webSocket = WebSocketFactory().createSocket(
                    "wss://fm4fqrprs4.execute-api.eu-west-2.amazonaws.com/dev?auth_token=" +
                            "${authToken}&vehicle_id=${phoneNumber}", 60000
                )
                webSocket!!.addListener(this)
                webSocket!!.isAutoFlush = true
                webSocket!!.pingInterval = 1000
                webSocket!!.pingSenderName = "PING_SENDER"
                webSocket!!.pingPayloadGenerator =
                    PayloadGenerator { Date().toString().toByteArray() }
                webSocket!!.pongInterval = 1000
                webSocket!!.pongSenderName = "PONG_SENDER"
                webSocket!!.pongPayloadGenerator =
                    PayloadGenerator { Date().toString().toByteArray() }
                webSocket!!.connectAsynchronously()

            } catch (e: Exception) {
                e.printStackTrace()
                if (e.message == null || e.message == "") {
                    socketListener?.onMessageShow("")
                } else {
                    socketListener?.onMessageShow(e.message!!)
                }

            }
        } else {
            //  socketListener?.onMessageShow("Socket Already Connected!")
            Timber.d("Socket Already Connected!")
        }
    }


    override fun handleCallbackError(websocket: WebSocket?, cause: Throwable?) {
        socketListener?.onMessageShow(cause!!.message!!)

    }

    override fun onFrame(websocket: WebSocket?, frame: WebSocketFrame?) {

    }

    override fun onThreadCreated(websocket: WebSocket?, threadType: ThreadType?, thread: Thread?) {

    }

    override fun onThreadStarted(websocket: WebSocket?, threadType: ThreadType?, thread: Thread?) {

    }

    override fun onStateChanged(websocket: WebSocket?, newState: WebSocketState?) {
        when (newState) {
            WebSocketState.CLOSED -> {
//                initSocketConnection(authToken, phoneNumber)

                socketListener?.onDissconnectSuccessfully()
            }
            WebSocketState.CLOSING -> {

            }
            WebSocketState.OPEN -> {
                webSocket = websocket
                socketListener?.onConnectSuccessfully()
            }
            WebSocketState.CONNECTING -> {

            }
            else -> {

            }
        }
    }

    override fun onTextMessageError(
        websocket: WebSocket?,
        cause: WebSocketException?,
        data: ByteArray?
    ) {
        socketListener?.onMessageShow(cause!!.message!!)
    }

    override fun onTextFrame(websocket: WebSocket?, frame: WebSocketFrame?) {

    }

    override fun onUnexpectedError(websocket: WebSocket?, cause: WebSocketException?) {
        socketListener?.onMessageShow(cause!!.message!!)

    }

    override fun onConnectError(websocket: WebSocket?, cause: WebSocketException?) {
        socketListener?.onMessageShow(cause!!.message!!)
    }

    override fun onSendError(
        websocket: WebSocket?,
        cause: WebSocketException?,
        frame: WebSocketFrame?
    ) {
        socketListener?.onMessageShow(cause!!.message!!)

    }

    override fun onFrameUnsent(websocket: WebSocket?, frame: WebSocketFrame?) {

    }

    override fun onDisconnected(
        websocket: WebSocket,
        serverCloseFrame: WebSocketFrame?,
        clientCloseFrame: WebSocketFrame?,
        closedByServer: Boolean
    ) {
        when (clientCloseFrame?.closeCode) {
            WebSocketCloseCode.ABNORMAL -> {
                webSocket = null
                System.gc()
                initSocketConnection(phoneNumber)
            }
            WebSocketCloseCode.AWAY -> {
                webSocket = null
                System.gc()
                initSocketConnection( phoneNumber)
            }
            WebSocketCloseCode.INCONSISTENT -> {
            }
            WebSocketCloseCode.INSECURE -> {
            }
            WebSocketCloseCode.NONE -> {
            }
            WebSocketCloseCode.NORMAL -> {
                /*webSocket = null
                System.gc()
                initSocketConnection(authToken, phoneNumber)*/
            }
            WebSocketCloseCode.OVERSIZE -> {
                webSocket = null
                System.gc()
                initSocketConnection(phoneNumber)
            }
            WebSocketCloseCode.UNCONFORMED -> {
                webSocket = null
                System.gc()
                initSocketConnection(phoneNumber)
            }

        }

    }

    override fun onSendingFrame(websocket: WebSocket?, frame: WebSocketFrame?) {

    }

    override fun onBinaryFrame(websocket: WebSocket?, frame: WebSocketFrame?) {

    }

    override fun onPingFrame(websocket: WebSocket?, frame: WebSocketFrame?) {

    }

    override fun onSendingHandshake(
        websocket: WebSocket?,
        requestLine: String?,
        headers: MutableList<Array<String>>?
    ) {

    }
    fun updateVehicleLocation() {
        val obj = JSONObject()
        obj.put("action", "getMessagesSMSAPP")
        obj.put("company_id", companyId)
        obj.put("phone_number", phoneNumber)

        try {
            Log.v("WEBSOCKET","POST="+obj.toString())
            if (webSocket != null && webSocket!!.socket != null && webSocket!!.socket.isConnected) {
                webSocket!!.sendText(obj.toString())

            } else {

                socketListener?.onMessageShow("Please check your internet Connection!!")
            }
        } catch (e: Exception) {
            Log.d("error socket", e.message)
        }

    }

    override fun onTextMessage(websocket: WebSocket?, text: String) {
        Log.v("WEBSOCKET",text)
        var notificationType: String
        val jsonObject = JSONObject(text)

if(jsonObject.has("message")) {
    var message = jsonObject.get("message")!!.toString()
    if(message.equals("Internal server error")) {
        try {
            Timber.tag("error: ").d(message)
            socketListener?.onServerError(
                message
            )
        } catch (e: Exception) {
            e.printStackTrace()

        }
    }
}

else if(jsonObject.has("errorMessage")) {
            var message = jsonObject.get("errorMessage")!!.toString()
                try {
                    Timber.tag("error: ").d(message)
                    socketListener?.onServerError(
                        message
                    )
                } catch (e: Exception) {
                    e.printStackTrace()

                }
        }
        notificationType = jsonObject.get("notification_type")!!.toString()
        Log.d("AllData", jsonObject.toString())
        when (notificationType) {

           WebSocketConstants.Send_MESSAGE_FOR_SMS -> {
                try {
                    val status = jsonObject.getBoolean("status")
if(status)
                    socketListener?.onInboxMessage(
                        notificationType,
                        jsonObject
                    )

                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
            else -> {
                try {

                    Timber.tag("Notification Type: ").d(notificationType)
                    socketListener?.onInboxMessage(
                        notificationType,
                       jsonObject
                    )
                    //  socketListener?.onMessageShow("WebSocketConstants.$notificationType")

                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        }
    }

    override fun onTextMessage(websocket: WebSocket?, data: ByteArray?) {

    }



    override fun onFrameError(
        websocket: WebSocket?,
        cause: WebSocketException?,
        frame: WebSocketFrame?
    ) {
        socketListener?.onMessageShow(cause!!.message!!)
    }

    override fun onCloseFrame(websocket: WebSocket?, frame: WebSocketFrame?) {

    }

    override fun onBinaryMessage(websocket: WebSocket?, binary: ByteArray?) {

    }

    override fun onContinuationFrame(websocket: WebSocket?, frame: WebSocketFrame?) {

    }

    override fun onConnected(
        websocket: WebSocket?,
        headers: MutableMap<String, MutableList<String>>?
    ) {
        socketListener?.onConnectSuccessfully()
    }

    override fun onFrameSent(websocket: WebSocket?, frame: WebSocketFrame?) {

    }

    override fun onThreadStopping(websocket: WebSocket?, threadType: ThreadType?, thread: Thread?) {

    }

    override fun onError(websocket: WebSocket, cause: WebSocketException) {
        socketListener?.onMessageShow(cause.message!!)
    }

    override fun onMessageDecompressionError(
        websocket: WebSocket?,
        cause: WebSocketException?,
        compressed: ByteArray?
    ) {

    }

    override fun onPongFrame(websocket: WebSocket?, frame: WebSocketFrame?) {

    }

    override fun onMessageError(
        websocket: WebSocket?,
        cause: WebSocketException,
        frames: MutableList<WebSocketFrame>?
    ) {
        socketListener?.onMessageShow(cause.message!!)
    }

    fun closeConnection() {
        if (webSocket != null) {
            webSocket!!.disconnect("Manual")
        //    webSocket!!.connectedSocket.close()
        }

    }





}