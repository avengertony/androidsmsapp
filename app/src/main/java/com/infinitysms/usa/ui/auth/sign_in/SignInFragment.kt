package com.infinitysms.usa.ui.auth.sign_in


import android.Manifest
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.media.SoundPool
import android.os.Build
import android.os.Bundle
import android.os.CountDownTimer
import android.telephony.SubscriptionInfo
import android.telephony.SubscriptionManager
import android.telephony.TelephonyManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.infinitysms.usa.R
import com.infinitysms.usa.data.AppDataManager
import com.infinitysms.usa.di.module.AppViewModelFactory
import com.infinitysms.usa.ui.base.BaseAuthFragment
import com.infinitysms.usa.ui.dashboard.DashboardActivity
import com.infinitysms.usa.ui.websocket.WebSocketDataManager
import com.infinitysms.usa.utils.Constants
import com.infinitysms.usa.utils.toolbar.FragmentToolbar
import kotlinx.android.synthetic.main.dialog_alert.view.*
import kotlinx.android.synthetic.main.dialog_messsage_alert.view.*
import kotlinx.android.synthetic.main.fragment_sign_in.*
import javax.inject.Inject


class SignInFragment : BaseAuthFragment<SignInFragmentVM>(), View.OnClickListener {

    private val TAG = SignInFragment::class.java.simpleName

    @Inject
    lateinit var appViewModelFactory: AppViewModelFactory

    private lateinit var signInFragmentVM: SignInFragmentVM

    @Inject
    lateinit var appDataManager: AppDataManager
    var inboxDialog: androidx.appcompat.app.AlertDialog? = null
    var soundBeepId: Int = 0

    var soundPool: SoundPool? = null
    companion object {
        @JvmStatic
        fun newInstance() =
            SignInFragment().apply {
                arguments = Bundle().apply {

                }
            }
    }

    override fun builder(): FragmentToolbar? {
        return null
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.btnLogin -> {
                hideKeyboard()
                if (isNetworkConnected()) {
                    signInFragmentVM.doLogin(
                        etPhoneNum.text.toString().trim(),
                        etPinNo.text.toString().trim()
                    )
                }
            }

        }
    }

    override fun getViewModel(): SignInFragmentVM {
        signInFragmentVM = ViewModelProvider(this@SignInFragment, appViewModelFactory)
            .get(SignInFragmentVM::class.java)
        return signInFragmentVM
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {

        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        if (root == null) {
            root = inflater.inflate(R.layout.fragment_sign_in, container, false) as ViewGroup
        }

        return root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        if (!isFragmentLoaded) {
            initInstances()
        }

        super.onViewCreated(view, savedInstanceState)
    }

    private fun initInstances() {
        observeViewModel()
        btnLogin.setOnClickListener(this)
        if(appDataManager.getPhoneNumbers()?.isNotEmpty()!!)
        {
            etPhoneNum.setText(appDataManager.getPhoneNumbers())
        }
        if (ActivityCompat.checkSelfPermission(
                this.activity!!,
                Manifest.permission.READ_PHONE_STATE
            ) != PackageManager.PERMISSION_GRANTED
            &&
            ActivityCompat.checkSelfPermission(
                this.activity!!,
                Manifest.permission.SEND_SMS
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(
                this.activity!!,
                arrayOf(
                    Manifest.permission.READ_PHONE_STATE, Manifest.permission.SEND_SMS

                ),
                Constants.REQUEST_CODE.LOCATION_PREMISSION_REQUEST_CODE
            )
        }
    }

    private fun observeViewModel() {

        signInFragmentVM.uiState.observe(viewLifecycleOwner, Observer {
            if (it.loading) {
                showLoadingDialog()
            } else {
                dismissLoadingDialog()
            }

            if (it.success) {

                    if(signInFragmentVM.user.value != null) {
                        appDataManager.saveUserData(signInFragmentVM.user.value!!)
                        startInstanceWithBackStackCleared(activity)
                    }

            }

            if (it.message != null) {

            }

            if (it.error != null) {
                showErrorToast(it.error!!)
            }

            if (it.messageWithAlert != null) {

            }
        })
    }

    fun startInstanceWithBackStackCleared(context: Context?) {
        val intent = Intent(context, DashboardActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
        context?.startActivity(intent)
        (context as AppCompatActivity).overridePendingTransition(
            R.anim.activity_enter,
            R.anim.activity_leave
        )
        context.finishAffinity()
    }



}
