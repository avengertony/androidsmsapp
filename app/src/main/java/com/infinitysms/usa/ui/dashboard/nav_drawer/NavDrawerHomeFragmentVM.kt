package com.infinitysms.usa.ui.dashboard.nav_drawer

import android.content.Context
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.gson.JsonObject
import com.infinitysms.usa.R
import com.infinitysms.usa.data.AppDataManager
import com.infinitysms.usa.data.models.UIState
import com.infinitysms.usa.utils.Constants
import com.infinitysms.usa.utils.LogUtils
import default
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableObserver
import io.reactivex.schedulers.Schedulers
import retrofit2.Response
import javax.inject.Inject

/**
 * Created by Android Tony on 23,March,2020
 */
class NavDrawerHomeFragmentVM @Inject constructor(private var context: Context, var appDataManager: AppDataManager) :
    ViewModel() {


    private val TAG = NavDrawerHomeFragmentVM::class.java.simpleName
    var uiState = MutableLiveData<UIState>().default(UIState(loading = false))

    private var compositeDisposable = CompositeDisposable()

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.dispose()
    }
    fun freeDriver(companyId: String, phoneNumber: String) {
        try {
            uiState.value = UIState(loading = true,success = false,error = null,message = null)


            val jsonObject = JsonObject()
            jsonObject.addProperty("company_id", companyId)
            jsonObject.addProperty("vehicle_id", phoneNumber)


            val loginDisposable = appDataManager.freeDriver(jsonObject)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(object : DisposableObserver<Response<JsonObject>>() {
                    override fun onComplete() {
                        uiState.value = UIState(loading = false)
                    }

                    override fun onNext(response: Response<JsonObject>) {
                        try {
                            if (response.code() == 200 && response.body()!!.has(Constants.API.KEY.SUCCESS)
                                && response.body()!!.get(Constants.API.KEY.SUCCESS).asBoolean

                            ) {
                               var message = response.body()!!.get(Constants.API.KEY.MESSAGE).asString
                                    uiState.value = UIState(success = true,message =message )

                            } else if (response.body() != null) {
                                var message = context.resources.getString(R.string.something_went_wrong)
                                if (response.body()!!.has(Constants.API.KEY.MESSAGE)) {
                                    message = response.body()!!.get(Constants.API.KEY.MESSAGE).asString
                                } else if (response.body()!!.has(Constants.API.KEY.ERROR)) {
                                    message = response.body()!!.get(Constants.API.KEY.ERROR).asString
                                }

                                uiState.value = UIState(error = message)
                            } else {
                                uiState.value = UIState(error = context.getString(R.string.something_went_wrong))
                            }
                        } catch (e: Exception) {
                            LogUtils.e(TAG, e.message.toString())
                            uiState.value = UIState(loading = false, error = e.message)
                        }
                    }

                    override fun onError(e: Throwable) {
                        uiState.value = UIState(loading = false, error = e.message)
                    }
                })
            compositeDisposable.add(loginDisposable)
        } catch (e: Exception) {
            LogUtils.e(TAG, e.message.toString())
            uiState.value = UIState(loading = false, error = e.message)
        }
    }

}