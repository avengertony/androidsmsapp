package com.infinitysms.usa.ui.splash

import androidx.lifecycle.ViewModel
import com.infinitysms.usa.data.AppDataManager
import javax.inject.Inject

/**
 * Created byAndroid Tony on 23,March,2020
 */
class SplashActivityViewModel
@Inject constructor(appDataManager: AppDataManager) : ViewModel()