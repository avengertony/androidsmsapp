package com.infinitysms.usa.ui.dashboard.home

import android.content.Context
import android.util.Log
import android.view.View
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.gson.JsonObject
import com.infinitysms.usa.MyApplication
import com.infinitysms.usa.R
import com.infinitysms.usa.data.AppDataManager
import com.infinitysms.usa.data.models.UIState
import com.infinitysms.usa.utils.Constants
import com.infinitysms.usa.utils.LogUtils
import default
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableObserver
import io.reactivex.schedulers.Schedulers
import org.json.JSONObject
import retrofit2.Response
import javax.inject.Inject

/**
 * Created by Android Tony on 23,March,2020
 */
class HomeFragmentVM @Inject constructor(private var context: Context, var appDataManager: AppDataManager) :
    ViewModel() {


    private val TAG = HomeFragmentVM::class.java.simpleName

    var uiState = MutableLiveData<UIState>().default(UIState(loading = false))

    private var compositeDisposable = CompositeDisposable()

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.dispose()
    }

}