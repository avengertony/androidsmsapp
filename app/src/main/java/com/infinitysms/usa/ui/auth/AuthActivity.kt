package com.infinitysms.usa.ui.auth

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.lifecycle.ViewModelProvider
import com.infinitysms.usa.R
import com.infinitysms.usa.data.AppDataManager
import com.infinitysms.usa.di.module.AppViewModelFactory
import com.infinitysms.usa.ui.base.BaseAuthActivity
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasAndroidInjector
import javax.inject.Inject

class AuthActivity : BaseAuthActivity<AuthActivityVM>(), HasAndroidInjector {

    companion object {
        fun startInstanceWithBackStackCleared(context: Context?) {
            val intent = Intent(context, AuthActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
            context?.startActivity(intent)
            (context as AppCompatActivity).overridePendingTransition(R.anim.activity_enter, R.anim.activity_leave)
        }
    }

    @Inject
    lateinit var authActivityVM: AuthActivityVM

    @Inject
    lateinit var appViewModelFactory: AppViewModelFactory

    @Inject
    lateinit var appDataManager: AppDataManager

    @Inject
    lateinit var androidInjector : DispatchingAndroidInjector<Any>

    override fun androidInjector(): AndroidInjector<Any> {
        return androidInjector
    }

    override fun getViewModel(): AuthActivityVM {
        authActivityVM = ViewModelProvider(this@AuthActivity, appViewModelFactory)
            .get(AuthActivityVM::class.java)
        return authActivityVM
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_auth)
    }
}
