package com.infinitysms.usa.ui.dashboard.my_profile


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.gson.Gson
import com.infinitysms.usa.BuildConfig
import com.infinitysms.usa.R
import com.infinitysms.usa.data.AppDataManager
import com.infinitysms.usa.data.models.EmployeeDetail
import com.infinitysms.usa.data.models.ProfileBean
import com.infinitysms.usa.di.module.AppViewModelFactory
import com.infinitysms.usa.ui.base.BaseFragment
import com.infinitysms.usa.ui.dashboard.my_profile.RecyclerItemDecoration.SectionCallback
import com.infinitysms.usa.utils.Constants
import com.infinitysms.usa.utils.toolbar.FragmentToolbar
import com.infinitysms.usa.utils.toolbar.IBackButtonCallback
import kotlinx.android.synthetic.main.fragment_myprofile.*
import kotlinx.android.synthetic.main.layout_progress_and_error.*
import loadUrl
import makeGone
import makeVisible
import javax.inject.Inject


class MyProfileFragment : BaseFragment<MyProfileFragmentVM>(),
    View.OnClickListener{

    private var profileData: ProfileBean?=null
    private val TAG = "MyProfileFragment"

    companion object {
        @JvmStatic
        fun newInstance() =
            MyProfileFragment().apply {
                arguments = Bundle().apply {

                }
            }
    }

    @Inject
    lateinit var appViewModelFactory: AppViewModelFactory
    private lateinit var viewModel: MyProfileFragmentVM
    @Inject
    lateinit var appDataManager: AppDataManager
    private var employeeDetail: EmployeeDetail? = null

    override fun builder(): FragmentToolbar? {
        return FragmentToolbar.Builder()
            .withId(R.id.toolbar)
            .withTitle(getString(R.string.profile))
            .withBackground()
            .withBackButton()
            .withBackButtonCallback(object : IBackButtonCallback {
                override fun onToolbarBackPressed(view: View) {
                    Navigation.findNavController(this@MyProfileFragment.view!!)
                        .navigateUp()
                }
            })
            .build()
    }

    override fun getViewModel(): MyProfileFragmentVM {
        viewModel =
            ViewModelProvider(this@MyProfileFragment, appViewModelFactory)
                .get(MyProfileFragmentVM::class.java)
        return viewModel
    }

    override fun onClick(v: View?) {
        when(v?.id) {

        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            employeeDetail = arguments?.getParcelable(Constants.KEY.EMPLOYEE)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        if (root == null) {
            root =
                inflater.inflate(
                    R.layout.fragment_myprofile,
                    container,
                    false
                ) as ViewGroup
        }

        return root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        if (!isFragmentLoaded) {
            initInstances()
        }

        super.onViewCreated(view, savedInstanceState)
    }

    override fun onResume() {
        hideBottomNavigation()
        lockedNavDrawer()

        super.onResume()
    }

    private fun initInstances() {

        observeViewModel()

        if (isNetworkConnected()) {
            viewModel.getUserDetail()
        }
    }

    private fun observeViewModel() {
        viewModel.uiState.observe(viewLifecycleOwner, Observer {
            if (it.loading) {
                clMain.makeGone()
                clBottom.makeGone()
                loadingLayout.makeVisible()
            } else {
                clMain.makeVisible()
                clBottom.makeVisible()
                loadingLayout.makeGone()
            }

            /*if (it.success) {

            }*/

            if (it.message != null) {
                showInfoToast(it.message!!)
            }

            if (it.error != null) {
                showErrorToast(it.error!!)
            }

            if (it.messageWithAlert != null) {
                showDefaultAlertDialog(getString(R.string.message), it.messageWithAlert!!)
            }
        })

        viewModel.profileBean.observe(viewLifecycleOwner, Observer {
            if(it!=null)
            {
                profileData=it
                civProfilePic.loadUrl(activity!!, BuildConfig.IMG_BASE_URL+profileData?.driver_details?.user_image!!)
                ratingProfile.progress = profileData?.driver_rating!!.toInt()
var user=appDataManager?.getUserData()
                user?.phone_number= profileData?.driver_details?.mobileNumber!!
                appDataManager.saveUserData(user!!)
                getData()
                setupRecyclerview()
            }
        })
    }
 var linearLayoutManager: LinearLayoutManager? = null
    var dataList: ArrayList<HashMap<String, String>> = ArrayList()
fun setupRecyclerview()
{

    linearLayoutManager = LinearLayoutManager(this.activity!!)
    recyclerMain.setLayoutManager(linearLayoutManager)
    val dataAdapter = DataAdapter(this.activity!!, dataList)
    recyclerMain.setAdapter(dataAdapter)

    val recyclerItemDecoration = RecyclerItemDecoration(
        this.activity!!,
        resources.getDimensionPixelSize(R.dimen.height_button),
        true,
        getSectionCallback(dataList)!!
    )
    recyclerMain.addItemDecoration(recyclerItemDecoration)

}
    private fun getSectionCallback(list: ArrayList<HashMap<String, String>>): SectionCallback? {
        return object : SectionCallback {
            override fun isSection(pos: Int): Boolean {
                return pos == 0 || list[pos]["Title"] !== list[pos - 1]["Title"]
            }

            override fun getSectionHeaderName(pos: Int): String {
                val dataMap = list[pos]
                return dataMap["Title"]!!
            }
        }
    }

    private fun getData() {
        var user=appDataManager.getUserData()!!
        val dataMAp1: HashMap<String, String> = HashMap()
        dataMAp1["Title"] = "Driver Details"
      //  dataMAp1["Desc1"] = "Driver ID"
        dataMAp1["Desc1"] = profileData?.driver_details?.driver_id.toString()
      //  dataMAp1["Desc3"] = "Full Name"
        dataMAp1["Desc2"] = profileData?.driver_details?.firstname+" "+profileData?.driver_details?.lastname
       // dataMAp1["Desc5"] = "Company"
      //  dataMAp1["Desc3"] =  user.company_name.toString()
      //  dataMAp1["Desc7"] =  "Phone"
        dataMAp1["Desc4"] =  profileData?.driver_details?.mobileNumber!!
      //  dataMAp1["Desc9"] =  "Email"
        dataMAp1["Desc5"] =  profileData?.driver_details?.email!!

        dataList.add(dataMAp1)
        val dataMAp2: HashMap<String, String> = HashMap()
        dataMAp2["Title"] = "Vehicles"
        dataMAp2["Desc1"] = profileData?.vehicle_details?.vehicle_random_id.toString()
        dataMAp2["Desc2"] = profileData?.vehicle_details?.vehicle_number.toString()
        dataMAp2["Desc3"] = "Not Available"
        dataMAp2["Desc4"] = "Not Available"
        dataMAp2["Desc5"] = profileData?.vehicle_details?.colour!!
        dataList.add(dataMAp2)


        val dataMAp3: HashMap<String, String> = HashMap()
        dataMAp3["Title"] = "Documents"
        var listdata=profileData?.driver_documents!!
        for(i in listdata.indices)
        dataMAp3["Desc"+(i+1)] = Gson().toJson(profileData?.driver_documents!![i])
        dataList.add(dataMAp3)

    }
}


