package com.infinitysms.usa.ui.services

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.widget.Toast
import com.infinitysms.usa.ui.dashboard.DashboardActivity

class MyBroadcastReceiver : BroadcastReceiver() {

        override fun onReceive(context: Context, intent: Intent) {
           // var integerTime = intent.getIntExtra("TimeRemaining", 0)
         //   Toast.makeText(context,integerTime.toString(),Toast.LENGTH_SHORT).show()
            if(!DashboardActivity.isLocationSendStop && DashboardActivity.webSocketDataManager!=null)
                DashboardActivity.webSocketDataManager?.updateVehicleLocation()

        }
    }