package pegasus.driver.mvvm.dashboard.myjobs

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.infinitysms.usa.R
import com.infinitysms.usa.data.models.MyJobData
import kotlinx.android.synthetic.main.item_my_jobs.view.*
import java.text.SimpleDateFormat
import java.util.*


class ShowMyJobsAdapter(
    val context: Context,
    var itemList: ArrayList<MyJobData>
) : RecyclerView.Adapter<ShowMyJobsAdapter.OrderViewHolder>() {

    /* lateinit var onSellerOrderClickListener: OnOrderClickListener

     fun setOnOrderClickListener(onOrderClickListener: OnOrderClickListener) {
         this.onSellerOrderClickListener = onOrderClickListener
     }
 */
    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ShowMyJobsAdapter.OrderViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.item_my_jobs, p0, false)
        return ShowMyJobsAdapter.OrderViewHolder(view)
    }

    override fun getItemCount() = itemList.size

    override fun onBindViewHolder(p0: ShowMyJobsAdapter.OrderViewHolder, p1: Int) {

        val formatter = SimpleDateFormat("dd MMM yyyy, hh:mm a")

        val calendar = Calendar.getInstance()
        calendar.timeInMillis = (itemList[p1]._source.booking_time * 1000L)


        p0.tvStartAddress.text = (itemList[p1]._source.source_address)
        p0.tvEndAddress.text = (itemList[p1]._source.destination_address)
        p0.tvStatus.text = itemList[p1]._source.status
        p0.tvAmount.text = "£ " + String.format("%.02f", itemList[p1]._source.driver_income)
        p0.labelAmount.text = itemList[p1]._source.pay_type
        p0.tvTime.text = formatter.format(calendar.time)
        if (itemList[p1]._source.is_bid == 1)
            p0.tvBidStatus.text = "Bid Job"
        else
            p0.tvBidStatus.text = "Normal Job"

        /*p0.clJobDetails.setOnClickListener {
            onSellerOrderClickListener.onOrderClicked(itemList[p1])
        }*/
    }

    class OrderViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val clJobDetails = view.clJobDetails!!
        val tvStartAddress = view.tvStartAddress!!
        val tvEndAddress = view.tvEndAddress!!
        val tvStatus = view.tvStatus!!
        val tvAmount = view.tvAmount!!
        val labelAmount = view.labelAmount!!
        val tvTime = view.tvTime!!
        val tvBidStatus = view.tvBidStatus!!
    }

    interface OnOrderClickListener {
        fun onOrderClicked(orderItemModel: MyJobData)
    }
}