package pegasus.driver.mvvm.dashboard.myjobs

import android.content.Context
import android.os.Build
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.infinitysms.usa.R
import com.infinitysms.usa.data.models.MessageData
import com.infinitysms.usa.data.models.MyJobData
import kotlinx.android.synthetic.main.item_message.view.*
import kotlinx.android.synthetic.main.item_my_jobs.view.*
import java.text.SimpleDateFormat
import java.util.*


class ShowMyMessageAdapter(
    val context: Context,
    var itemList: ArrayList<MessageData>
) : RecyclerView.Adapter<ShowMyMessageAdapter.OrderViewHolder>() {


    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): OrderViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.item_message, p0, false)
        return OrderViewHolder(view)
    }

    override fun getItemCount() = itemList.size

    override fun onBindViewHolder(p0: ShowMyMessageAdapter.OrderViewHolder, p1: Int) {
         p0.tvStatus.text = itemList[p1].status
        if(itemList[p1].code==0)
        {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                p0.tvStatus.setTextColor(context.getColor(R.color.red))
            }
        }
        else{
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                p0.tvStatus.setTextColor(context.getColor(R.color.green_btn))
            }
        }
        if(itemList[p1].message!=null)
        p0.tvMessage.text = itemList[p1].message +" to "+ itemList[p1].phone_number

    }

    class OrderViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        val tvStatus = view.tvStatusMessage!!
        val tvMessage = view.tvMessage!!

    }


}