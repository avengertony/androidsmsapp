package com.infinitysms.usa

import android.content.Context
import androidx.multidex.MultiDex
import androidx.multidex.MultiDexApplication
import com.infinitysms.usa.data.models.User
import com.infinitysms.usa.di.component.DaggerAppComponent
import com.infinitysms.usa.utils.LocaleHelper
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasAndroidInjector
import timber.log.Timber
import javax.inject.Inject

/**
 * Created byAndroid Tony on 23,March,2020
 */
class MyApplication : MultiDexApplication(), HasAndroidInjector {

    companion object {
        lateinit var context: Context
    }

    @Inject
    lateinit var androidInjector: DispatchingAndroidInjector<Any>

    override fun androidInjector(): AndroidInjector<Any> {
        return androidInjector
    }

   /* override fun applicationInjector(): AndroidInjector<out DaggerApplication> {
        val appComponent = DaggerAppComponent.builder().application(this).build()
        appComponent.inject(this)
        Mapbox.getInstance(this, getString(R.string.map_box_key))
        return appComponent
    }
*/
    override fun onCreate() {
        super.onCreate()

        context = this
        DaggerAppComponent.builder().application(this).build().inject(this)


        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }

    }

    override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(base?.let { LocaleHelper.onAttach(it) })
        MultiDex.install(this)

    }

    private var user: User? = null

    fun getUser(): User? {
        return user
    }

    fun setUser(user: User?) {
        this.user = user
    }
}