package com.infinitysms.usa.utils

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.app.ActivityManager
import android.content.BroadcastReceiver
import android.content.Context.ACTIVITY_SERVICE
import android.content.Intent
import android.content.IntentFilter
import android.content.pm.PackageManager
import android.location.Location
import android.os.Build
import android.util.Log
import androidx.annotation.NonNull
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.fragment.app.FragmentActivity
import com.infinitysms.usa.R
import com.infinitysms.usa.ui.dashboard.DashboardActivity
import com.infinitysms.usa.ui.services.MyBroadcastReceiver
import com.infinitysms.usa.ui.services.MyTimerService
import com.infinitysms.usa.utils.Constants.REQUEST_CODE.Companion.LOCATION_PREMISSION_REQUEST_CODE

/**
 * Created by Android Tony on 14,February,2020
 */
class LocationUtils private constructor(var context: AppCompatActivity) {

    private val TAG = LocationUtils::class.java.simpleName
    var br: BroadcastReceiver?=null
    companion object {
        private var instance: LocationUtils? = null
        fun getInstance(context: AppCompatActivity): LocationUtils {
            if (instance == null)
                instance = LocationUtils(context)

            return instance as LocationUtils
        }
    }


    private var isContinuesLocation = false
    private var locationUtilsListener: LocationUtilsListener? = null


    // method for turn on GPS
    fun getLocation(isContinuesLocation: Boolean, locationUtilsListener: LocationUtilsListener) {

        this.isContinuesLocation = isContinuesLocation
        this.locationUtilsListener = locationUtilsListener

        requestLocation()
    }

    fun getLastKnownLocation() {


        if (ActivityCompat.checkSelfPermission(
                this.context,
                Manifest.permission.FOREGROUND_SERVICE
            ) !== PackageManager.PERMISSION_GRANTED
        ) {
            return
        }
        startLocationService()

    }

    fun requestLocation() {
        if (checkLocationPermission()) {
            if (!isContinuesLocation) {

            } else {
                getLastKnownLocation()

            }
        }
    }

    fun checkLocationPermission(): Boolean {
        var isPermissionGranted = true
        if (ActivityCompat.checkSelfPermission(
                context,
                Manifest.permission.FOREGROUND_SERVICE
            ) !== PackageManager.PERMISSION_GRANTED ||
            ActivityCompat.checkSelfPermission(
                context,
                Manifest.permission.SEND_SMS
            ) !== PackageManager.PERMISSION_GRANTED

            || ActivityCompat.checkSelfPermission(
                context,
                Manifest.permission.READ_PHONE_STATE
            ) !== PackageManager.PERMISSION_GRANTED

        ) {
            ActivityCompat.requestPermissions(
                context,
                arrayOf(
                    Manifest.permission.SEND_SMS,
                    Manifest.permission.READ_PHONE_STATE,
                    Manifest.permission.FOREGROUND_SERVICE
                ),
                LOCATION_PREMISSION_REQUEST_CODE
            )
            isPermissionGranted = false
        }
        return isPermissionGranted
    }

    fun onRequestPermissionsResult(
        requestCode: Int,
        @NonNull permissions: Array<String>,
        @NonNull grantResults: IntArray
    ) {
        when (requestCode) {
            LOCATION_PREMISSION_REQUEST_CODE -> {
                // If request is cancelled, the result arrays are empty.
                if ((grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    requestLocation()
                    // getLastKnownLocation()
                } else {
                }
            }
        }
    }


    fun startLocationService() {
        if (!isLocationServiceRunning()) {
            if (!DashboardActivity.dontStartService) {
                try {

                    var intentFilter = IntentFilter()
                    intentFilter.addAction("Counter")
                    br = MyBroadcastReceiver()

                    context.applicationContext.registerReceiver(br, intentFilter)




                    val serviceIntent =
                        Intent(context.applicationContext, MyTimerService::class.java)
                    serviceIntent.putExtra("STATUS","START")
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                        context.applicationContext.startForegroundService(serviceIntent)
                    } else {
                        context.startService(serviceIntent)
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        }
    }
    fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == Constants.REQUEST_CODE.GPS_SETTING_REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                //isGPS = true // flag maintain before get location
                //requestLocation()
                getLastKnownLocation()
            } else {
                locationUtilsListener?.onFailure(context.resources.getString(R.string.location_is_not_enabled))
            }
        }

    }
    fun stopLocationService() {
        if (isLocationServiceRunning()) {
            if(br!=null)
            {

                context.applicationContext.unregisterReceiver(br)
                if((context as DashboardActivity).sentStatusReceiver!=null) {
                    context.unregisterReceiver((context as DashboardActivity).sentStatusReceiver)
                    context.unregisterReceiver((context as DashboardActivity).deliveredStatusReceiver)
                }

            }
            val serviceIntent =
                Intent(context.applicationContext, MyTimerService::class.java)
            context.stopService(serviceIntent)

        }
    }

    interface LocationUtilsListener {
        fun onSuccess(location: Location)
        fun onFailure(message: String)
    }

    private fun isLocationServiceRunning(): Boolean {
        val manager =
            context.getSystemService(ACTIVITY_SERVICE) as ActivityManager
        for (service in manager.getRunningServices(
            Int.MAX_VALUE
        )) {
            Log.d(
                TAG,
                "MyTimerService: MyTimerService service is already running." + service.service.className
            )
            if ("com.infinitysms.usa.ui.services.MyTimerService" == service.service.className) {
                DashboardActivity.goingTostartService = false
                Log.d(
                    TAG,
                    "MyTimerService: MyTimerService service is already running."
                )
                return true
            }
        }
        Log.d(
            TAG,
            "MyTimerService: MyTimerService service is not running."
        )
        return false
    }

}