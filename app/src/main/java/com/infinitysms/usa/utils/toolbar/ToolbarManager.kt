package com.infinitysms.usa.utils.toolbar

import android.content.Context
import android.view.View
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import com.infinitysms.usa.R
import kotlinx.android.synthetic.main.layout_toolbar.view.*
import makeGone
import makeVisible

/**
 * Created byAndroid Tony on 23,March,2020
 */
class ToolbarManager constructor(
    private var builder: FragmentToolbar,
    private var container: View
) {


    /***
     * Choose FragmentToolbar.NO_TOOLBAR as ID, if no toolbar required.
     * */
    fun prepareToolbar(context: Context) {
        if (builder.resId != FragmentToolbar.NO_TOOLBAR) {

            val fragmentToolbar = container.findViewById(builder.resId) as ConstraintLayout

            if (builder.title != "") {
                fragmentToolbar.tvTitle.text = builder.title.toString()
                fragmentToolbar.tvTitle.makeVisible()
            } else {
                fragmentToolbar.tvTitle.makeGone()
            }

            if (builder.isBackButton) {
                if (builder.isWithBackground) {
                    fragmentToolbar.ivBack.makeVisible()
                    fragmentToolbar.ivBack.setImageResource(R.mipmap.back_white)
                } else {
                    fragmentToolbar.ivBack.makeVisible()
                    fragmentToolbar.ivBack.setImageResource(R.mipmap.back)
                }

                if (builder.isFromNavigationDrawer) {
                    fragmentToolbar.mcvMain.setCardBackgroundColor(
                        ContextCompat.getColor(
                            context,
                            R.color.colorPrimaryDark
                        )
                    )
                }


                fragmentToolbar.ivBack.setOnClickListener {
                    builder.backButtonListener?.onToolbarBackPressed(it)
                }
            } else {
                fragmentToolbar.ivBack.makeGone()
            }

            if (builder.isHamburger) {
                if (builder.isWithBackground) {
                    fragmentToolbar.ivHamburger.makeVisible()
                } else {
                    fragmentToolbar.ivHamburger.makeVisible()
                }
                fragmentToolbar.ivHamburger.setOnClickListener {
                    builder.hamburgerListener?.onToolbarHamburgerClicked(it)
                }
            } else {
                fragmentToolbar.ivHamburger.makeGone()
            }

            if (builder.isMenuVisible) {
                fragmentToolbar.ivMenu.makeVisible()
                fragmentToolbar.ivMenu.setOnClickListener {
                    builder.menuListener?.onMenuClicked(it)
                }
            } else {
                fragmentToolbar.ivMenu.makeGone()
            }

            if (builder.isNotification) {
                fragmentToolbar.ivNotification.makeVisible()
                fragmentToolbar.ivNotification.setOnClickListener {
                    builder.notificationListener?.onToolbarNotificationClicked(it)
                }
            } else {
                fragmentToolbar.ivNotification.makeGone()
            }

            if (builder.isEvent) {
                fragmentToolbar.ivEvent.makeVisible()
                fragmentToolbar.ivEvent.setOnClickListener {
                    builder.eventListener?.onToolbarEventClicked(it)
                }
            } else {
                fragmentToolbar.ivEvent.makeGone()
            }

            if (builder.isEditButton) {
                fragmentToolbar.ivEdit.makeVisible()
            } else {
                fragmentToolbar.ivEdit.makeGone()
            }

            if (builder.isDateVisible) {
                fragmentToolbar.tvDate.text = builder.date
                fragmentToolbar.tvDay.text = builder.day
                fragmentToolbar.tvDate.makeVisible()
                fragmentToolbar.tvDay.makeVisible()
            } else {
                fragmentToolbar.tvDate.makeGone()
                fragmentToolbar.tvDay.makeGone()
            }
        }
    }

}