package com.infinitysms.usa.utils

import android.animation.ValueAnimator
import android.app.ActivityManager
import android.app.AlertDialog
import android.app.Dialog
import android.content.Context
import android.content.pm.PackageInfo
import android.content.pm.PackageManager
import android.content.res.Resources
import android.graphics.Color
import android.graphics.Point
import android.graphics.drawable.ColorDrawable
import android.location.Address
import android.location.Geocoder
import android.location.Location
import android.os.Build
import android.os.Environment
import android.provider.Settings
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.NavOptions
import com.infinitysms.usa.R
import com.infinitysms.usa.utils.widgets.ColorToast
import java.io.File
import java.io.IOException
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit

/**
 * Created byAndroid Tony on 23,March,2020
 */
class Helper(activity: AppCompatActivity) {

    private var screenWidth = 0
    private var screenHeight = 0

    private var activity: AppCompatActivity? = null
    private var loadingDialog: Dialog? = null

    init {
        this.activity = activity
    }

    companion object {
        fun getFormattedDateFromDate(
            strdate: String,
            oldstrFormat: String,
            strFormat: String,
            blnIsAMPM: Boolean
        ): String {

            val date = SimpleDateFormat(oldstrFormat).parse(strdate)
            var strFormattedDate = SimpleDateFormat(strFormat).format(date)

            if (blnIsAMPM) {
                val calendar = Calendar.getInstance()
                calendar.time = date
                val intTimeAMPM = calendar.get(Calendar.AM_PM)

                if (intTimeAMPM == 0) {
                    strFormattedDate += " AM"
                } else if (intTimeAMPM == 1) {
                    strFormattedDate += " PM"
                }
            }
            return strFormattedDate
        }

        fun getFormattedDate(strDate: String): String {
            var result = ""
            try {
                val fromFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
                val date = fromFormat.parse(strDate)

                val toFormatWithDate = SimpleDateFormat("dd-MM-yyyy, HH:mm a")
                val toFormatWithoutDate = SimpleDateFormat("HH:mm a")

                val calDate = Calendar.getInstance()
                calDate.timeInMillis = date?.time!!

                val calNow = Calendar.getInstance()

                if (calNow.get(Calendar.DATE) == calDate.get(Calendar.DATE)) {
                    result = "Today, " + toFormatWithoutDate.format(date)
                } else if (calNow.get(Calendar.DATE) - calDate.get(Calendar.DATE) == 1) {
                    result = "Yesterday, " + toFormatWithoutDate.format(date)
                } else {
                    result = toFormatWithDate.format(date)
                }
            } catch (ex: Exception) {

            }
            return result
        }

        fun getFormattedDateWithddMMMM(strDate: String): String {
            var result = ""
            try {
                val fromFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
                val date = fromFormat.parse(strDate)

                val toFormatWithDate = SimpleDateFormat("MMMM dd")
                val toFormatWithoutDate = SimpleDateFormat("HH:mm a")

                val calDate = Calendar.getInstance()
                calDate.timeInMillis = date?.time!!

                val calNow = Calendar.getInstance()

                if (calNow.get(Calendar.DATE) == calDate.get(Calendar.DATE)) {
                    //result =  "Today, " + toFormatWithoutDate.format(date)
                    result = "Today"
                } else if (calNow.get(Calendar.DATE) - calDate.get(Calendar.DATE) == 1) {
                    //result =  "Yesterday, " + toFormatWithoutDate.format(date)
                    result = "Yesterday"
                } else {
                    result = toFormatWithDate.format(date)
                }
            } catch (ex: Exception) {

            }
            return result
        }

        fun getDaysLeft(endDate: String?): String? {
            val myFormat = SimpleDateFormat("yyyy-MM-dd")
            val inputString1: String? = endDate
            var days: String? = null
            val currDate: Date = Calendar.getInstance().time

            try {
                val date2: Date = myFormat.parse(inputString1)
                val diff: Long = date2.time - currDate.time
                days = TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS).toString()
            } catch (e: ParseException) {
                e.printStackTrace()
            }
            return days
        }
    }

    fun getCurrentDate() : String {
        val c = Calendar.getInstance().getTime()
        val sdf = SimpleDateFormat("dd-MM-yyyy")
        return sdf.format(c)
    }

    fun getCurrentDayName() : String {
        val sdf = SimpleDateFormat("EEEE")
        val d: Date = Date()
        return sdf.format(d)
    }

    fun getAnimNavOption() : NavOptions {
        val navBuilder = NavOptions.Builder()
        return navBuilder
            .setEnterAnim(R.anim.trans_left_in)
            .setExitAnim(R.anim.trans_left_out)
            .setPopEnterAnim(R.anim.trans_right_in)
            .setPopExitAnim(R.anim.trans_right_out)
            .build()
    }
   fun getSlideUpAnimNavOption() : NavOptions {
        val navBuilder = NavOptions.Builder()
        return navBuilder
            .setEnterAnim(R.anim.trans_top_in)
            .setExitAnim(R.anim.trans_top_out)
            .setPopEnterAnim(R.anim.trans_bottom_in)
            .setPopExitAnim(R.anim.trans_bottom_out)
            .build()
    }

    fun getScreenHeight(c: Context): Int {
        if (screenHeight == 0) {
            val wm = c.getSystemService(Context.WINDOW_SERVICE) as WindowManager
            val display = wm.defaultDisplay
            val size = Point()
            display.getSize(size)
            screenHeight = size.y
        }

        return screenHeight
    }

    fun getScreenWidth(c: Context?): Int {
        if (screenWidth == 0) {
            val wm = c?.getSystemService(Context.WINDOW_SERVICE) as WindowManager
            val display = wm.defaultDisplay
            val size = Point()
            display.getSize(size)
            screenWidth = size.x
        }

        return screenWidth
    }

    fun isAndroid5(): Boolean {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP
    }

    fun isAndroid6(): Boolean {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.M
    }

    fun showAlertDialog(title: String, message: String) {
        AlertDialog.Builder(activity).setTitle(title).setMessage(message).setPositiveButton(
            "OK"
        ) { dialog, id -> dialog.dismiss() }.create().show()
    }

    fun showAlertDialogOnUiThread(title: String, message: String) {
        if (activity != null) {
            activity!!.runOnUiThread {
                AlertDialog.Builder(activity).setTitle(title).setMessage(message).setPositiveButton(
                    "OK"
                ) { dialog, id -> dialog.dismiss() }.create().show()
            }
        }
    }

    fun showErrorFinishAppDialogOnGuiThread(errorMessage: String) {
        if (activity != null) {
            activity!!.runOnUiThread {
                AlertDialog.Builder(activity).setMessage(errorMessage).setTitle("message")
                    .setPositiveButton("OK") { dialog, id ->
                        dialog.dismiss()
                        System.runFinalizersOnExit(true)
                        System.exit(0)
                    }.create().show()
            }
        }
    }

    fun showErrorFinishAppDialog(errorMessage: String) {
        if (activity != null) {
            AlertDialog.Builder(activity).setMessage(errorMessage).setTitle("message")
                .setPositiveButton("OK") { dialog, id ->
                    dialog.dismiss()
                    System.runFinalizersOnExit(true)
                    System.exit(0)
                }.create().show()
        }
    }

    fun showErrorToast(message: String) {

        if (message != null){
            activity?.runOnUiThread {
                ColorToast.makeText(
                    activity,
                    message,
                    ColorToast.LENGTH_SHORT,
                    ColorToast.ERROR,
                    false
                ).show()
            }

        }

    }

    fun showInfoToast(message: String?) {
        if (message != null){
            activity?.runOnUiThread {
                ColorToast.makeText(
                    activity,
                    message,
                    ColorToast.LENGTH_SHORT,
                    ColorToast.INFO,
                    false
                ).show()
            }

        }

    }

    fun showWarningToast(message: String?) {
        if (message != null) {
            activity?.runOnUiThread {
                ColorToast.makeText(
                    activity,
                    message,
                    ColorToast.LENGTH_SHORT,
                    ColorToast.WARNING,
                    false
                ).show()
            }

        }
    }

    fun showSuccessToast(message: String?) {
        if (message != null){
            activity?.runOnUiThread {
                ColorToast.makeText(
                    activity,
                    message,
                    ColorToast.LENGTH_SHORT,
                    ColorToast.SUCCESS,
                    false
                ).show()
            }

        }

    }

    fun showShortToast(msg: String) {
        if (activity != null) {
            activity!!.runOnUiThread { Toast.makeText(activity, msg, Toast.LENGTH_SHORT).show() }
        }
    }

    fun showLongToast(msg: String) {
        if (activity != null) {
            activity!!.runOnUiThread { Toast.makeText(activity, msg, Toast.LENGTH_LONG).show() }
        }
    }

    /*fun showCustomAlertDialog(title: String, message: String) {
        val dialog = Dialog(activity as AppCompatActivity)
        dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(true)
        dialog.setContentView(R.layout.common_alert_dialog)

        val tvTitle = dialog.findViewById(R.id.tvTitle) as TextView
        tvTitle.text = title

        val tvMessage = dialog.findViewById(R.id.tvMessage) as TextView
        tvMessage.text = message

        val mbOK = dialog.findViewById(R.id.mbOK) as MaterialButton
        mbOK.setOnClickListener {
            dialog.cancel()
        }
        dialog.window?.attributes?.windowAnimations = R.style.DialogGrowAnimation
        dialog.show()
    }*/

    fun showLoadingDialog() {
        if (loadingDialog == null) {
            loadingDialog = Dialog(activity as AppCompatActivity)
            loadingDialog?.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            loadingDialog?.requestWindowFeature(Window.FEATURE_NO_TITLE)
            loadingDialog?.setCancelable(false)
            loadingDialog?.setContentView(R.layout.custom_loading_dialog)
            loadingDialog?.window?.attributes?.windowAnimations = R.style.DialogGrowAnimation
        }
if(!loadingDialog?.isShowing!!)
        loadingDialog?.show()
    }

    fun dismissLoadingDialog() {
        if (loadingDialog != null && loadingDialog?.isShowing!! ) {
            loadingDialog?.cancel()
        }
    }

    fun hideKeyboard() {
        val inputMethodManager =
            activity!!.getSystemService(AppCompatActivity.INPUT_METHOD_SERVICE) as InputMethodManager
        //Find the currently focused view, so we can grab the correct window token from it.
        var view = activity!!.currentFocus
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = View(activity)
        }
        inputMethodManager.hideSoftInputFromWindow(view.windowToken, 0)
    }

    fun hideWindowKeyboard() {
        activity?.getWindow()?.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN)
    }

    fun showKeyboard() {
        val inputMethodManager =
            activity!!.getSystemService(AppCompatActivity.INPUT_METHOD_SERVICE) as InputMethodManager
        //Find the currently focused view, so we can grab the correct window token from it.
        var view = activity!!.currentFocus
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = View(activity)
        }
        //inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);

        inputMethodManager.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT)
    }

    fun isAppIsInBackground(context: Context): Boolean {
        var isInBackground = true
        val am = context.getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT_WATCH) {
            val runningProcesses = am.runningAppProcesses
            for (processInfo in runningProcesses) {
                if (processInfo.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND) {
                    for (activeProcess in processInfo.pkgList) {
                        if (activeProcess == context.packageName) {
                            isInBackground = false
                        }
                    }
                }
            }
        } else {
            val taskInfo = am.getRunningTasks(1)
            val componentInfo = taskInfo[0].topActivity
            if (componentInfo?.packageName == context.packageName) {
                isInBackground = false
            }
        }

        return isInBackground
    }

    fun dpToPx(dp: Float): Float {
        return (dp * Resources.getSystem().getDisplayMetrics().density)
    }

    fun pxToDp(px: Float): Float {
        return (px / Resources.getSystem().getDisplayMetrics().density)
    }

    /*fun dpToPx(dp: Int): Int {

        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_PX, dp.toFloat(), activityS!!.resources.displayMetrics).toInt()
    }*/

    fun getAppVersionName(): String {
        val pm = activity!!.packageManager
        var pInfo: PackageInfo? = null

        try {
            pInfo = pm.getPackageInfo(activity!!.packageName, 0)
        } catch (e1: PackageManager.NameNotFoundException) {
            e1.printStackTrace()
        }

        return pInfo!!.versionName
    }

    fun getAppVersionCode(): Int {
        val pm = activity!!.packageManager
        var pInfo: PackageInfo? = null

        try {
            pInfo = pm.getPackageInfo(activity!!.packageName, 0)
        } catch (e1: PackageManager.NameNotFoundException) {
            e1.printStackTrace()
        }

        return pInfo!!.versionCode
    }

    /**
     * Returns the unique serial number of device
     */
    fun getSerialNumber(): String {
        return Build.SERIAL
    }

    fun getDeviceId(): String {
        return Settings.Secure.getString(activity!!.contentResolver, Settings.Secure.ANDROID_ID)
    }

    fun getFormattedDateFromTimestamp(
        timestampInMilliSeconds: Long,
        strFormat: String,
        blnIsAMPM: Boolean
    ): String {
        val date = Date()
        //date.setTime(timestampInMilliSeconds * 1000L);
        date.time = timestampInMilliSeconds

        var strFormattedDate = SimpleDateFormat(strFormat).format(date)

        if (blnIsAMPM) {
            val calendar = Calendar.getInstance()
            calendar.timeInMillis = timestampInMilliSeconds
            val intTimeAMPM = calendar.get(Calendar.AM_PM)

            if (intTimeAMPM == 0) {
                strFormattedDate += " AM"
            } else if (intTimeAMPM == 1) {
                strFormattedDate += " PM"
            }
        }
        return strFormattedDate

    }

    fun getAddressFromLatLong(location: Location, context: Context): ArrayList<String> {
        val addressList = ArrayList<String>()
        try {
            val geocoder: Geocoder
            val addresses: List<Address>
            geocoder = Geocoder(context, Locale.getDefault())

            addresses = geocoder.getFromLocation(
                location.latitude,
                location.longitude,
                1
            ) // Here 1 represent max location result to returned, by documents it recommended 1 to 5

            val address =
                addresses[0].getAddressLine(0) // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
            val city = addresses[0].locality
            val state = addresses[0].adminArea
            val country = addresses[0].countryName
            val postalCode = addresses[0].postalCode
            val knownName = addresses[0].featureName // Only if available else return NULL

            addressList.add(city)
            addressList.add(state)
            addressList.add(country)
            addressList.add(address)
        } catch (e: IOException) {
            e.printStackTrace()
        }

        return addressList
    }

    private fun getDate(time: Long): Date {
        val cal = Calendar.getInstance()
        val tz = cal.timeZone//get your local time zone.
        val sdf = SimpleDateFormat("dd/MM/yyyy hh:mm a")
        sdf.timeZone = tz//set time zone.
        val localTime = sdf.format(Date(time * 1000))
        var date = Date()
        try {
            date = sdf.parse(localTime)//get local date
        } catch (e: ParseException) {
            e.printStackTrace()
        }

        return date
    }

    fun textAnimation(textView: TextView, value: Float) {
        val animator = ValueAnimator.ofFloat(0F, value)
        animator.duration = 1000
        animator.addUpdateListener { animation ->
            textView.text = "Account :" + animation.animatedValue.toString()
        }
        animator.start()
    }

    @Throws(IOException::class)
    fun createImageFile(): File {
        // Create an image file name
        val timeStamp = SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())
        val imageFileName = "JPEG_" + timeStamp + "_"
        val storageDir = Environment.getExternalStoragePublicDirectory(
            Environment.DIRECTORY_PICTURES
        )

        // Save a file: path for use with ACTION_VIEW intents
        return File.createTempFile(
            imageFileName, /* prefix */
            ".jpg", /* suffix */
            storageDir      /* directory */
        )
    }
}