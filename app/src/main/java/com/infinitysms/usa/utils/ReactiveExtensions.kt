import android.content.Context
import android.graphics.drawable.Drawable
import android.view.View
import android.widget.ImageView
import androidx.lifecycle.MutableLiveData
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.Target
import com.infinitysms.usa.R


fun ImageView.loadUrl(context: Context?, url: String) {
    if (context != null) {
        Glide
            .with(context)
            .load(url)
            .apply(
                RequestOptions()
                    .placeholder(R.mipmap.ic_launcher)
                    .fitCenter()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
            )
            .into(this)
    } else {
        this.setImageResource(R.mipmap.ic_launcher)
    }
}

fun ImageView.loadUrlWithCallback(context: Context?, url: String, ivPlaceHolder: ImageView) {
    if (context != null) {
        Glide.with(context)
            .load(url)
            .listener(object : RequestListener<Drawable> {
                override fun onLoadFailed(
                    e: GlideException?,
                    model: Any?,
                    target: Target<Drawable>?,
                    isFirstResource: Boolean
                ): Boolean {
                    ivPlaceHolder.setImageResource(R.mipmap.ic_launcher)
                    return false
                }

                override fun onResourceReady(
                    resource: Drawable?,
                    model: Any?,
                    target: Target<Drawable>?,
                    dataSource: DataSource?,
                    isFirstResource: Boolean
                ): Boolean {
                    //ivPlaceHolder.makeGone()
                    return false
                }
            })
            .into(this)
    } else {
        this.makeGone()
        ivPlaceHolder.setImageResource(R.mipmap.ic_launcher)
        ivPlaceHolder.makeVisible()
    }
}

fun ImageView.loadGIF(context: Context?, id: Int) {
    if (context != null) {
        Glide
            .with(context)
            .load(id)
            .apply(
                RequestOptions()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
            )
            .into(this)
    }
}

fun <T : Any?> MutableLiveData<T>.default(initialValue: T) = apply { setValue(initialValue) }

fun View.makeVisible() {
    visibility = View.VISIBLE
}

fun View.makeInVisible() {
    visibility = View.INVISIBLE
}

fun View.makeGone() {
    visibility = View.GONE
}