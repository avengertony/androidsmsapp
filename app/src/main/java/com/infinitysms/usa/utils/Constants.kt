package com.infinitysms.usa.utils

import com.infinitysms.usa.BuildConfig
import com.infinitysms.usa.MyApplication.Companion.context
import com.infinitysms.usa.R

/**
 * Created byAndroid Tony on 23,March,2020
 */
object Constants {

    // Address Types
    const val PICK_UP = 0
    const val VIA = 1
    const val DESTINATION = 2

    const val PREF_NAME = "app_perf_" + BuildConfig.APPLICATION_ID

    interface DEVICE_TYPE {
        companion object {
            const val ANDROID = "A"
            const val IOS = "I"
        }
    }

    interface FLAVOR {
        companion object {
          //  const val DEVELOPER = "developer"
            const val USA = "usa"
           // const val KUWAIT = "kuwait"
        }
    }

    interface REQUEST_CODE {
        companion object {
            const val ERROR_DIALOG_REQUEST = 9001
            const val PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 9002
            const val PERMISSIONS_REQUEST_ENABLE_GPS = 9003
            const val LOCATION_PREMISSION_REQUEST_CODE = 1000
            const val GPS_SETTING_REQUEST_CODE = 1001
        }
    }

    interface KEY {
        companion object {
            const val FROM = "from"
            const val REGISTER_LIST = "registers"
            const val REGISTER = "register"
            const val DATA_ENTITY_CATEGORY = "data_entity_category"
            const val DATA_ENTITY_LIST = "data_entity_list"
            const val DATA_ENTITY = "data_entity"
            const val NOTIFICATION = "notification"
            const val WEB_URL = "web_url"
            const val EMPLOYEE = "employee"
        }
    }

    interface BOOKING {
        companion object {
            const val ARRIVED = "ARRIVED"
            const val START = "PICKED UP"
            const val FINISH = "FINISH"
            const val COMPLETE = "COMPLETE"

        }
    }

    interface API {
        interface KEY {
            companion object {
                const val SUCCESS = "status"
                const val STATUS_CODE = "status_code"
                const val MESSAGE = "message"
                const val ERROR = "error"
                const val DATA = "data"
                const val TOKEN = "android_auth_token"
                const val USER = "user"
                const val IMAGEPATH = "user"
            }
        }

        interface STATUS {
            companion object {
                const val ERROR = "error"
                const val SHORT_BREAK = "short break"
                const val LONG_BREAK = "long break"
                const val END_BREAK = "end break"
            }
        }
    }

    enum class Legends(val title: String) {
        WORKING_DAY(context.resources.getString(R.string.working_day)),
        PRESENT_DAY(context.resources.getString(R.string.present_day)),
        OFF_DAY(context.resources.getString(R.string.off_day)),
        ON_LEAVE_DAY(context.resources.getString(R.string.on_leave_day))
    }
}