package com.infinitysms.usa.utils

import android.Manifest
import android.annotation.SuppressLint
import android.annotation.TargetApi
import android.app.Activity
import android.content.ContentUris
import android.content.Intent
import android.content.pm.PackageManager
import android.database.Cursor
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Matrix
import android.media.ExifInterface
import android.net.Uri
import android.os.Build
import android.os.Environment
import android.provider.DocumentsContract
import android.provider.MediaStore
import android.util.Log
import androidx.appcompat.app.AlertDialog
import androidx.core.app.ActivityCompat
import androidx.core.content.FileProvider
import androidx.fragment.app.FragmentActivity
import com.infinitysms.usa.R
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.net.URISyntaxException

/**
 * Created by Android Tony on 17,February,2020
 */
class CameraGalleryUtils private constructor(var context: FragmentActivity) {

    private val TAG = CameraGalleryUtils::class.java.simpleName

    companion object {
        private var instance: CameraGalleryUtils? = null

        fun getInstance(context: FragmentActivity): CameraGalleryUtils {
            if (instance == null)
                instance = CameraGalleryUtils(context)

            return instance as CameraGalleryUtils
        }
    }

    private val RC_TAKE_PHOTO = 101
    private val RC_SELECT_FILE = 102
    private val RC_CAMERA_AND_STORAGE_PERMISSION = 103
    private val RC_READ_EXTERNAL_STORAGE_PERMISSION = 104
    private var currentFile: File? = null
    private var cameraGalleryUtilsListener: CameraGalleryUtilsListener? = null


    fun showSelectImageOptionsDialog(cameraGalleryUtilsListener: CameraGalleryUtilsListener) {
        this.cameraGalleryUtilsListener = cameraGalleryUtilsListener

        val options = arrayOf(
            context.resources.getString(R.string.take_photo),
            context.resources.getString(R.string.choose_from_gallery),
            context.resources.getString(R.string.cancel)
        )
        val builder = AlertDialog.Builder(context)
        with(builder) {
            setTitle(context.resources.getString(R.string.select_options))
            setItems(options) { dialog, which ->
                when (options[which]) {
                    options[0] -> {
                        launchCameraWithPermissions(cameraGalleryUtilsListener)
                        dialog.dismiss()
                    }
                    options[1] -> {
                        launchGalleryWithPermission(cameraGalleryUtilsListener)
                        dialog.dismiss()
                    }
                    options[2] -> {
                        dialog.dismiss()
                    }
                }
            }
            show()
        }
    }

    fun launchCameraWithPermissions(cameraGalleryUtilsListener: CameraGalleryUtilsListener) {
        this.cameraGalleryUtilsListener = cameraGalleryUtilsListener

        if (Build.VERSION.SDK_INT >= 23) {
            requestCameraAndStoragePermissions()
        } else {
            launchCamera()
        }
    }

    @TargetApi(Build.VERSION_CODES.M)
    private fun requestCameraAndStoragePermissions() {
        var blnShouldRequest = true
        val cameraPermission = Manifest.permission.CAMERA
        val storagePermission = Manifest.permission.WRITE_EXTERNAL_STORAGE

        val hasCameraPermission =
            ActivityCompat.checkSelfPermission(context.application!!, Manifest.permission.CAMERA)
        val hasExternalStoragePermission = ActivityCompat.checkSelfPermission(
            context.application!!,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
        )

        val permissions = ArrayList<String>()
        if (hasCameraPermission != PackageManager.PERMISSION_GRANTED
            && !ActivityCompat.shouldShowRequestPermissionRationale(
                context,
                Manifest.permission.CAMERA
            )
        ) {
            permissions.add(cameraPermission)
        }

        if (hasExternalStoragePermission != PackageManager.PERMISSION_GRANTED
            && !ActivityCompat.shouldShowRequestPermissionRationale(
                context,
                Manifest.permission.WRITE_EXTERNAL_STORAGE
            )
        ) {
            permissions.add(storagePermission)
        }

        if (permissions.isNotEmpty()) {
            val params = permissions.toTypedArray()
            ActivityCompat.requestPermissions(context, params, RC_CAMERA_AND_STORAGE_PERMISSION)
        } else if (hasCameraPermission != PackageManager.PERMISSION_GRANTED
            && ActivityCompat.shouldShowRequestPermissionRationale(
                context,
                Manifest.permission.CAMERA
            )
        ) {
            cameraGalleryUtilsListener?.onFailure(context.resources.getString(R.string.enable_camera_permission))
        } else if (hasExternalStoragePermission != PackageManager.PERMISSION_GRANTED
            && ActivityCompat.shouldShowRequestPermissionRationale(
                context,
                Manifest.permission.WRITE_EXTERNAL_STORAGE
            ) && blnShouldRequest
        ) {
            cameraGalleryUtilsListener?.onFailure(context.resources.getString(R.string.enable_storage_permission))
        } else {
            launchCamera()
        }
    }

    private fun launchCamera() {
        val takePictureIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(context.packageManager) != null) {
            // Create the File where the photo should go
            if (isExternalStorageReadable()) {
                currentFile = getImageFile()
                if (currentFile != null) {
                    val photoURI = FileProvider.getUriForFile(
                        context.applicationContext!!,
                        context.applicationContext.packageName + ".provider", currentFile!!
                    )
                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI)
                    context.startActivityForResult(takePictureIntent, RC_TAKE_PHOTO)
                }
            }
        }
    }

    /* Checks if external storage is available to at least read */
    private fun isExternalStorageReadable(): Boolean {
        val state = Environment.getExternalStorageState()
        return Environment.MEDIA_MOUNTED == state || Environment.MEDIA_MOUNTED_READ_ONLY == state
    }

    private fun getImageFile(): File {
        val file = File(
            Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS),
            "" + System.currentTimeMillis() + ".png"
        )
        try {
            if (!file.createNewFile()) {
                Log.e(TAG, "File not created")
            }
        } catch (e: IOException) {
            e.printStackTrace()
        }
        return file
    }

    fun launchGalleryWithPermission(cameraGalleryUtilsListener: CameraGalleryUtilsListener) {
        this.cameraGalleryUtilsListener = cameraGalleryUtilsListener

        if (Build.VERSION.SDK_INT >= 23) {
            requestReadStoragePermissions()
        } else {
            launchGallery()
        }
    }

    @TargetApi(Build.VERSION_CODES.M)
    private fun requestReadStoragePermissions() {
        var blnShouldRequest = true
        val readStoragePermission = Manifest.permission.READ_EXTERNAL_STORAGE

        val hasReadStoragePermission = ActivityCompat.checkSelfPermission(
            context.application!!,
            Manifest.permission.READ_EXTERNAL_STORAGE
        )

        val permissions = ArrayList<String>()

        if (hasReadStoragePermission != PackageManager.PERMISSION_GRANTED
            && !ActivityCompat.shouldShowRequestPermissionRationale(
                context,
                Manifest.permission.READ_EXTERNAL_STORAGE
            )
        ) {
            permissions.add(readStoragePermission)
        }

        if (permissions.isNotEmpty()) {
            val params = permissions.toTypedArray()
            ActivityCompat.requestPermissions(context, params, RC_READ_EXTERNAL_STORAGE_PERMISSION)
        } else if (hasReadStoragePermission != PackageManager.PERMISSION_GRANTED
            && ActivityCompat.shouldShowRequestPermissionRationale(
                context,
                Manifest.permission.READ_EXTERNAL_STORAGE
            ) && blnShouldRequest
        ) {
            cameraGalleryUtilsListener?.onFailure(context.resources.getString(R.string.enable_read_storage_permission))
        } else {
            launchGallery()
        }
    }

    private fun launchGallery() {
        val intent = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
        intent.type = "image/*"
        context.startActivityForResult(Intent.createChooser(intent, "Select File"), RC_SELECT_FILE)
    }

    fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        when (requestCode) {
            RC_CAMERA_AND_STORAGE_PERMISSION -> requestCameraAndStoragePermissions()

            RC_READ_EXTERNAL_STORAGE_PERMISSION -> requestReadStoragePermissions()
        }
    }

    fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        try {
            var thumbnail: Bitmap? = null
            if (resultCode == Activity.RESULT_OK) {
                when (requestCode) {
                    RC_TAKE_PHOTO -> {
                        val strImagePath = currentFile?.absolutePath
                        val options = BitmapFactory.Options()
                        // downsizing image as it throws OutOfMemory Exception for larger images
                        options.inSampleSize = 4

                        thumbnail = BitmapFactory.decodeFile(strImagePath, options)

                        val exif = ExifInterface(strImagePath)
                        val rotation = exif.getAttributeInt(
                            ExifInterface.TAG_ORIENTATION,
                            ExifInterface.ORIENTATION_NORMAL
                        )
                        val rotationInDegrees = exifToDegrees(rotation)

                        val matrix = Matrix()
                        if (rotation.toFloat() != 0f) {
                            matrix.preRotate(rotationInDegrees.toFloat())
                        }

                        thumbnail = Bitmap.createBitmap(
                            thumbnail,
                            0,
                            0,
                            thumbnail.width,
                            thumbnail.height,
                            matrix,
                            true
                        )

                        if (thumbnail != null) {
                            val file = File(strImagePath)
                            thumbnail = saveFile(file, thumbnail)
                        }

                        if (thumbnail != null) {
                            cameraGalleryUtilsListener?.onSuccess(thumbnail, strImagePath)
                        } else {
                            cameraGalleryUtilsListener?.onFailure(context.resources.getString(R.string.something_went_wrong))
                        }
                    }

                    RC_SELECT_FILE -> {
                        val uri = data?.data
                        var strImagePath: String? = null
                        try {
                            strImagePath = getPath(uri!!)

                            thumbnail = BitmapFactory.decodeFile(strImagePath)
                        } catch (e: URISyntaxException) {
                            LogUtils.e(TAG, "Unable to upload file from the given uri", e)
                            cameraGalleryUtilsListener?.onFailure(context.resources.getString(R.string.something_went_wrong))
                        }

                        if (thumbnail != null) {
                            cameraGalleryUtilsListener?.onSuccess(thumbnail, strImagePath)
                        } else {
                            cameraGalleryUtilsListener?.onFailure(context.resources.getString(R.string.something_went_wrong))
                        }
                    }
                }
            }
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }

    private fun exifToDegrees(exifOrientation: Int): Int {
        if (exifOrientation == ExifInterface.ORIENTATION_ROTATE_90) {
            return 90
        } else if (exifOrientation == ExifInterface.ORIENTATION_ROTATE_180) {
            return 180
        } else if (exifOrientation == ExifInterface.ORIENTATION_ROTATE_270) {
            return 270
        }
        return 0
    }

    private fun saveFile(file: File?, bitmap: Bitmap): Bitmap {
        var fos: FileOutputStream? = null
        try {
            fos = FileOutputStream(file!!)
            // Use the compress method on the BitMap object to write image to the OutputStream
            bitmap.compress(Bitmap.CompressFormat.PNG, 50, fos)
        } catch (e: Exception) {
            e.printStackTrace()
        } finally {
            try {
                if (fos != null) {
                    fos.close()
                }
            } catch (e: IOException) {
                Log.d(TAG, e.message.toString())
            }

        }

        //return file?.absolutePath
        return bitmap
    }

    /*
     * Gets the file path of the given Uri.
     */
    @SuppressLint("NewApi")
    @Throws(URISyntaxException::class)
    private fun getPath(uri: Uri): String? {
        var uri = uri
        val needToCheckUri = Build.VERSION.SDK_INT >= 19
        var selection: String? = null
        var selectionArgs: Array<String>? = null
        // Uri is different in versions after KITKAT (Android 4.4), we need to
        // deal with different Uris.
        if (needToCheckUri && DocumentsContract.isDocumentUri(
                context.getApplicationContext(),
                uri
            )
        ) {
            if (isExternalStorageDocument(uri)) {
                val docId = DocumentsContract.getDocumentId(uri)
                val split = docId.split(":".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
                return Environment.getExternalStorageDirectory().toString() + "/" + split[1]
            } else if (isDownloadsDocument(uri)) {
                val id = DocumentsContract.getDocumentId(uri)
                uri = ContentUris.withAppendedId(
                    Uri.parse("content://downloads/public_downloads"), java.lang.Long.valueOf(id)!!
                )
            } else if (isMediaDocument(uri)) {
                val docId = DocumentsContract.getDocumentId(uri)
                val split = docId.split(":".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
                val type = split[0]
                if ("image" == type) {
                    uri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI
                } else if ("video" == type) {
                    uri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI
                } else if ("audio" == type) {
                    uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI
                }
                selection = "_id=?"
                selectionArgs = arrayOf(split[1])
            }
        }
        if ("content".equals(uri.scheme, ignoreCase = true)) {
            val projection = arrayOf(MediaStore.Images.Media.DATA)
            var cursor: Cursor? = null
            try {
                cursor = context.getContentResolver()
                    ?.query(uri, projection, selection, selectionArgs, null)
                val column_index = cursor!!.getColumnIndexOrThrow(MediaStore.Images.Media.DATA)
                if (cursor.moveToFirst()) {
                    return cursor.getString(column_index)
                }
            } catch (e: Exception) {
            }

        } else if ("file".equals(uri.scheme, ignoreCase = true)) {
            return uri.path
        }
        return null
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is ExternalStorageProvider.
     */
    private fun isExternalStorageDocument(uri: Uri): Boolean {
        return "com.android.externalstorage.documents" == uri.authority
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is DownloadsProvider.
     */
    private fun isDownloadsDocument(uri: Uri): Boolean {
        return "com.android.providers.downloads.documents" == uri.authority
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is MediaProvider.
     */
    private fun isMediaDocument(uri: Uri): Boolean {
        return "com.android.providers.media.documents" == uri.authority
    }

    interface CameraGalleryUtilsListener {
        fun onSuccess(bitmap: Bitmap, imagePath: String?)
        fun onFailure(message: String)
    }
}