package com.infinitysms.usa.utils.toolbar

import android.view.MenuItem
import android.view.View
import androidx.annotation.IdRes
import androidx.annotation.MenuRes

/**
 * Created byAndroid Tony on 23,March,2020
 */
class FragmentToolbar private constructor(
    @IdRes val resId: Int,
    val title: String,
    @MenuRes val menuId: Int,
    val isBackButton: Boolean,
    val isHamburger: Boolean,
    val isNotification: Boolean,
    val isEvent: Boolean,
    val isEditButton: Boolean,
    val isWithBackground: Boolean,
    val isFromNavigationDrawer: Boolean,
    val isMenuVisible: Boolean,
    val isDateVisible: Boolean,
    val date: String,
    val day: String,
    val backButtonListener: IBackButtonCallback?,
    val hamburgerListener: IHamburgerCallback?,
    val notificationListener: INotificationCallback?,
    val eventListener: IEventCallback?,
    val editListener: IEditButtonCallback?,
    val menuListener: IMenuCallback?
) {

    companion object {
        @JvmField
        val NO_TOOLBAR = -1
        val NO_NOTIFICATION = -1
        val NO_FILTER = -1
    }


    class Builder {
        private var resId: Int = -1
        private var menuId: Int = -1
        private var title: String = ""
        private var date: String = ""
        private var day: String = ""
        private var isBackButton: Boolean = false
        private var isHamburger: Boolean = false
        private var isNotification: Boolean = false
        private var isEvent: Boolean = false
        private var isEditButton: Boolean = false
        private var iswithBackground: Boolean = false
        private var isFromNavigationDrawer: Boolean = false
        private var isMenuVisible: Boolean = false
        private var isDateVisible: Boolean = false
        private var menuItems: MutableList<Int> = mutableListOf()
        private var menuClicks: MutableList<MenuItem.OnMenuItemClickListener?> = mutableListOf()
        private var backButtonListener: IBackButtonCallback? = null
        private var hamburgerListener: IHamburgerCallback? = null
        private var notificationListener: INotificationCallback? = null
        private var eventListener: IEventCallback? = null
        private var editListener: IEditButtonCallback? = null
        private var menuListener: IMenuCallback? = null


        /***
         * Pass FragmentToolbar.NO_TOOLBAR as ID if no toolbar required.
         * */

        fun withId(@IdRes resId: Int) = apply { this.resId = resId }


        fun withTitle(title: String) = apply { this.title = title }

        fun withEdit() = apply { this.isEditButton = true }

        fun withBackButtonCallback(backButtonListener: IBackButtonCallback) = apply {
            this.backButtonListener = backButtonListener
        }

        fun withHamburgerCallback(hamburgerListener: IHamburgerCallback) = apply {
            this.hamburgerListener = hamburgerListener
        }

        fun withNotificationCallback(notificationListener: INotificationCallback) = apply {
            this.notificationListener = notificationListener
        }

        fun withEventCallback(eventListener: IEventCallback) = apply {
            this.eventListener = eventListener
        }

        fun withEditButtonCallback(editListener: IEditButtonCallback) = apply {
            this.editListener = editListener
        }

        fun withMenuCallback(menuListener: IMenuCallback) = apply {
            this.menuListener = menuListener
        }

        fun withBackButton() = apply { this.isBackButton = true }

        fun withHamburger() = apply { this.isHamburger = true }

        fun withNotification() = apply { this.isNotification = true }

        fun withEvent() = apply { this.isEvent = true }

        fun withBackground() = apply { this.iswithBackground = true }

        fun withNavigationDrawer() = apply { this.isFromNavigationDrawer = true }

        fun withMenu() = apply { this.isMenuVisible = true }

        fun withDateAndDay(date: String, day: String) = apply {
            this.isDateVisible = true
            this.date = date
            this.day = day
        }

        fun build() = FragmentToolbar(
            resId,
            title,
            menuId,
            isBackButton,
            isHamburger,
            isNotification,
            isEvent,
            isEditButton,
            iswithBackground,
            isFromNavigationDrawer,
            isMenuVisible,
            isDateVisible,
            date,
            day,
            backButtonListener,
            hamburgerListener,
            notificationListener,
            eventListener,
            editListener,
            menuListener
        )
    }

}

interface IBackButtonCallback {
    fun onToolbarBackPressed(view: View)
}

interface IEditButtonCallback {
    fun onToolbarEditClicked(view: View)
}

interface IHamburgerCallback {
    fun onToolbarHamburgerClicked(view: View)
}

interface INotificationCallback {
    fun onToolbarNotificationClicked(view: View)
}

interface IEventCallback {
    fun onToolbarEventClicked(view: View)
}

interface IMenuCallback {
    fun onMenuClicked(view: View)
}